package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


public class StatisticsCursorAdaptorActivity extends Activity {

 public DatabaseAccess dbHelper;
 public SimpleCursorAdapter dataAdapter;
 public SQLiteDatabase database;
String numo,myTime, myDate;
int marq, session,nberOfCards;

	ImageView topImage;
	TextView t1;



 @Override
 public void onCreate(Bundle savedInstanceState) {
	 requestWindowFeature(Window.FEATURE_NO_TITLE);
  super.onCreate(savedInstanceState);
  setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.statistics_main);

  topImage= (ImageView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.topImage);
  t1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t1);

  dbHelper = new DatabaseAccess(this);
  dbHelper.open();
 // dbHelper.onCreate(database);
//dbHelper.onUpgrade(database);

	 displayListView();}


 private void displayListView() {


	 MyContentTable();
  ListView listView = (ListView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.listView1);
  // Assign adapter to ListView
  listView.setAdapter(dataAdapter);

}



	public void  MyContentTable() //this method populate the content of the list view

	{Cursor cursor = dbHelper.fetchAllChapters();
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_NBER_FAVORITE,
				DatabaseAccess.KEY_NBER_MARKED,
				DatabaseAccess.KEY_NBER_OWN,
				DatabaseAccess.KEY_NBER_WRONG,
				DatabaseAccess.KEY_NBER_NOT_STUDIED,
				DatabaseAccess.KEY_NBER_FLASHCARDS
		};
		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.favorite,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.marked,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.own,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.wrong,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.studied,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.statistics_chapter_list_content, cursor, columns, to, 0);
	}

	@Override
	public void onBackPressed(){


		Intent intent = new Intent(this,Tabs.class);
		startActivity(intent);
		overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
		//finish();

	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	@Override
	public void onDestroy(){

		super.onDestroy();
	}
}


  
  


 
