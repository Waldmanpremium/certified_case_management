package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Locale;

public class Slideshow_Mode extends ParentClass implements TextToSpeech.OnInitListener {

	private Handler mHandler;

	//Newely added september 2018
	TextToSpeech tts;
	boolean zx=false;
	boolean boospeak=false;
	CountDownTimer cTimer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		pageLoader();
		gotoquiz.setText("Seconds remaining");



// diplayCheckAll is a method coming fron ParentClass it displays the content of the study page
		i = 0;
		tts = new TextToSpeech(getApplicationContext(), this);
		mHandler = new Handler();
		mHandler.post(mUpdate);

		//if (!tts.isSpeaking())	{}

		displaySlideShow();
		speaker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!zx){
					speakText();zx=true;
					speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakermuteblack);
				} else {tts.stop();
					zx=false;
					speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);}
			}});

//********  Actions to be executed when the button is clicked ***********
		//**********************************************************************
		//there are some actions that are inherited from the ParentClass so there's no need to repeat it here in this class
		//Just we added in this class the button that have specific actions to this class(study mode)

		gotoquiz.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");
			}});
		addcomment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		bcomment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		bquestion1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		bquestion2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		banswer1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		banswer2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		addcards.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});

		marked.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});
		correct.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});

		favorite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});


		}

	private Runnable mUpdate = new Runnable() {

		public void run() {

			/*chel = dbHelper.getChap_tableCount();
			toastShort("this is the number of chapters"+chel);*/
			del = dbHelper.getNumberOfFlashcards(num);

			if (i < del) {

				if (!tts.isSpeaking())	{

				i = i + 1;

				displayCheckAll(i);
				displaySlideShow();
				mHandler.postDelayed(this, 21000);
				startTimer();
				if (zx){ speakText();}} else {mHandler.postDelayed(this, 1000);}


				//speakerOnandOFF_Test();

			} else {
				toastShort("Great Job!, this is the last flashcard in this chapter/section!");
			}

			if (i == del & num < chel) {
				i = 0;
				num++;

			}


            if(i ==del &num ==chel)

		{

			toastShort("Congartulations! Well Done, you have finished all the study notes /n Great Job! ");
		}

	}


	};

	//start timer function
	void startTimer() {
		cTimer = new CountDownTimer(21000, 1000) {
			public void onTick(long millisUntilFinished) {
				flashNbers.setText(""+millisUntilFinished / 1000);
			}
			public void onFinish() { flashNbers.setText("Done");
			}
		};
		cTimer.start();
	}


	//cancel timer
	void cancelTimer() {
		if(cTimer!=null)
			cTimer.cancel();
	}


	void displaySlideShow()
	{//to hide next and previous chapter
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( 0, GridLayout.LayoutParams.FILL_PARENT, 0);
		super.i10.setLayoutParams(param);
		super.i8.setLayoutParams(param);

		super.addcards.setLayoutParams(param);
		super.addcomment.setLayoutParams(param);
		super.edit.setLayoutParams(param);
		super.correct.setLayoutParams(param);
		super.favorite.setLayoutParams(param);
		super.marked.setLayoutParams(param);

		LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 0);
		super.l1.setLayoutParams(param1);




	}



	@Override
	public void onInit(int status){

		if(status== TextToSpeech.SUCCESS){

			int result=tts.setLanguage(Locale.US);

		}

	}

	@Override
	public void onDestroy(){
		// Don't forget to shutdown tts!
		if(tts!=null){
			tts.stop();
			tts.shutdown();
			zx=false;
		}
		session_Tracker();
		cTimer.onFinish();
		cancelTimer();
		mHandler.removeCallbacks(mUpdate);
		super.onDestroy();
	}



	public void speakText(){

		CharSequence text="flashcard number" + i+"Question: \n"+q.getText().toString()+"\n The Answer is: \n"+ans.getText().toString()+".\n"+comment.getText().toString();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			tts.speak(text,TextToSpeech.QUEUE_FLUSH,null,"id1");
		} else{HashMap<String, String> map = new HashMap<>();
			map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
			tts.speak(""+text, TextToSpeech.QUEUE_FLUSH, map);}
	}
}
