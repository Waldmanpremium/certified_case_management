package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Testing_Mode extends ParentClass {

int yes1 = 0, no1 = 0,
        sum1 = 0, j = 0, res = 0;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		pageLoader();
no1=wrong;
sum1=responded;
		testSessionNber=testSessionNber+1;

// diplayCheckAll is a method coming from ParentClass it displays the content of the study page
		displayCheckAll(i);
        testingBar_Weignt((float)1.3);
		//this method hide the answer and the comment
		noDisplayedAnswer();
//********  Actions to be executed when the button is clicked ***********
//**********************************************************************

//Specific buttn to testing class*****************


//this is the "Answer" button
		butt0.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				displayCheckAllForButons(i,8);
				testingBar_Weignt((float)1.3);
			}});

//this is the "correct" button
		butt1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ans.getText().toString().isEmpty()) {
					toastShort("See the answer first then click correct or wrong!");
				} else {
				dbHelper.correct_click(num, i);
            //toastShort("Very good you answered correct");
				correct_test();
				yes1++;
					sum1++;
					session_Tracker1();

			}}});
//this is the "wrong" button
		butt2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ans.getText().toString().isEmpty()) {
					toastShort("See the answer first then click correct or wrong!");
				} else {
					dbHelper.wrong_click(num, i);
					//toastShort("Sorry for you, you answered wrongly");
					correct_test();
            		no1++;
					sum1++;
					session_Tracker1();
				}}});

//this is the "Discard" button
		butt3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                if (i < del) {
                    i = i + 1;

                    displayCheckAllForButons(i,8);
                    //this method hide the answer and the comment
                    noDisplayedAnswer();
                    testingBar_Weignt((float)1.3);
                }

                else toastShort("Hey, this is the last flashcard");
            }

			});

//this is the "Result" button
		butt4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


              if (sum1 == 0) {
                    toastShort("Please, respond to question first by clicking Correct or Wrong"
                            );
                } else {   res = ((sum1-no1)*100 / sum1);
                    if (res < 20) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Your score is "
                                        + res
                                        + "%"
                                        + "\n\n"
                                        + "Your score is very low! You'd better prepare from the Learning Mode, before retaking this test!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        if (res < 50) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Your score is "
                                            + res
                                            + "%"
                                            + "\n\n"
                                            + "Your score still under the average! You'd better prepare from the Learning Mode, before retaking this test!",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            if (res < 75) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        "Your score is "
                                                + res
                                                + "%"
                                                + "\n\n"
                                                + "Your are almost done! You should improve a little bit your preparation & concentrate more and you will succeed your exam!",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(
                                        getApplicationContext(),
                                        " Congratulations, Your score is "
                                                + res
                                                + "%"
                                                + "\n\n"
                                                + "Very good score, you are well concentrated, also your preparation is fine!",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }



			}}});

//General buttons for all classes but with some specifications
//Previous Chapter imageButton
		i8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				previousChapterButton();
				//this method hide the answer and the comment
				noDisplayedAnswer();
				testingBar_Weignt((float)1.3);
			}
		});

//Next Chapter imageButton
		i10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				nextChapterButton();
				//this method hide the answer and the comment
				noDisplayedAnswer();
				testingBar_Weignt((float)1.3);

			}
		});
// next flashcard button
		but3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//del = dbHelper.getNumberOfFlashcards(num);
				if (i < del) {
					i = i + 1;

					displayCheckAllForButons(i,8);
					//this method hide the answer and the comment
					noDisplayedAnswer();
					testingBar_Weignt((float)1.3);
				}

					else toastShort("Hey, this is the last flashcard");
			}
		});
// Previous flashcard button
		but1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (i > 1) {
					i = i - 1;
					views=views+1;
					displayCheckAllForButons(i,8);
					//this method hide the answer and the comment
					noDisplayedAnswer();
					testingBar_Weignt((float)1.3);
				} else toastShort("Hey, this is the first flashcard");
			}
		});

	// Goto Quiz number	# Button
		gotoquiz.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				layoutParameters();
				String quizNber = flashNbers.getText().toString();
				if (quizNber.isEmpty()) toastShort("Please type the quiz number you want to goto");

				int quizInt = Integer.parseInt(quizNber);
				if (quizInt==0) toastShort("Please type the quiz number you want to goto, the number should be between 1 and "
						+ del);


				if (quizInt > del) toastShort("Please type the quiz number you want to goto, the number should be between 1 and "
								+ del);

				if (quizInt > 0 && quizInt <= del) {
					displayOneView(num, quizInt);
					i = quizInt;
					flashNbers.clearFocus();
					views=views+1;
					session_Tracker();
					favorite_test();
					seen_before_test();
					marked_test();
					correct_test();
					Comment_test();
					ownFlash_test();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(flashNbers.getWindowToken(), 0);
//this method hide the answer and the comment
					noDisplayedAnswer();
					testingBar_Weignt((float)1.3);
				}
				// setFocusable(false)
			}
		});


// Add Comment Button
		addcomment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//xcomment = dbHelper.return_Comment(num, i);
				// String comment1=comment.getText().toString();
				if (ans.getText().toString().isEmpty()) {
					toastShort("See the answer first then write down your comment!");
				} else {
					comment.setEnabled(true);
					if (!xcomment.isEmpty()) {
						toastShort("Already you have a comment!\n You Can edit your comment again.\nPlease SAVE it, once you are done.");
						commentWeight(5);
						pageWeight(3);
						bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcommentsave);
						comment.requestFocus();
						InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
						imm.showSoftInput(comment, InputMethodManager.SHOW_IMPLICIT);
					} else {
						if (xxx != 2) {

							Toast.makeText(
									getApplicationContext(),
									"Please Write down your comment!\nKindly SAVE it, once you are done.",
									Toast.LENGTH_SHORT).show();
							bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcommentsave);

							commentWeight(5);

							pageWeight(3);

							InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							imm.showSoftInput(comment,
									InputMethodManager.SHOW_IMPLICIT);
							comment.setText("Write your comment here!");
							comment.requestFocus();

							xxx = 2;
						} else {
							commentWeight(0);
							pageWeight(8);
							comment.clearFocus();
							InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
							bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcomment);
							xxx = 1;
						}
					}

				}

			}});
// This button save the comment added
		bcomment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String comment1 = comment.getText().toString();
				bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcomment);

				// DONT USE ==Null or !Null in if condition or elsewhere in any
				// test condition, NEVER EVER
				if (!comment1.isEmpty()) {

					dbHelper.saveComment(num, i, comment1, getBaseContext().getApplicationContext());
					addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentblack);
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Your Comment is saved successfully",
							Toast.LENGTH_SHORT).show();
					comment.clearFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);

					pageWeight(8);

				} else {
					dbHelper.saveComment(num, i, comment1, getBaseContext().getApplicationContext());
					addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentblack);
					toastShort("This Flashcard without any comment!");


					commentWeight(0);
					pageWeight(8);
					comment.clearFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);

				}
			}

		});
//bquestion 1 save the edited question
		bquestion1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					String edit_q = q.getText().toString();
					bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);

					dbHelper.saveEdit_question(num, i, edit_q, getBaseContext().getApplicationContext());
					edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Your Edited Question was saved successfully",
							Toast.LENGTH_SHORT).show();
					q.clearFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(q.getWindowToken(), 0);
					question_answer_editext_test(false, false);
					pageWeight(8);

			}
		});
//bquestion2  to save the new question
		bquestion2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// int j1=R.drawable.ccaddcards;
				// if(addcards.getId()==j1){

				// if(aImg==bImg){

				// if (!q.getText().toString().isEmpty()){
				if (!q.getText().toString().isEmpty()) {
					del = del + 1;
					dbHelper.insertLog_q(q.getText().toString(), ans.getText()
							.toString(), num, del, 1, getBaseContext().getApplicationContext());
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Your new Flashcard was saved successfully",
							Toast.LENGTH_SHORT).show();
					q.clearFocus();
					ans.requestFocus();
					question_answer_editext_test(false, true);
					pageWeight(8);
					bquestion2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
					banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswersave);

				} else {
					toastShort("Please, add your Question/Term before saving!"
							);
				}
			}

		}

		);
//banswer 1  to save the edited answer
		banswer1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!ans.getText().toString().isEmpty()) {
					String edit_ans = ans.getText().toString();
					banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

					dbHelper.saveEdit_answer(num, i, edit_ans, getBaseContext().getApplicationContext());
					edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Your Edited Answer was saved successfully",
							Toast.LENGTH_SHORT).show();
					ans.clearFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(ans.getWindowToken(), 0);
					question_answer_editext_test(false, false);
					pageWeight(8);

				}else {
					toastShort("Please, add your edited answer/def before saving!"
					);}
			}});
//banswer2  to save the new answer
		banswer2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (!ans.getText().toString().isEmpty()) {
					dbHelper.insertLog_ans(q.getText().toString(), ans
							.getText().toString(), num, del, 1, getBaseContext().getApplicationContext());
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Your new Flashcard was saved successfully",
							Toast.LENGTH_SHORT).show();
					ans.clearFocus();

					question_answer_editext_test(false, false);
					pageWeight(8);

					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(q.getWindowToken(), 0);
					rightSide1_Weight(1);
					rightSide2_Weight(0);
					banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);
				} else {
					Toast.makeText(getBaseContext().getApplicationContext(),
							"Please, add your Answer/Explanation/Definition before saving!",
							Toast.LENGTH_SHORT).show();
				}

			}

		});

// edit button To edit question and then answer
		edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				rightSide1_Weight(1);
				rightSide2_Weight(0);
				edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
				if (xxx == 1) {

					Toast.makeText(
							getApplicationContext(),
							"After editing the question.\nPlease SAVE it. to edit the answer click again on Edit button",
							Toast.LENGTH_SHORT).show();
					question_answer_editext_test(true, false);
					commentWeight(0);
					q_Weight(10);
					ans_Weight(3);

					bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestionsave);
					banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

					q.setCursorVisible(true);
					q.setFocusableInTouchMode(true);
					ans.setFocusable(true);
					q.requestFocus(); // to trigger the soft input

					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.showSoftInput(q, InputMethodManager.SHOW_IMPLICIT);
					xxx = xxx + 1;
				} else {
					if (xxx == 2) {
						{

							Toast.makeText(
									getApplicationContext(),
									"After editing the Answer.\nPlease SAVE it. to edit the question click again on Edit button",
									Toast.LENGTH_SHORT).show();
							question_answer_editext_test(false, true);
							banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswersave);
							bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
							commentWeight(0);

							q_Weight(3);
							ans_Weight(8);

							ans.setCursorVisible(true);
							ans.setFocusableInTouchMode(true);
							ans.setFocusable(true);
							ans.requestFocus();

							InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							imm.showSoftInput(ans,
									InputMethodManager.SHOW_IMPLICIT);



							xxx = 1;

						}

					}
				}
			}

		});

//addcard button is for adding a new flashcard
		addcards.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {toastShort("This option is deactivated from the Slideshow Mode - Go to Learning Mode to use it!");}});





		//marked imagebutton is to add the flashcard to marked flashcards
		marked.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				int button = dbHelper.marked_click(num, i);

				if (button == 1) {

					marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markclicked);
					Toast.makeText(getApplicationContext(),
							"Marked for review successfully: " + button,
							Toast.LENGTH_SHORT).show();
				} else {

					marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markblack);
					Toast.makeText(
							getApplicationContext(),
							"Removed from Marked for review successfully: "
									+ button, Toast.LENGTH_SHORT).show();
				}
			}
		});
//correct imageButton is to give you information that the flashcard was tested successfully or not
		correct.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				int button = dbHelper.correct_method(num, i);

				if (button == 1) {
					Toast.makeText(
							getApplicationContext(),
							"This Quiz was answered correctly during the previous test",
							Toast.LENGTH_SHORT).show();
				} else if (button == 2) {

					Toast.makeText(
							getApplicationContext(),
							"This Quiz was answered wrongly during the previous test",
							Toast.LENGTH_SHORT).show();
				} else {

					Toast.makeText(getApplicationContext(),
							"This Quiz was not tested yet!", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});


		favorite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				int button = dbHelper.favorite_click(num, i);

				if (button == 1) {

					favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteclicked);
					Toast.makeText(getApplicationContext(),
							"Added to favorite successfully: " + button,
							Toast.LENGTH_SHORT).show();
				} else {

					favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteblack);
					Toast.makeText(getApplicationContext(),
							"Removed from favorite successfully: " + button,
							Toast.LENGTH_SHORT).show();
				}
			}
		});


	}

	public void Comment_test1() {

		// displayOneView(num,i);
		// String button=comment.getText().toString();
		//xcomment = dbHelper.return_Comment(num, i);

		if (xcomment.equals("0")) {

			addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentblack);
			commentWeight(0);
			toSpeak ="Question:\n"+q.getText().toString()+"\n.The Answer is:\n"+ans.getText().toString();

		} /*else {

			addcomment.setImageResource(R.drawable.ccaddcomment);

			commentWeight(5);
			comment.setEnabled(false);
			//if(!getApplicationContext().getClass().getPackageName().equals("Testing_Mode")){
			toastShort( xcomment);
			toSpeak ="Question:\n"+q.getText().toString()+"\n. The Answer is: \n"+ans.getText().toString()+"\n. Your Comment is:.\n "+comment.getText().toString();
		}*/
	}

	public void session_Tracker1(){

		dbHelper.Time_and_Date_Finish_Saver_Method0(myTime,myDate,num,i,views, sum1,no1, sessionNber,testSessionNber );

	/* Date currentDate = new Date(System.currentTimeMillis());

	 Time currentTime = new Time();
	 currentTime.setToNow();*/
	}


private void noDisplayedAnswer(){
	//this is specific to testing class
	ans.setText("");
	comment.setText("");
	commentWeight(0);
	Comment_test1();
}

}
