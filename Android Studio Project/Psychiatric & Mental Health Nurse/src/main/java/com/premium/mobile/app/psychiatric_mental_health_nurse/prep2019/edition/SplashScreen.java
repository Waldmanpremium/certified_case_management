package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;


public class SplashScreen extends Activity {
    private ProgressBar mProgress;
    Button b1, b2, b3,b4;
    String app_version;
    int quizNber;
    int ie,sessionNber ;
    private DatabaseAccess dbHelper;
    private SQLiteDatabase database;
    private ProgressBar spinner;
    private Handler mHandler;
    int resId1,resId2,resId3,num,numo,num1,k,z;
    String quest1,text1,title ,ftitle ,fquest,ftext;

    int viewed,marked1,nonStudied1,own1, favorite1,results,  marq, cid, fid,responded,wrong, ChapNber;
    String myTime,myDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        // Show the splash screen
        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.splash_screen);
        mProgress = (ProgressBar) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.splash_screen_progress_bar);
        //ImageView im1 = (ImageView) findViewById (R.id.im1);
        Animation a1 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        Animation a10 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.alltogether);
        //im1.startAnimation(a1);
        //im1.startAnimation(a10);


        dbHelper = new DatabaseAccess(this);
        dbHelper.open();
        dbHelper.onCreate(database);
        sessionNber = dbHelper.getSession_tableCount();
        if (dbHelper.getResults(sessionNber) == -1 || sessionNber == 0 || sessionNber == -1) {

            Toast.makeText(getApplicationContext(),
                    "We are getting things done for you.\n\nLoading In Progress...", Toast.LENGTH_SHORT).show();
        }
        // Start lengthy operation in a background thread
        new Thread(new Runnable() {
            public void run() {

                doWork();
                startApp();

               finish();
            }
        }).start();
    }

    private void doWork() {

            try {

                sessionNber = dbHelper.getSession_tableCount();
                if (dbHelper.getResults(sessionNber) == -1 || sessionNber == 0 || sessionNber == -1) {


                    dbHelper.onUpgrade(database);}

                if (sessionNber == -1 || sessionNber == 0) {


                    sessionNber = dbHelper.Time_and_Date_START_Saver_Method(myTime, myDate, 1, 1, 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
              //  Log.v("myapp", e.getMessage());
               // Timber.e(e.getMessage());
            }


        try {mProgress.setProgress(60);
            Thread.sleep(200);
            mProgress.setProgress(80);
            Thread.sleep(300);
            mProgress.setProgress(100);
            Thread.sleep(400);}catch (Exception e) {
            e.printStackTrace();
            //Log.v("myapp", e.getMessage());
            // Timber.e(e.getMessage());
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreen.this, Tabs.class);
        startActivity(intent);
    }
    //method Newly added spetember 2018 to get total number of chapters
 /*   public int OldReturnNumberOfChapterFromString(){

        k=0; numo=1;


        resId3 = getApplicationContext().getResources().getIdentifier("titl" + numo, "string", getPackageName());
        while (resId3 != 0) {resId3 = getApplicationContext().getResources().getIdentifier("titl" + numo, "string", getName());
            k = k + 1;

            numo++;
        }
        k=k-1;
        return	k;
    }



    public int OldReturnNumberOfFalshcardsFromString(int num){

        z=0; num1=1;
        fquest = "q" + num+"a";
        resId1 =getApplicationContext().getResources().getIdentifier ( fquest + num1,  "string", getName());
        while (resId1 != 0) {
            resId1 =getApplicationContext().getResources().getIdentifier ( fquest + num1,  "string", getName());
            z = z + 1;
            num1++; }
        z = z - 1;
        num1=1;
        return	z;
    }
*/

    @Override
    public void onBackPressed() {
        // do nothing.
    }
}