package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.LinearLayout;


public class MarkedActivity extends ParentClass {

    int j = num, k = i;
    private static final String TAG = "myApp";
    int marq;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        dbHelper = new DatabaseAccess(this);
        dbHelper.open();
       marq = getIntent().getIntExtra("somekey7",6);

        pageLoader();
        flashNbers.setFocusable(false);
        gotoquiz.setText("Card #");
        //to hide next and previous chapter
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( 0, GridLayout.LayoutParams.FILL_PARENT, 0);
        super.i10.setLayoutParams(param);
        super.i8.setLayoutParams(param);


        switch (marq) {

            case 6: Marked_Display();   break;
            case 7: ownFlash_Display(); break;
            case 8: unseen_Display(); break; //same as non-studied
            case 9:Favorite_Display();   break;
            case 10:wrongFlash_Display();   break;
        }

        // next flashcard button
        but3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    switch (marq) {

                    case 6: nextCheckForMarkedFlashcards();   break;
                    case 7: nextCheckForOwnFlashcards(); break;
                    case 8: nextCheckForUnseenFlashcards(); break;
                    case 9: nextCheckForFavoriteFlashcards();   break;
                    case 10: nextCheckForWrongFlashcards();   break;
                }

            }
        /*if(num==chel){
                if (k==del){if (m==1){displayCheckAllForButons(k, 8);}else {toastShort("This is the last marked card for review");}} else {for (k=i; k<del;k++) {
            m = dbHelper.marked_method(num, k);
            i = k;
            if (m == 1) {
                displayCheckAllForButons(k, 8);
                break;
            }

 }*/


        /*if  (i==1){

            if (m==1) {
                displayCheckAllForButons(i, 8);
                toastShort("Hey! You just moved to next chapter!");
            i++;  } else{
    while(m!=1 && i<del) {
        i++;
        m=dbHelper.marked_method(num, i);
    }
                if (m==1) {
                    displayCheckAllForButons(i, 8);
                    if (i < del) i++;
                    else if (num < chel) {
                        num++;
                        i = 1;
                    } else if (m==1) {
                        displayCheckAllForButons(i, 8);}
                }}} else{


  if (i>1 && i<del){


      if (m==1) {
          displayCheckAllForButons(i, 8);
          i++;
      } else{

            while(m!=1 && i<del) {
                 i++;
                m=dbHelper.marked_method(num, i);}
          if (m==1) {
              displayCheckAllForButons(i, 8);
           if (i<del)   i++; else if (num < chel){num++;i=1;}
          }}} else {

            if(i==del){

            if (m==1) {
                displayCheckAllForButons(i, 8); if (num < chel){num++;i=1;}
               }
               else {
                if (num < chel){num++;i=1;}
                m=dbHelper.marked_method(num, i);
                if (m==1) {
                    displayCheckAllForButons(i, 8);
                    toastShort("Aloo You just moved to next chapter!");
                    i++;}else{

                while(m!=1 && i<del) {
                    i++;
                    m=dbHelper.marked_method(num, i);}


// diplayCheckAll is a method coming fron ParentClass it displays the content of the study page
        displayCheckAllForButons(i, 8);
        toastShort("You just moved to next chapter!");
    }}}}}


    */
        });
        // Previous flashcard button
        but1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (marq) {

                    case 6: previousCheckForMarkedFlashcards();   break;
                    case 7: previousCheckForOwnFlashcards(); break;
                    case 8: previousCheckForUnseenFlashcards(); break;
                    case 9: previousCheckForFavoriteFlashcards();   break;
                    case 10: previousCheckForWrongFlashcards();   break;
                }

            }
        });
    }


    public void Marked_Display(){
            m = dbHelper.marked_method(num, i);
        if(i==1&&m==1)

    {
        displayCheckAll(i);
        i++;
    } else

    {
        while (m != 1 && i < del) {
            i++;
            m = dbHelper.marked_method(num, i);
        }
        displayCheckAll(i);
        i++;
    }

}

public void  nextCheckForMarkedFlashcards() {
    chel = dbHelper.getChap_tableCount();


    if (num == chel) {
        for (k = i; k < del; k++) {

            i++;
            m = dbHelper.marked_method(chel, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }
        }

        if (k == del) {
            m = dbHelper.marked_method(chel, del);
            if (m == 1) {
                displayCheckAllForButons(k, 8);


            } else {

                toastShort("Hey, this is the Last marked for review flashcard");
            }

        }

    } else {
        while (num < chel) {
            //  for (j = num; j < chel; j++) {
            del = dbHelper.getNumberOfFlashcards(j);

            //for (k = i; k < del; k++) {
            while (i < del) {

                m = dbHelper.marked_method(j, i);
                if (m == 1) {
                    displayCheckAllForButons(i, 8);

                    break;
                }
                i++;
            }

            if (i == del) {
                m = dbHelper.marked_method(num, del);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);
                    num++;
                    i = 1;
                    break;
                } else {
                    num++;
                    i = 1;
                }
            }
            if (m == 1) {
                displayCheckAllForButons(i, 8);
                break;
            }

            num++;
        }
        if (num == chel) {
            while (i<del) {


                m = dbHelper.marked_method(chel, i);
                if (m == 1) {
                    displayCheckAllForButons(i, 8);

                    break;
                }
                i++;
            }

            if (i == del) {
                m = dbHelper.marked_method(chel, del);
                if (m == 1) {
                    displayCheckAllForButons(i, 8);


                } else {

                    toastShort("Hey, this is the Last marked for review flashcard");
                }

            }
        }
    }
}
    public void  previousCheckForMarkedFlashcards(){
        chel =dbHelper.getChap_tableCount();

        if(num==1 ){  for (k = i; k > 1; k--) {

            i--;
            m = dbHelper.marked_method(1, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == 1) {
                m = dbHelper.marked_method(1, 1);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is the 1st marked for review flashcard, no records before it");
                }

            }

        } else {
            for (j = num; j > 1; j--) {
                del = dbHelper.getNumberOfFlashcards(j);

                for (k = i; k > 1; k--) {

                    i--;
                    m = dbHelper.marked_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == 1) {
                    m = dbHelper.marked_method(j, 1);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num--;
                        i = del+1;
                        break;
                    } else {
                        num--;
                        i = del+1;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }
//******************************FAVORITE METHODS*****************************************
    public void Favorite_Display(){
        m = dbHelper.favorite_method(num, i);
        if(i==1&&m==1)

        {
            displayCheckAll(i);
            i++;
        } else

        {
            while (m != 1 && i < del) {
                i++;
                m = dbHelper.favorite_method(num, i);
            }
            displayCheckAll(i);
            i++;
        }

    }

    public void  nextCheckForFavoriteFlashcards(){
        chel =dbHelper.getChap_tableCount();


        if(num==chel ){  for (k = i; k < del; k++) {

            i++;
            m = dbHelper.favorite_method(chel, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == del) {
                m = dbHelper.favorite_method(chel, del);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is the Last Favorite  flashcard");
                }

            }

        } else {
            for (j = num; j < chel; j++) {
                del = dbHelper.getNumberOfFlashcards(j);

                for (k = i; k < del; k++) {

                    i++;
                    m = dbHelper.favorite_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == del) {
                    m = dbHelper.favorite_method(j, del);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num++;
                        i = 0;
                        break;
                    } else {
                        num++;
                        i = 0;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }


    public void  previousCheckForFavoriteFlashcards(){
        chel =dbHelper.getChap_tableCount();


        if(num==1 ){  for (k = i; k > 1; k--) {

            i--;
            m = dbHelper.favorite_method(1, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == 1) {
                m = dbHelper.favorite_method(1, 1);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is the 1st favorite flashcard, no records before it");
                }

            }

        } else {
            for (j = num; j > 1; j--) {
                del = dbHelper.getNumberOfFlashcards(j);

                Log.v(TAG, "total flash nber " + del + "chapter nber" + j);
                for (k = i; k > 1; k--) {

                    i--;
                    m = dbHelper.favorite_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == 1) {
                    m = dbHelper.favorite_method(j, 1);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num--;
                        i = del+1;
                        break;
                    } else {
                        num--;
                        i = del+1;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }

 //****************************************OWN METHODS*************************************

    public void ownFlash_Display(){
        m = dbHelper.ownFlash_method(num, i);
        if(i==1&&m==1)

        {
            displayCheckAll(i);
            i++;
        } else

        {
            while (m != 1 && i < del) {
                i++;
                m = dbHelper.ownFlash_method(num, i);
            }
            displayCheckAll(i);
            i++;
        }

    }

    public void  nextCheckForOwnFlashcards(){
        chel =dbHelper.getChap_tableCount();

        Log.v(TAG,"total number of chapters"+chel);
        if(num==chel ){  for (k = i; k < del; k++) {

            i++;
            m = dbHelper.ownFlash_method(chel, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == del) {
                m = dbHelper.ownFlash_method(chel, del);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is your Last own flashcard");
                }

            }

        } else {
            for (j = num; j < chel; j++) {
                del = dbHelper.getNumberOfFlashcards(j);

                Log.v(TAG, "total flash nber " + del + "chapter nber" + j);
                for (k = i; k < del; k++) {

                    i++;
                    m = dbHelper.ownFlash_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == del) {
                    m = dbHelper.ownFlash_method(j, del);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num++;
                        i = 0;
                        break;
                    } else {
                        num++;
                        i = 0;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }


    public void  previousCheckForOwnFlashcards(){
        chel =dbHelper.getChap_tableCount();


        if(num==1 ){  for (k = i; k > 1; k--) {

            i--;
            m = dbHelper.ownFlash_method(1, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == 1) {
                m = dbHelper.ownFlash_method(1, 1);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is your 1st own flashcard, no records before it");
                }

            }

        } else {
            for (j = num; j > 1; j--) {
                del = dbHelper.getNumberOfFlashcards(j);


                for (k = i; k > 1; k--) {

                    i--;
                    m = dbHelper.ownFlash_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == 1) {
                    m = dbHelper.ownFlash_method(j, 1);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num--;
                        i = del+1;
                        break;
                    } else {
                        num--;
                        i = del+1;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }


    //****************************************OWN METHODS*************************************

    public void wrongFlash_Display(){
        m = dbHelper.wrongFlash_method(num, i);
        if(i==1&&m==1)

        {
            displayCheckAll(i);
            i++;
        } else

        {
            while (m != 1 && i < del) {
                i++;
                m = dbHelper.wrongFlash_method(num, i);
            }
            displayCheckAll(i);
            i++;
        }

    }

    public void  nextCheckForWrongFlashcards(){
        chel =dbHelper.getChap_tableCount();

        Log.v(TAG,"total number of chapters"+chel);
        if(num==chel ){  for (k = i; k < del; k++) {

            i++;
            m = dbHelper.wrongFlash_method(chel, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == del) {
                m = dbHelper.wrongFlash_method(chel, del);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is your Last wrongly replied flashcard");
                }

            }

        } else {
            for (j = num; j < chel; j++) {
                del = dbHelper.getNumberOfFlashcards(j);


                for (k = i; k < del; k++) {

                    i++;
                    m = dbHelper.wrongFlash_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == del) {
                    m = dbHelper.wrongFlash_method(j, del);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num++;
                        i = 0;
                        break;
                    } else {
                        num++;
                        i = 0;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }


    public void  previousCheckForWrongFlashcards(){
        chel =dbHelper.getChap_tableCount();


        if(num==1 ){  for (k = i; k > 1; k--) {

            i--;
            m = dbHelper.wrongFlash_method(1, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == 1) {
                m = dbHelper.wrongFlash_method(1, 1);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, this is your 1st wrong replied flashcard, no records before it");
                }

            }

        } else {
            for (j = num; j > 1; j--) {
                del = dbHelper.getNumberOfFlashcards(j);


                for (k = i; k > 1; k--) {

                    i--;
                    m = dbHelper.wrongFlash_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == 1) {
                    m = dbHelper.wrongFlash_method(j, 1);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num--;
                        i = del+1;
                        break;
                    } else {
                        num--;
                        i = del+1;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }




    //*******************************************UNSEEN / Non Studeied METHODS *******************************
    public void unseen_Display(){
        m = dbHelper.unseen_method(num, i);
        if(i==1&&m==1)

        {
            displayCheckAll(i);
            i++;
        } else

        {
            while (m != 1 && i < del) {
                i++;
                m = dbHelper.unseen_method(num, i);
            }
            displayCheckAll(i);
            i++;
        }

    }

    public void  nextCheckForUnseenFlashcards(){
        chel =dbHelper.getChap_tableCount();

        Log.v(TAG,"total number of chapters"+chel);
        if(num==chel ){  for (k = i; k < del; k++) {

            i++;
            m = dbHelper.unseen_method(chel, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == del) {
                m = dbHelper.unseen_method(chel, del);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hola!!!, this is the Last Non Studied  flashcard");
                }

            }

        } else {
            for (j = num; j < chel; j++) {
                del = dbHelper.getNumberOfFlashcards(j);

                for (k = i; k < del; k++) {

                    i++;
                    m = dbHelper.unseen_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == del) {
                    m = dbHelper.unseen_method(j, del);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num++;
                        i = 0;
                        break;
                    } else {
                        num++;
                        i = 0;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }


    public void  previousCheckForUnseenFlashcards(){
        chel =dbHelper.getChap_tableCount();


        if(num==1 ){  for (k = i; k > 1; k--) {

            i--;
            m = dbHelper.unseen_method(1, i);
            if (m == 1) {
                displayCheckAllForButons(i, 8);

                break;
            }}

            if (k == 1) {
                m = dbHelper.unseen_method(1, 1);
                if (m == 1) {
                    displayCheckAllForButons(k, 8);


                } else{

                    toastShort("Hey, there's no previous non studied Flashcards");
                }

            }

        } else {
            for (j = num; j > 1; j--) {
                del = dbHelper.getNumberOfFlashcards(j);

                Log.v(TAG, "total flash nber " + del + "chapter nber" + j);
                for (k = i; k > 1; k--) {

                    i--;
                    m = dbHelper.unseen_method(j, i);
                    if (m == 1) {
                        displayCheckAllForButons(i, 8);

                        break;
                    }

                }

                if (k == 1) {
                    m = dbHelper.unseen_method(j, 1);
                    if (m == 1) {
                        displayCheckAllForButons(k, 8);
                        num--;
                        i = del+1;
                        break;
                    } else {
                        num--;
                        i = del+1;
                    }
                }
                if (m == 1) {
                    displayCheckAllForButons(i, 8);
                    break;
                }
            }

        }
    }







    }



