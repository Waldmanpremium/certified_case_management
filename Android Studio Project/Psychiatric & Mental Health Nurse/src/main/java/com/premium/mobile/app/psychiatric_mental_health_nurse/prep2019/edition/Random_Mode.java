package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import java.util.Random;

public class Random_Mode extends ParentClass {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        pageLoader();
        gotoquiz.setText("Cards #");
   final   int  fid=random1();
// diplayCheckAll is a method coming fron ParentClass it displays the content of the study page
        displayCheckAll(fid);

        //Previous Chapter imageButton
        i8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                previousChapterButton(random1());
            }
        });

//Next Chapter imageButton
        i10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {nextChapterButton(random1());    }
        });

//********  Actions to be executed when the button is clicked ***********
 //**********************************************************************
 //there are some actions that are inherited from the ParentClass so there's no need to repeat it here in this class
 //Just we added in this class the button that have specific actions to this class(study mode)




 // next flashcard button
        but3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                 i=random1();

                    displayCheckAllForButons(i,8);

            }
        });
// Previous flashcard button
        but1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                    i = random1();
                    views=views+1;
                    displayCheckAllForButons(i,8);

            }
        });





    }

public int random1(){
    del = dbHelper.getNumberOfFlashcards(num);

    Random r1 = new Random();
    final  int fid=r1.nextInt(del)+1;
return fid;}

}
