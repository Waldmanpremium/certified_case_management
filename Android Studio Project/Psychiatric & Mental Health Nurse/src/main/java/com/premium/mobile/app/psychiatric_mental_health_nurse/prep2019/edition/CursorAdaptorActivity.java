package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class CursorAdaptorActivity extends Activity {

 public DatabaseAccess dbHelper;
 public SimpleCursorAdapter dataAdapter;
 public SQLiteDatabase database;
String numo,myTime, myDate;
int marq, session,nberOfCards;

	ImageView topImage;
	TextView t1;

	//back button : this method is executed when the user click on his phone back button
	@Override
	public void onBackPressed()
	{
		Intent intent = new Intent(this,Tabs.class);
		startActivity(intent);
		overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
	}

 @Override
 public void onCreate(Bundle savedInstanceState) {
	 requestWindowFeature(Window.FEATURE_NO_TITLE);
  super.onCreate(savedInstanceState);
  setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.main);

  topImage= (ImageView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.topImage);
  t1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t1);

  dbHelper = new DatabaseAccess(this);
  dbHelper.open();
 // dbHelper.onCreate(database);
//dbHelper.onUpgrade(database);


//marq is the mode coming from the main menu Page
	 marq = Integer.parseInt(getIntent().getStringExtra("somekey0"));



  //Generate ListView from SQLite Database
 
  dbHelper.nberOfFlahcards();

  //newly added sep2018
  dbHelper.nberOfMarkedFlahcards();
  dbHelper.nberOfFavoriteFlahcards();
  dbHelper.nberOfWrongFlahcards();
  dbHelper.nberOfOwnFlahcards();
  dbHelper.nberOfNotStudiedFlahcards();

  displayListView();
 }

 
 public void time_Getter(){
	 
 myDate = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
	 
	 // 15:07:53
 myTime  = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) ;

 }



 
 private void displayListView() {

	 switch (marq) {

		 case 1:MyContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.revisionwhite,"Learning Mode",18);break;
		 case 2:MyContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bookwhite,"Handout Mode",18);break;
		 case 3:MyContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.testwhite,"Test mode",18);break;
		 case 4:MyContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.randomwhite,"Random Mode",18);break;
		 case 5:MyContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.diaporamawhite,"Slideshow Mode",18);break;

		 //Added sept 2018
		 case 6:MyMarkedContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markclicked,"Marked For Review",18);break;
		 case 7:MyOwnContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcardswhiteicon,"My Own",18);break;
		 case 8:MyNotStudiedContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.notseenwhite,"Non-Studied",18);break;
		 case 9:MyFavoriteContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoritewhite,"Favorite",18);break;
		 case 10:MyWrongContentTable();topTitleModification (com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.wrongwhite,"Wrong  Answered Quiz",18);break;

	 }
//"Number of Flashcards & Quizzes: "+
  //"Chapter Number: "+
  // create the adapter using the cursor pointing to the desired data 
  //as well as the layout information

	 //MyWrongContentTable();

  
  ListView listView = (ListView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.listView1);
  // Assign adapter to ListView
  listView.setAdapter(dataAdapter);

  
  listView.setOnItemClickListener(new OnItemClickListener() {
	   @Override
	   public void onItemClick(AdapterView<?> listView, View view, 
	     int position, long id) 
	   { 
	   // Get the cursor, positioned to the corresponding row in the result set
	   Cursor cursor = (Cursor) listView.getItemAtPosition(position);
		  if(cursor != null && cursor.moveToFirst()){

		//this part of the initial session tracker, it will create a new session key & save mainly starting date and time
		 time_Getter();
 		session=dbHelper.Time_and_Date_START_Saver_Method(myTime, myDate,marq,position+1,1);

	 }


		   int pos = position+1;

	   switch (marq) {
		case 1:
			 Intent i1 = new Intent(getApplicationContext(),Learning_Mode.class);

				i1.putExtra("somekey2", pos);
				i1.putExtra("somekey3", session);

				startActivity(i1);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				break;
		case 2:
			 Intent i2 = new Intent(getApplicationContext(),Handbook_New.class);
				i2.putExtra("somekey2", pos);
				i2.putExtra("somekey3", session);
				
				startActivity(i2);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
            break;
		case 3:
			 Intent i3 = new Intent(getApplicationContext(),Testing_Mode.class);
				i3.putExtra("somekey2", pos);
				i3.putExtra("somekey3", session);
				
				startActivity(i3);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			Toast.makeText(getApplicationContext(),
					"This is the Testing mode, read the question and guess in your mind the right answer" , Toast.LENGTH_LONG).show();

			break;
		case 4:

			Intent i4 = new Intent(getApplicationContext(),Random_Mode.class);
				i4.putExtra("somekey2", pos);
				i4.putExtra("somekey3", session);


				startActivity(i4);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			Toast.makeText(getApplicationContext(),
					"This is the Random mode, Cards will be displayed randomly - not in the order way!" , Toast.LENGTH_SHORT).show();

			break;
		case 5:
			 Intent i5 = new Intent(getApplicationContext(),Slideshow_Mode.class);
				i5.putExtra("somekey2", pos);
				i5.putExtra("somekey3", session);
				
				startActivity(i5);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);Toast.makeText(getApplicationContext(),
				"This is the Slideshow mode, Cards will be displayed automatically every 20 seconds \n There's no need to any action for your good side." , Toast.LENGTH_LONG).show();
			break;



			// This is for Marked activity it passes through CursorAdapterActivity (table of content)
		   case 6:
			   if(dbHelper.getNumberOfMarkedFlashcards(pos)==0||dbHelper.getNumberOfMarkedFlashcards(pos)==-1){
				   Toast.makeText(getApplicationContext(),
						   " This chapter/section doesn't contain any  marked for review flashcards.", Toast.LENGTH_SHORT).show();

	   				} else {

				   Intent i6 = new Intent(getApplicationContext(), MarkedActivity.class);
				   i6.putExtra("somekey2", pos);
				   i6.putExtra("somekey3", session);
				   i6.putExtra("somekey7", marq);

				   startActivity(i6);
				   overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				   Toast.makeText(getApplicationContext(),
						   " This mode displays only your marked for review flashcards.", Toast.LENGTH_SHORT).show();
			   } break;



		   // This is for Marked activity it passes through CursorAdapterActivity (table of content)

		   //this is for own to be completed - see how to manage the Marked activity class
		   case 7:

			   if(dbHelper.getNumberOfOwnFlashcards(pos)==0||dbHelper.getNumberOfOwnFlashcards(pos)==-1){
				   Toast.makeText(getApplicationContext(),
						   " This chapter/section doesn't contain your own flashcards.", Toast.LENGTH_SHORT).show();

			   } else {
				   Intent i7 = new Intent(getApplicationContext(), MarkedActivity.class);
				   i7.putExtra("somekey2", pos);
				   i7.putExtra("somekey3", session);
				   i7.putExtra("somekey7", marq);
				   startActivity(i7);
				   overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				   Toast.makeText(getApplicationContext(),
						   " This mode displays only your Owen flashcards.", Toast.LENGTH_SHORT).show();
			   }break;

		   //this is for not studied to be done it's not completed
		   case 8:

			   if(dbHelper.getNumberOfNotStudiedFlashcards(pos)==0||dbHelper.getNumberOfNotStudiedFlashcards(pos)==-1){
				   Toast.makeText(getApplicationContext(),
						   " This chapter/section doesn't contain any  non-studied flashcards.", Toast.LENGTH_SHORT).show();

			   } else {
				   Intent i8 = new Intent(getApplicationContext(), MarkedActivity.class);
				   i8.putExtra("somekey2", pos);
				   i8.putExtra("somekey3", session);
				   i8.putExtra("somekey7", marq);
				   startActivity(i8);
				   overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				   Toast.makeText(getApplicationContext(),
						   " This mode displays only your non-studied flashcards.", Toast.LENGTH_SHORT).show();
			   } break;

		   //this is for favorite to be done it's not completed

		   case 9:
			   if(dbHelper.getNumberOfFavoriteFlashcards(pos)==0||dbHelper.getNumberOfFavoriteFlashcards(pos)==-1){
				   Toast.makeText(getApplicationContext(),
						   " This chapter/section doesn't contain any  of your Favorite flashcards.", Toast.LENGTH_SHORT).show();

			   } else {
				   Intent i9 = new Intent(getApplicationContext(), MarkedActivity.class);
				   i9.putExtra("somekey2", pos);
				   i9.putExtra("somekey3", session);
				   i9.putExtra("somekey7", marq);
				   startActivity(i9);
				   overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				   Toast.makeText(getApplicationContext(),
						   " This mode displays only your Favorite flashcards.", Toast.LENGTH_SHORT).show();
			   } break;

		   case 10:
			   if(dbHelper.getNumberOfWrongFlashcards(pos)==0||dbHelper.getNumberOfWrongFlashcards(pos)==-1){
				   Toast.makeText(getApplicationContext(),
						   " This chapter/section doesn't contain any Wrong Answered Quiz.", Toast.LENGTH_SHORT).show();

			   } else {
				   Intent i10 = new Intent(getApplicationContext(), MarkedActivity.class);
				   i10.putExtra("somekey2", pos);
				   i10.putExtra("somekey3", session);
				   i10.putExtra("somekey7", marq);
				   startActivity(i10);
				   overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				   Toast.makeText(getApplicationContext(),
						   " This mode displays only  the flashcards that you answered wrongly.", Toast.LENGTH_SHORT).show();
			   } break;
		
		}


	   
	  
	   
	}
	  });


}
 public void toastShort(String str){ Toast.makeText(getApplicationContext(),
	     str , Toast.LENGTH_SHORT).show();
 }
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}


	public void  MyContentTable() //this method populate the content of the list view

	{Cursor cursor = dbHelper.fetchAllChapters();
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_FLASHCARDS
		};
		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0);
	}

	public void  MyMarkedContentTable() //this method change the content of the main menu (listview)

	{Cursor cursor = dbHelper.fetchAllChapters(); //************this is is the error fetchAllChapters()
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_MARKED};

		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0);
	}


	public void  MyFavoriteContentTable() //this method change the content of the main menu (listview)
	{Cursor cursor = dbHelper.fetchAllChapters();
	//************ all the column comes from fetchAllChapters in the database access - this is is the error fetchAllChapters()
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_FAVORITE};

		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id, //from the listview in the main menu
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0); }


	public void  MyOwnContentTable() //this method change the content of the main menu (listview)
	{Cursor cursor = dbHelper.fetchAllChapters();
		//************ all the column comes from fetchAllChapters in the database access - this is is the error fetchAllChapters()
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_OWN};

		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id, //from the listview in the main menu
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0); }

	public void  MyWrongContentTable() //this method change the content of the main menu (listview)
	{Cursor cursor = dbHelper.fetchAllChapters();
		//************ all the column comes from fetchAllChapters in the database access - this is is the error fetchAllChapters()
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_WRONG};

		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id, //from the listview in the main menu
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0); }


	public void  MyNotStudiedContentTable() //this method change the content of the main menu (listview)
	{Cursor cursor = dbHelper.fetchAllChapters();
		//************ all the column comes from fetchAllChapters in the database access - this is is the error fetchAllChapters()
		String[] columns = new String[]{
				DatabaseAccess.KEY_ChapID,
				DatabaseAccess.KEY_TITLE,
				DatabaseAccess.KEY_NBER_NOT_STUDIED};

		int[] to = new int[]{
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapter_id, //from the listview in the main menu
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chapetr_title,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.nber_of_flashcards};
		dataAdapter = new SimpleCursorAdapter(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.chapter_list_content, cursor, columns, to, 0); }


	public void topTitleModification (int RdrawableImageName, String titlecontent, int textSize ){
		//	LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( 0, LayoutParams.MATCH_PARENT, weight);
		//	topImage.setLayoutParams(param);
			topImage.setImageResource(RdrawableImageName);
		topImage.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.circle5);
			//t1.setBackgroundResource(R.drawable.med2white);
			t1.setText(titlecontent +" Flashcards List");
			t1.setTextSize(textSize);
			t1.setTextColor(Color.parseColor("#ffffff"));


		}

}


  
  


 
