package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Apps extends Activity {

	TextView t1, im1, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11,
			tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22,
			tv23, tv24, tv25, tv26, tv27, tv28, tv29, tv30, tv31, tv32, tv33,
			tvx;
	Typeface tf;
	ImageButton i1, i2, i3, i4, i5,i6, ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8,
			ib9, ib10, ib11, ib12, ib13, ib14, ib15, ib16, ib17, ib18, ib19,
			ib20, ib21, ib22, ib23, ib24, ib25, ib26, ib27, ib28, ib29, ib30,
			ib31, ib32, ib33, ibx;
	String application, market, email, market_name,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33;


	//back button : this method is executed when the user click on his phone back button


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.apps);

		i1 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i1);
		i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2);
		i3 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i3);
		i4 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i4);
		i5 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i5);
		i6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i6);

		ib1 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib1);
		ib2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib2);
		ib3 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib3);
		ib4 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib4);
		ib5 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib5);
		ib6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib6);
		ib7 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib7);
		ib8 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib8);
		ib9 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib9);
		ib10 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib10);
		ib11 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib11);
		ib12 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib12);
		ib13 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib13);
		ib14 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib14);
		ib15 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib15);
		ib16 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib16);
		ib17 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib17);
		ib18 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib18);
		ib19 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib19);
		ib20 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib20);
		ib21 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib21);
		ib22 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib22);
		ib23 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib23);
		ib24 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib24);
		ib25 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib25);
		ib26 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib26);
		ib27 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib27);
		ib28 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib28);
		ib29 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib29);
		ib30 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib30);
		ib31 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib31);
		ib32 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib32);
		ib33 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib33);
		ibx = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ibx);

		tv1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv1);
		tv2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv2);
		tv3 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv3);
		tv4 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv4);
		tv5 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv5);
		tv6 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv6);
		tv7 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv7);
		tv8 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv8);
		tv9 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv9);
		tv10 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv10);
		tv11 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv11);
		tv12 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv12);
		tv13 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv13);
		tv14 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv14);
		tv15 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv15);
		tv16 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv16);
		tv17 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv17);
		tv18 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv18);
		tv19 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv19);
		tv20 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv20);
		tv21 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv21);
		tv22 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv22);
		tv23 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv23);
		tv24 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv24);
		tv25 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv25);
		tv26 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv26);
		tv27 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv27);
		tv28 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv28);
		tv29 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv29);
		tv30 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv30);
		tv31 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv31);
		tv32 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv32);
		tv33 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv33);
		tvx = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tvx);
		im1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.im1);

		LinearLayout ll1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ll1);


			ll1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.splash_shape);


		application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
		market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
		market_name = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market_name);
		email = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.email);
		
		x1= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x1);
		x2= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x2);
		x3= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x3);
		x4= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x4);
		x5= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x5);
		x6= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x6);
		x7= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x7);
		x8= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x8);
		x9= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x9);
		x10= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x10);
		x11= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x11);
		x12= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x12);
		x13= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x13);
		x14= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x14);
		x15= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x15);
		x16= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x16);
		x17= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x17);
		x18= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x18);
		x19= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x19);
		x20= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x20);
		x21= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x21);
		x22= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x22);
		x23= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x23);
		x24= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x24);
		x25= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x25);
		x26= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x26);
		x27= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x27);
		x28= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x28);
		x29= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x29);
		x30= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x30);
		x31= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x31);
		x32= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x32);
		x33= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x33);
		
		
	tv1.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv1));
	tv2.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv2));
	tv3.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv3));
	tv4.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv4));
	tv5.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv5));
	tv6.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv6));
	tv7.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv7));
	tv8.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv8));
	tv9.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv9));
	tv10.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv10));
	tv11.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv11));
	tv12.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv12));
	tv13.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv13));
	tv14.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv14));
	tv15.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv15));
	tv16.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv16));
	tv17.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv17));
	tv18.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv18));
	tv19.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv19));
	tv20.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv20));
	tv21.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv21));
	tv22.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv22));
	tv23.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv23));
	tv24.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv24));
	tv25.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv25));
	tv26.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv26));
	tv27.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv27));
	tv28.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv28));
	tv29.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv29));
	tv30.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv30));
	tv31.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv1));
	tv32.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv2));
	tv33.setText(getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.tv3));
		
		
		
		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/barbie.ttf");
		tv1.setTypeface(tf);
		tv2.setTypeface(tf);
		tv3.setTypeface(tf);
		tv4.setTypeface(tf);
		tv5.setTypeface(tf);
		tv6.setTypeface(tf);
		tv7.setTypeface(tf);
		tv8.setTypeface(tf);
		tv9.setTypeface(tf);
		tv10.setTypeface(tf);
		tv11.setTypeface(tf);
		tv12.setTypeface(tf);
		tv13.setTypeface(tf);
		tv14.setTypeface(tf);
		tv15.setTypeface(tf);
		tv16.setTypeface(tf);
		tv17.setTypeface(tf);
		tv18.setTypeface(tf);
		tv19.setTypeface(tf);
		tv20.setTypeface(tf);
		tv21.setTypeface(tf);
		tv22.setTypeface(tf);
		tv23.setTypeface(tf);
		tv24.setTypeface(tf);
		tv25.setTypeface(tf);
		tv26.setTypeface(tf);
		tv27.setTypeface(tf);
		tv28.setTypeface(tf);
		tv29.setTypeface(tf);
		tv30.setTypeface(tf);
		tv31.setTypeface(tf);
		tv32.setTypeface(tf);
		tv33.setTypeface(tf);
		tvx.setTypeface(tf);

		Animation anim1 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinhorizdown);
		im1.startAnimation(anim1);

		Animation anb1 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib1.startAnimation(anb1);
		Animation anb2 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv1.startAnimation(anb2);
		Animation anb3 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib2.startAnimation(anb3);
		Animation anb4 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv2.startAnimation(anb4);
		Animation anb5 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib3.startAnimation(anb5);
		Animation anb6 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv3.startAnimation(anb6);
		Animation anb7 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib4.startAnimation(anb7);
		Animation anb8 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv4.startAnimation(anb8);
		Animation anb9 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib5.startAnimation(anb9);
		Animation anb10 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv5.startAnimation(anb10);
		Animation anb11 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib6.startAnimation(anb11);
		Animation anb12 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv6.startAnimation(anb12);
		Animation anb13 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib7.startAnimation(anb13);
		Animation anb14 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv7.startAnimation(anb14);
		Animation anb15 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib8.startAnimation(anb15);
		Animation anb16 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv8.startAnimation(anb16);
		Animation anb17 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib9.startAnimation(anb17);
		Animation anb18 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv9.startAnimation(anb18);
		Animation anb19 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib10.startAnimation(anb19);
		Animation anb20 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv10.startAnimation(anb20);
		Animation anb21 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib11.startAnimation(anb21);
		Animation anb22 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv11.startAnimation(anb22);
		Animation anb23 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib12.startAnimation(anb23);
		Animation anb24 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv12.startAnimation(anb24);
		Animation anb25 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib13.startAnimation(anb25);
		Animation anb26 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv13.startAnimation(anb26);
		Animation anb27 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib14.startAnimation(anb27);
		Animation anb28 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv14.startAnimation(anb28);
		Animation anb29 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib15.startAnimation(anb29);
		Animation anb30 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv15.startAnimation(anb30);

		Animation anba1 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib16.startAnimation(anba1);
		Animation anba2 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv16.startAnimation(anba2);
		Animation anba3 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib17.startAnimation(anba3);
		Animation anba4 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv17.startAnimation(anba4);
		Animation anba5 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib18.startAnimation(anba5);
		Animation anba6 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv18.startAnimation(anba6);
		Animation anba7 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib19.startAnimation(anba7);
		Animation anba8 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv19.startAnimation(anba8);
		Animation anba9 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib20.startAnimation(anba9);
		Animation anba10 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv20.startAnimation(anba10);
		Animation anba11 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib21.startAnimation(anba11);
		Animation anba12 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv21.startAnimation(anba12);
		Animation anba13 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib22.startAnimation(anba13);
		Animation anba14 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv22.startAnimation(anba14);
		Animation anba15 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib23.startAnimation(anba15);
		Animation anba16 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv23.startAnimation(anba16);
		Animation anba17 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib24.startAnimation(anba17);
		Animation anba18 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv24.startAnimation(anba18);
		Animation anba19 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib25.startAnimation(anba19);
		Animation anba20 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv25.startAnimation(anba20);
		Animation anba21 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib26.startAnimation(anba21);
		Animation anba22 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv26.startAnimation(anba22);
		Animation anba23 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib27.startAnimation(anba23);
		Animation anba24 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv27.startAnimation(anba24);
		Animation anba25 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib28.startAnimation(anba25);
		Animation anba26 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv28.startAnimation(anba26);
		Animation anba27 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib29.startAnimation(anba27);
		Animation anba28 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv29.startAnimation(anba28);
		Animation anba29 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib30.startAnimation(anba29);
		Animation anba30 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv30.startAnimation(anba30);

		Animation anba31 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib31.startAnimation(anba31);
		Animation anba32 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv31.startAnimation(anba32);
		Animation anba33 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib32.startAnimation(anba33);
		Animation anba34 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv32.startAnimation(anba34);
		Animation anba35 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ib33.startAnimation(anba35);
		Animation anba36 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tv33.startAnimation(anba36);
		Animation anba37 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
		ibx.startAnimation(anba37);
		Animation anba38 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
		tvx.startAnimation(anba38);

		LinearLayout l1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.l1);
		Animation anl1 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinhorizontal);
		l1.startAnimation(anl1);

		Animation ani1 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i1.startAnimation(ani1);
		Animation ani2 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i2.startAnimation(ani2);
		Animation ani3 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i3.startAnimation(ani3);
		Animation ani4 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i4.startAnimation(ani4);
		Animation ani5 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i5.startAnimation(ani5);

		im1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(market));
				startActivity(i);
			}
		});

		ib1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x1));
				startActivity(i);
			}
		});
		tv1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x1));
				startActivity(i);
			}
		});
		ib2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x2));
				startActivity(i);
			}
		});

		tv2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x2));
				startActivity(i);

			}
		});

		ib3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x3));
				startActivity(i);
			}
		});
		tv3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x3));

				startActivity(i);
			}
		});

		ib4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x4));
				startActivity(i);
			}
		});
		tv4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x4));
				startActivity(i);
			}
		});

		ib5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x5));

				startActivity(i);
			}
		});
		tv5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x5));

				startActivity(i);
			}
		});

		ib6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x6));
				startActivity(i);
			}
		});
		tv6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x6));
				startActivity(i);
			}
		});

		ib7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x7));
				startActivity(i);
			}
		});
		tv7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x7));
				startActivity(i);
			}
		});

		ib8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x8));
				startActivity(i);
			}
		});
		tv8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x8));
				startActivity(i);
			}
		});

		ib9.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x9));
				startActivity(i);
			}
		});
		tv9.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x10));
				startActivity(i);
			}
		});
		ib10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x10));
				startActivity(i);
			}
		});
		tv10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x11));
				startActivity(i);
			}
		});
		ib11.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x11));
				startActivity(i);
			}
		});
		tv11.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x11));
				startActivity(i);
			}
		});
		ib12.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x12));
				startActivity(i);
			}
		});

		tv12.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x12));
				startActivity(i);
			}
		});

		ib13.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x13));
				startActivity(i);
			}
		});
		tv13.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x13));
				startActivity(i);
			}
		});

		ib14.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x14));
				startActivity(i);
			}
		});
		tv14.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x14));
				startActivity(i);
			}
		});

		ib15.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x15));
				startActivity(i);
			}
		});
		tv15.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x15));
				startActivity(i);
			}
		});

		ib16.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(x16));
				startActivity(i);
			}
		});
		tv16.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(x16));
				startActivity(i);
			}
		});

		ib17.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x17));
				startActivity(i);
			}
		});
		tv17.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x17));
				startActivity(i);
			}
		});

		ib18.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x18));

				startActivity(i);
			}
		});
		tv18.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x18));

				startActivity(i);
			}
		});

		ib19.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x19));
				startActivity(i);
			}
		});
		tv19.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x20));
				startActivity(i);
			}
		});
		ib20.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x20));

				startActivity(i);
			}
		});
		tv20.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x20));
				startActivity(i);
			}
		});
		ib21.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x21));
				startActivity(i);
			}
		});
		tv21.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x21));

				startActivity(i);
			}
		});
		ib22.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x22));

				startActivity(i);
			}
		});

		tv22.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x22));

				startActivity(i);
			}
		});

		ib23.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x23));
				startActivity(i);
			}
		});
		tv23.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x23));
				startActivity(i);
			}
		});

		ib24.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x24));
				startActivity(i);
			}
		});
		tv24.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x24));
				startActivity(i);
			}
		});

		ib25.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x25));

				startActivity(i);
			}
		});
		tv25.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x25));

				startActivity(i);
			}
		});

		ib26.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x26));

				startActivity(i);
			}
		});
		tv26.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x26));

				startActivity(i);
			}
		});

		ib27.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x27));

				startActivity(i);
			}
		});
		tv27.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x27));
				startActivity(i);
			}
		});

		ib28.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x28));
				startActivity(i);
			}
		});
		tv28.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(x28));
				startActivity(i);
			}
		});

		ib29.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x29));
				startActivity(i);
			}
		});
		tv29.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x29));
				startActivity(i);
			}
		});
		ib30.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x30));
				startActivity(i);
			}
		});
		tv30.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x30));
				startActivity(i);
			}
		});
		ib31.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(x31));
				startActivity(i);
			}
		});
		tv31.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(x31));
				startActivity(i);
			}
		});
		ib32.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x32));
				startActivity(i);
			}
		});
		tv32.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x32));
				startActivity(i);
			}
		});

		ib33.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x33));
				startActivity(i);
			}
		});
		tv33.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(x33));
				startActivity(i);
			}
		});

		ibx.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(market));
				startActivity(i);
			}
		});
		tvx.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(market));
				startActivity(i);
			}
		});

		i1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] to = { email };

				Intent in = new Intent(Intent.ACTION_SEND);
				in.putExtra(Intent.EXTRA_EMAIL, to);

				in.putExtra(Intent.EXTRA_SUBJECT, "Regarding ur app:"
						+ application);
				in.putExtra(Intent.EXTRA_TEXT,
						"Hi there, \n\n Just a quick comment about your application: \""
								+ application + "from: " + market_name + "\n\n");
				in.setType("message/rfc822");
				startActivity(in);
			}
		});
		i2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in1 = new Intent(Apps.this, Info.class);
				startActivity(in1);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			}
		});

		i3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

			}
		});
		i4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

			}
		});

		i5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in1 = new Intent(Apps.this, Tabs.class);
				startActivity(in1);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			}
		});
		i6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri
						.parse(market));
				startActivity(i);
			}
		});
	}

	@Override
	public void onBackPressed(){


		Intent intent = new Intent(this,Tabs.class);
		startActivity(intent);
		overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
		//finish();

	}

	@Override
	public void onDestroy(){

		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}


}