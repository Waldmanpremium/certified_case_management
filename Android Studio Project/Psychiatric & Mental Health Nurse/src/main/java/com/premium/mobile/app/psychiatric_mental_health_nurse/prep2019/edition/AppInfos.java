package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppInfos extends Activity {

	TextView t1, im1, tv1, tv2, tv3, tv4,tv4a, tv5, tv6, tv7, tv8, tv9, tv10, tv11,
			tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22,
			tv23, tv24, tv25, tv26, tv27, tv28, tv29, tv30, tv31, tv32, tv33,
			tvx;
	Typeface tf;
	ImageButton i1, i2, i3, i4, i5, ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8,
			ib9, ib10, ib11, ib12, ib13, ib14, ib15, ib16, ib17, ib18, ib19,
			ib20, ib210,ib211, ib220, ib230, ib240, ib250, ib260, ib270,  ib221, ib231, ib241,ib242, ib251, ib261, ib271,ib28, ib29, ib30,
			ib31, ib32, ib33, ibx;
	String application, market, email, market_name,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33;


	//back button : this method is executed when the user click on his phone back button


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.appinfos);

		i1 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i1);
		i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2);
		i3 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i3);
		i4 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i4);
		i5 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i5);

		ib1 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib1);
		ib2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib2);
		ib3 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib3);
		ib4 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib4);
		ib5 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib5);
		ib6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib6);
		ib7 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib7);
		ib8 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib8);
		ib9 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib9);
		ib10 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib10);
		ib11 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib11);
		ib12 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib12);
		ib13 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib13);
		ib14 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib14);
		ib15 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib15);
		ib16 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib16);
		ib17 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib17);
		ib18 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib18);
		ib19 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib19);
		ib20 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib20);
		ib210 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib210);
		ib220 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib220);
		ib230 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib230);
		ib240 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib240);
		ib250 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib250);
		ib260 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib260);
		ib270 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib270);
		ib211 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib211);
		ib221 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib221);
		ib231 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib231);
		ib241 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib241);
		ib242 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib242);
		ib251 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib251);
		ib261 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib261);
		ib271 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib271);
		ib28 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib28);
		ib29 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib29);
		ib30 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib30);
		ib31 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib31);
		ib32 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib32);
		ib33 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ib33);
		ibx = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ibx);

		tv1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv1);
		tv2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv2);
		tv3 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv3);
		tv4 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv4);
		tv4a = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv4a);
		tv5 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv5);
		tv6 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv6);
		tv7 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv7);
		tv8 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv8);
		tv9 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv9);
		tv10 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv10);
		tv11 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv11);
		tv12 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv12);
		tv13 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv13);
		tv14 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv14);
		tv15 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv15);
		tv16 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv16);
		tv17 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv17);
		tv18 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv18);
		tv19 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv19);
		tv20 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv20);
		tv21 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv21);
		tv22 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv22);
		tv23 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv23);
		tv24 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv24);
		tv25 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv25);
		tv26 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv26);
		tv27 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv27);
		tv28 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv28);
		tv29 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv29);
		tv30 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv30);
		tv31 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv31);
		tv32 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv32);
		tv33 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tv33);
		tvx = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.tvx);
		im1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.im1);

		LinearLayout ll1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ll1);

		/*Random r1 = new Random();
		int number = r1.nextInt(4);
		switch (number) {
		case 0:
			ll1.setBackgroundResource(R.drawable.bmenu1);
			break;
		case 1:
			ll1.setBackgroundResource(R.drawable.bmenu2);
			break;
		case 2:
			ll1.setBackgroundResource(R.drawable.bmenu2);
			break;
		case 3:
			ll1.setBackgroundResource(R.drawable.bsplash5);
			break;
		}*/

		application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
		market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
		market_name = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market_name);
		email = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.email);
		
		x1= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x1);
		x2= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x2);
		x3= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x3);
		x4= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x4);
		x5= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x5);
		x6= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x6);
		x7= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x7);
		x8= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x8);
		x9= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x9);
		x10= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x10);
		x11= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x11);
		x12= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x12);
		x13= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x13);
		x14= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x14);
		x15= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x15);
		x16= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x16);
		x17= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x17);
		x18= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x18);
		x19= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x19);
		x20= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x20);
		x21= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x21);
		x22= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x22);
		x23= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x23);
		x24= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x24);
		x25= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x25);
		x26= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x26);
		x27= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x27);
		x28= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x28);
		x29= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x29);
		x30= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x30);
		x31= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x31);
		x32= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x32);
		x33= getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.x33);
		
		
	tv1.setText("This widget panel displays the last test session result. When you click on this widget you will move to the Infographic presentations. You should have practiced at least one test mode session to activate the infographics.");
	tv2.setText("This widget panel displays the number of wrongly answered flashcards. When you click on this widget you will move to the wrong responded questions that you missed during the test mode sessions. You should have practiced at least one test mode session to activate the wrong responded questions list.");
	tv3.setText("This widget panel displays the number of cards that you mentioned as favorite. When you click on this widget you will move to the list of your favorite cards. You should have chosen at least one favorite flashcard in order to activate this function.");
	tv4.setText("This widget panel displays the number of Marked for review (Flagged) flashcards. When you click on this widget you will move to the list of your Flagged for review cards. You should have flagged at least one flashcard in order to activate this function.");
	tv4a.setText("When you click on this widget you will move to the data analytics of your studies so far.");
	tv5.setText("When you click on this widget you will move to the last study session (last flashcard studied). You should have practiced at least one study session in order to activate this function.");
	tv6.setText("This widget panel displays the number of flashcards that you have added to this app (your own study notes). When you click on this widget you will move to the list of your own cards. You should have added and saved at least one flashcard in order to activate this function.");
	tv7.setText("This widget panel displays the number of non-studied flashcards. When you click on this widget you will move to the list of the non-studied yet cards.");
	tv8.setText("When you click on this widget you will move to the Search by Topic page in order to fetch flashcards by the keywords that you add.");
	tv9.setText("This is the Learning-Mode study session button. When you click on this widget you will move to the Learning-Mode study session. In this mode the question and answer are displayed.");
	tv10.setText("This is the Handout-Mode study session button. When you click on this widget you will move to the Handout-Mode study session. In this Study mode all the flashcards in the chosen chapter are displayed all together (like you are reading a book). You can listen to all the cards if you activate the speaker (recommended when driving or riding).");
	tv11.setText("This is the Test-Mode study session button. When you click on this widget you will move to the Test-Mode study session. In this Study mode only questions that are displayed, you should guess in your mind the right answer, after that you should click on the answer button then click either on correct button, or wrong button if you have missed the answer.");
	tv12.setText("This is the Random-Mode study session button. When you click on this widget you will move to the random-Mode study session. In this Study mode flashcards (questions and answers) are displayed randomly and not in the sequential order.");
	tv13.setText("This is the Slideshow-Mode study session button. When you click on this widget you will move to the Slideshow-Mode study session. In this hands-free mode, flashcards are displayed automatically without need to your intervention. Every 20 seconds a new card is displayed. You can listen to all the cards if you activate the speaker (recommended when driving or riding).");
	tv14.setText("This is the \"Exam taking Tips\" button. When you click on this widget you will move to the Exam taking Tips and Tricks page where you will find a bunch of strongly recommended tips and  proven strategies that helps you succeed any MCQ exam.");
	tv15.setText("This is the \"Related apps\" button. When you click on this widget you will move to the Google play store page where you will find a bunch of strongly recommended self-learning apps with proven content that will assist you in succeeding your coming exams.");
	tv16.setText("This is the \"App Features\" button. When you click on this widget you will see this page ;) .");
	tv17.setText("This is the \"Send Us Your Feedback\" button. When you click on this widget you will have the opportunity to share with us your kind feedback, suggestions, improvement opportunities or just to say Hello or Thank You Guys.");
	tv18.setText("This is the \"Share This App\" button. When you click on this widget you will have the opportunity to share this app with your friend, collogues, teammates or followers. By sharing this app you are building a better knowledge society and enhancing the knowledge sharing culture.");
	tv19.setText("This is the \"Go-to Question#\" button. before clicking on this widget, you have to write the specific flashcard number that you want to move to, in the edit-text zone. For instance, if you are in the card #23 and want to go to card #67, you should write 67 in the edit text zone (that displays 23) and then click the Go-to button.");
	tv20.setText("This is the \"Seen\" button. This widget displays the number of times the concerned flashcards have been seen (studied).");
	tv21.setText("This is the \"Add Your Card\" button. When you click on this widget you will have the opportunity to add your own study notes. You will be asked to write down the question first then you should click on \"Save Question\" button, then you have to add the answer and save it by hitting \"Save Answer\" Button. The new card will be added at the end of the chapter" );
	tv22.setText("This is the \"Add Your Comment\" button. When you click on this widget you will have the opportunity to add your comment to the flashcard. You will be asked to write down the comment then you should click on \"Save Comment\" button. The new comment will be displayed at the bottom of the flashcard.");
	tv23.setText("This is the \"Edit/Replace\" button. When you click on this widget you will have the opportunity to Edit or replace the flashcard. You will be asked to edit/replace the question first then you should click on Save Question button, then you could edit the answer and then save it by hitting Save Answer. ");
	tv24.setText("This is the \"Right/Wrong\" button. If you have tested the flashcard in test mode, if you responded the right answer the green color will be activated, if you responded wrongly the red cross will be activated automatically. The gray widget means that the flashcard hasn’t be tested yet. ");
	tv25.setText("This is the \"Add to my Favorite Quiz\" button. When you click on this widget the displayed flashcard will be added to your favorite Quiz and the button will stay activated.");
	tv26.setText("This is the \"Add to Flagged Quiz\" button. When you click on this widget the displayed flashcard will be added to your marked for review Quizzes and the button will stay activated.");
	tv27.setText("This is the \"Speaker\" button. When you click on this widget the speaker will be on and you could listen to the displayed flashcard(s). When you click again the speaker will be shutdown.");
	tv28.setText("This is the \"Save Question\" button. When you click on \"Add a new Flashcard\" or \"Edit\"  button, this widget will be activated. When you save the edited question a confirmation message will popup.");
	tv29.setText("This is the \"Save Answer\" button. After saving the edited question, this widget will be activated and then you could save the edited answer. When you save the Answer a confirmation message will popup.");
	tv30.setText("This is the Save Comment button. When you click on Add a new comment to a flashcard this button will be activated. Click on it to save your new added comment. When you save the Comment a confirmation message will popup.");
	/*tv31.setText(getResources().getString(R.string.tv1));
	tv32.setText(getResources().getString(R.string.tv2));
	tv33.setText(getResources().getString(R.string.tv3));*/



		LinearLayout l1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.l1);
		Animation anl1 = AnimationUtils.loadAnimation(this,
				com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinhorizontal);
		l1.startAnimation(anl1);

		Animation ani1 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i1.startAnimation(ani1);
		Animation ani2 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i2.startAnimation(ani2);
		Animation ani3 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i3.startAnimation(ani3);
		Animation ani4 = AnimationUtils
				.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i4.startAnimation(ani4);
		Animation ani5 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i5.startAnimation(ani5);



		i1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] to = { email };

				Intent in = new Intent(Intent.ACTION_SEND);
				in.putExtra(Intent.EXTRA_EMAIL, to);

				in.putExtra(Intent.EXTRA_SUBJECT, "Regarding ur app:"
						+ application);
				in.putExtra(Intent.EXTRA_TEXT,
						"Hi there, \n\n Just a quick comment about your application: \""
								+ application + "from: " + market_name + "\n\n");
				in.setType("message/rfc822");
				startActivity(in);
			}
		});
		i2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in1 = new Intent(AppInfos.this, Info.class);
				startActivity(in1);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			}
		});

		i3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});
		i4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});

		i5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in1 = new Intent(AppInfos.this, Tabs.class);
				startActivity(in1);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
			}
		});

	}
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

	@Override
	public void onDestroy(){

		super.onDestroy();
	}

	@Override
	public void onBackPressed(){


		Intent intent = new Intent(this,Tabs.class);
		startActivity(intent);
		overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
		//finish();

	}

}