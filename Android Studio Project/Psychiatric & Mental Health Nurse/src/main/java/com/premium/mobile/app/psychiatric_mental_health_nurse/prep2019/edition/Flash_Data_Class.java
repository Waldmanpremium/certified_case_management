package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

public class Flash_Data_Class {

    //private variables
    int _flash_id;
    int chap_id;
    String _question;
    String _answer;
    String _comment;

    // Empty constructor
    public Flash_Data_Class(){

    }
    
 // constructor
    public Flash_Data_Class(int id){
        this._flash_id = id;
       
    }
    // constructor
    public Flash_Data_Class(int id, String question, String answer){
        this._flash_id = id;
        this._question = question;
        this._answer = answer;
    }

    // constructor
    public Flash_Data_Class(String question, String answer){
        this._question = question;
        this._answer = answer;
    }
    // getting ID
    public int getID(){
        return this._flash_id;
    }

    // setting id
    public void setID(int id){
        this._flash_id = id;
    }

    // getting question
    public String getQuestion(){
        return this._question;
    }

    // setting question
    public void setQuestion(String question){
        this._question = question;
    }

    // getting phone number
    public String getAnswer(){
        return this._answer;
    }

    // setting phone number
    public void setAnswer(String answer){
        this._answer = answer;
    }

}
