package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;


public class Handbook_New extends ParentClass implements TextToSpeech.OnInitListener{
    public DatabaseAccess dbHelper;
    public SimpleCursorAdapter dataAdapter;
    public SQLiteDatabase database;

    TextView flashNbers,t2, q11, ans11, comment;


    //Newely added september 2018
    TextToSpeech tts;
    boolean zx=false;
    boolean boospeak=false;


    Context context;
    String abc,allText="",flashcard,application, quest1,text1,Comment1,colorStringBackground="#00000000",colorStringText="#000000";
    int o=0,p=0,q=0, num1=0;
    Typeface typeface1;
    ImageButton i3,i4,i9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.handout_new);

        tts = new TextToSpeech(getApplicationContext(), this);
        dbHelper = new DatabaseAccess(this);
        dbHelper.open();
        dbHelper.onCreate(database);
        dbHelper.onUpgrade(database);
        title = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.title);
        //fl2= (LinearLayout) findViewById(R.id.fl2);

        context=this;
        q11 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.q11);
        t2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t2);
        ans11 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ans11);
        speaker = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.speaker);//speaker button
        i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2); //share button
        i3= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i3); // change the color of the background
        i4= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i4);  // change the font of the text
        i6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i6);  //zoomin button
        i7 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i7);   // zoomout button
        i8 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i8);   //previous chapter
        i9= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i9); //this is the home button
        i10 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i10);   // Next chapter




        //llistview = (LinearLayout) findViewById(R.id.llistview);
      //  l1 = (LinearLayout) findViewById(R.id.l1);
       // ltitle = (LinearLayout) findViewById(R.id.ltitle);


        final Context context = this;


        application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
        market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
        root = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.root_address)
                + getPackageName();
        app_version = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_version);
        iTextSize = 18;


        dbHelper = new DatabaseAccess(this);
        dbHelper.open();

        num = getIntent().getIntExtra("somekey2", 1);
        sessionNber = getIntent().getIntExtra("somekey3", 1);



        del = dbHelper.getNumberOfFlashcards(num);
        chel = dbHelper.getChap_tableCount();
        //We update the session table dtat with this method the session data
        session_Tracker();
        getChapterTitle();

        flashcard=getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.flashcard_name);
        abc="";


        num1=1;


        Typeface tf2=Typeface
                .createFromAsset(getAssets(),"fonts/gothic.ttf");
        typeface1=tf2;
        fillOut_TextView( num,num1);




        //********  Actions to be executed when the button is clicked by the inherited class***********
//*************************************************************************************************




// This is the share Button
        i2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillOut_TextView( num,num1);
                shareMethod();}
        });


// Activate and deactivate button
        // Activate and deactivate button
        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tts!=null){
                    tts.stop();
                }
               // fillOut_TextView( num,num1);
                abc=t2.getText().toString();
                if (!zx){
                    speech(abc);
                    zx=true;
                  //  Log.v(TAG, "***********************!!!!!!!********************\n"+toSpeak);
                    speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakermuteblack);
                } else {
                    tts.stop(); // this stops only
                //tts.shutdown(); this method kills the process so you should initiate it back after using it
                    zx=false;
                    speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);}
            }});

        // This is the change of the color of the background
        i3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                o++;

                switch (o) {
                    case 1:
                        colorStringBackground="#000000";
                        colorStringText="#FFFFFF";
                        fillOut_TextView( num,num1);

                        Toast.makeText(getApplicationContext(),
                                "Night Mode Activated!", Toast.LENGTH_SHORT).show();

                        break;
                    case 2:
                        colorStringBackground="#F5A9E1";
                        colorStringText="#000000";
                        fillOut_TextView( num,num1);
                        Toast.makeText(getApplicationContext(),
                                "Night Mode Deactivated!", Toast.LENGTH_SHORT)
                                .show();

                        break;
                    case 3:

                        colorStringBackground="#30A9D0F5";
                        fillOut_TextView( num,num1);

                        break;
                    case 4:
                        colorStringBackground="#81F7D8";
                        fillOut_TextView( num,num1);
                        break;
                    case 5:
                        colorStringBackground="#A9F5A9";
                        fillOut_TextView( num,num1);

                        break;
                    case 6:
                        colorStringBackground="#D8F781";
                        fillOut_TextView( num,num1);
                        break;
                    case 7:

                        colorStringBackground="#F3F781";
                        fillOut_TextView( num,num1);

                        break;
                    case 8:

                        colorStringBackground="#F7BE81";
                        fillOut_TextView( num,num1);
                        break;
                    case 9:

                        colorStringBackground="#F5A9A9";
                        fillOut_TextView( num,num1);
                        Toast.makeText(getApplicationContext(),
                                "You look like playing with this app not studing ;)", Toast.LENGTH_SHORT)
                                .show();

                        break;
                    case 10:
                        colorStringBackground="#D0A9F5";
                        fillOut_TextView( num,num1);
                        
                        break;
                    case 11:
                        colorStringBackground="#e6ccff";
                        fillOut_TextView( num,num1);

                        break;
                    case 12:
                        colorStringBackground="#8000ff";
                        colorStringText="#FFFFFF";
                        fillOut_TextView( num,num1);
                        ;

                        break;
                    case 13:
                        colorStringBackground="##ffcce6";
                        colorStringText="#000000";
                        fillOut_TextView( num,num1);


                    default:

                        colorStringBackground="#ff1a8c";
                        colorStringText="#FFFFFF";
                        fillOut_TextView( num,num1);

                        Toast.makeText(getApplicationContext(),
                                "Are you studing or playing with this app! :)",
                                Toast.LENGTH_SHORT).show();

                        o = 0;

                }}
        });


        // change the font of the text
        i4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {p++;

                switch (p) {
                    case 1:
                        Typeface tf3 = Typeface.createFromAsset(getAssets(),
                                "fonts/barbie.ttf");
                        typeface1=tf3;
                   fillOut_TextView( num,num1);

                        break;

                    case 2:
                        Typeface tf4 = Typeface.createFromAsset(getAssets(),
                                "fonts/gothic.ttf");

                        typeface1=tf4;
                      fillOut_TextView( num,num1);

                        ;
                        break;
                    case 3:
                        Typeface tf5 = Typeface.createFromAsset(getAssets(),
                                "fonts/bold.ttf");
                        typeface1=tf5;
                     fillOut_TextView( num,num1);

                        Toast.makeText(getApplicationContext(),
                                "Drop us your feedback regarding your best font! ;)",
                                Toast.LENGTH_SHORT).show();
                        break;

                    case 4:
                        Typeface tf11 = Typeface.createFromAsset(getAssets(),
                                "fonts/chalkdust.ttf");
                        typeface1=tf11;
                     fillOut_TextView( num,num1);

                        break;
                    case 5:
                        Typeface tf6 = Typeface.createFromAsset(getAssets(),
                                "fonts/frank.ttf");
                        typeface1=tf6;
                   fillOut_TextView( num,num1);
                        ;

                    case 6:
                        Typeface tf7 = Typeface.createFromAsset(getAssets(),
                                "fonts/pristina.ttf");
                        typeface1=tf7;
                 fillOut_TextView( num,num1);

                    case 7:
                        Typeface tf8 = Typeface.createFromAsset(getAssets(),
                                "fonts/myriad.otf");
                        typeface1=tf8;
                   fillOut_TextView( num,num1);

                        Toast.makeText(getApplicationContext(),
                                "Please try to  concentrate on your studies! :)",
                                Toast.LENGTH_SHORT).show();

                        p = 0;
                }


            }});



        //Zoomin  (text size) Button
        i6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              fillOut_TextView( num,num1);
                //dataAdapter.setViewBinder(binder);
                zoomIn();



            }});

        // Zoomout  (text size) Button
        i7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              fillOut_TextView( num,num1);
                // dataAdapter.setViewBinder(binder);
                zoomOut();


            }});

//Previous Chapter imageButton
        i8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousChapterButton();
                session_Tracker();

            }
        });


        // this the Home button
        i9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }});

//Next Chapter imageButton
        i10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextChapterButton();
                session_Tracker();

            }
        });



    }



    //zoomIn and zoomOut methods
    public void zoomIn(){
        if (iTextSize < 90)
            iTextSize = iTextSize + 3;
        else
            toastShort("Hey, This is the maximum font size allowed");//local method


    }

    public void zoomOut() {
        if (iTextSize > 6)
            iTextSize = iTextSize - 3;
        else
            toastShort("Hey, This is the minimum font size allowed");

    }

    public void getChapterTitle() {
        Cursor cr = dbHelper.getSomethingFromChapsTable(num);
        if (cr != null) {
            if (cr.moveToFirst()) {
                String label = cr.getString(cr.getColumnIndex("Title"));
                title.setText(label);
            }
            cr.close();

        }
    }



    //speak function
    public void speakText() {

        //String toSpeak = inputText.getText().toString();
        //String toSpeak ="question:         "+q.getText().toString()+"The Answer is:                  "+ans.getText().toString();

        // tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(toSpeak);

            //toSpeak content is the comment_test() method
        } else {
            ttsUnder20(toSpeak);
        }
    }

    @SuppressWarnings("deprecation")
    public void ttsUnder20(String toSpeak) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(String toSpeak) {
        String utteranceId=this.hashCode() + "";
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }




    public void previousChapterButton(){
        if (num > 1) {
            num = num - 1;
            i = 1;
            getChapterTitle();
            fillOut_TextView( num,i);
            toastShort("You have just moved to the previous section/chapter");
        } else toastShort("Hey, this is the first section / chapter");}


    public void nextChapterButton(){
        if (num < chel) {
            num = num + 1;
            i = 1;
            getChapterTitle();
            fillOut_TextView( num,i);
            toastShort("You have just moved to the next section/chapter");

        }

        else toastShort("Hey, this is the last section / chapter");}


    public void session_Tracker() {

        Time_Getter();
        dbHelper.Time_and_Date_Finish_Saver_Method(myTime, myDate, num, 1, 1, sessionNber);
    }






    @Override
    public void onInit(int status){

        if(status==TextToSpeech.SUCCESS){

            int result=tts.setLanguage(Locale.US);
        }

    }

    private void speech(String charSequence) {

        int position=0 ;


        int sizeOfChar= charSequence.length();
        String testStri= charSequence.substring(position,sizeOfChar);


        int next = 1500;
        int pos =0;
        while(true) {
            String temp="";
            Log.e("in loop", "" + pos);

            try {

                temp = testStri.substring(pos, next);
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, temp);
                tts.speak(temp, TextToSpeech.QUEUE_ADD, params);

                pos = pos + 1500;
                next = next + 1500;

            } catch (Exception e) {
                temp = testStri.substring(pos, testStri.length());
                tts.speak(temp, TextToSpeech.QUEUE_ADD, null);
                break;

            }

        }

    }
    

    public void fillOut_TextView(int num,int num1){
        allText="";
        del = dbHelper.getNumberOfFlashcards(num);
num1=0;
        while(num1<del){
            num1++;
            Cursor cr = dbHelper.fetchAllFlashsWithChap(num, num1);

            quest1 = cr.getString(cr.getColumnIndex("Question"));
            text1 = cr.getString(cr.getColumnIndex("Answer"));
            String xcr = cr.getString(cr.getColumnIndex("Comment"));
            if (xcr != null) {
                Comment1 = cr.getString(cr.getColumnIndex("Comment"));
            } else {
                Comment1 = "";
            }
            if (Comment1.equals("")) {
                allText = allText + "<br></br>" + "<font color=#A4A4A4>" + flashcard + num1 + "</font>" + "<br></br>" + "<font color=#045FB4>Question: <br></br>" + quest1 + "</font>" + "<br></br>" + "<br></br>Answer: <br></br>" + text1 + "<br></br>" + "<br></br>" + "<br></br>";
            } else { allText = allText + "<br></br>" + "<font color=#A4A4A4>" + flashcard + num1 + "</font>" + "<br></br>" + "<font color=#045FB4>Question: <br></br>" + quest1 + "</font>" + "<br></br>" + "<br></br>Answer: <br></br>" + text1 + "<br></br>"
                    + "<br></br>" + "<br></br><font color=#045FB4> Your Comment: </font><br></br><font color=#808B96><i>"+ Comment1 +"</i> </font>"+"<br></br>" + "<br></br>" + "<br></br>";}

        }
        t2.setText(Html.fromHtml(allText));
        t2.setTextSize(iTextSize);
        t2.setBackgroundColor(Color.parseColor(colorStringBackground));
        t2.setTextColor(Color.parseColor(colorStringText));
        t2.setTypeface(typeface1);
        if(tts!=null){
            tts.stop(); }
        abc=t2.getText().toString();
        if (zx){speech(abc);} else {if(tts!=null){
            tts.stop();
            //tts.shutdown();
        }}
    }


    public void dataToShare(){
        Cursor cr = dbHelper.fetchAllFlashsWithChap(num, num1);

        quest1 = cr.getString(cr.getColumnIndex("Question"));
        text1 = cr.getString(cr.getColumnIndex("Answer"));
        String xcr = cr.getString(cr.getColumnIndex("Comment"));
        if (xcr != null) {
            Comment1 = cr.getString(cr.getColumnIndex("Comment"));
        } else {
            Comment1 = "";
        }}


    public void shareMethod() {
        dataToShare();
        if (Comment1.equals("")) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            String shareBody = "Hey,"
                    + "\n\n"
                    + "Just I would like to share this flashcard (# "
                    + i
                    + ")"
                    + "\n\n"
                    + "Question: "
                    + quest1
                    + "\n\n"
                    + "Answer: "
                    + text1

                    + "\n\n\n"
                    + "Get more flashcards and outstanding study notes from this app (Google Play  App Store):\n "
                    +"\"" + application +  "\"\n\n"+ "Here\'s the link: "+"\n\n"+ "https://play.google.com/store/apps/details?id="+ getPackageName()+"\n\n"+"All The Best.";;

            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title1 + " from this android App: " + application);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            sharingIntent.putExtra(Intent.EXTRA_BCC, email);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            String shareBody = "Hey,"
                    + "\n\n"
                    + "Just I would like to share this flashcard (# "
                    + i
                    + ")"
                    + "\n\n"
                    + "Question: "
                    + quest1
                    + "\n\n"
                    + "Answer: "
                    + text1
                    + "\n\n"
                    + Comment1
                    + "\n\n\n"
                    + "You Can Get more flashcards and outstanding study notes from this app (Google Play  App Store):\n "
                    +"\"" + application +  "\"\n\n"+ "Here\'s the link: "+"\n\n"+ "https://play.google.com/store/apps/details?id="+ getPackageName()+"\n\n"+"All The Best.";;

            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title.getText() + " from this android App: " + application);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            sharingIntent.putExtra(Intent.EXTRA_BCC, email);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

    }
    @Override
    public void onPause(){
        // Don't forget to shutdown tts!
        if(tts!=null){
            tts.stop();
            tts.shutdown();
            zx=false;speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
        }
        session_Tracker();
        super.onPause();
        //   finish();
    }

    //back button : this method is executed when the user click on his phone back button
    @Override
    public void onBackPressed()
    {if(tts!=null){
        tts.stop();
        tts.shutdown();
        zx=false; speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
    }
        session_Tracker();
        Intent intent = new Intent(this,Tabs.class);
        startActivity(intent);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
        finish();}
}