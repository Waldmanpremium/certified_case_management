package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess{
		//extends Activity {

//Newly added spetember 2018




    private SQLiteOpenHelper openHelper;

    public SQLiteDatabase database;


    private static DatabaseAccess instance;
    
    // flash_table table name
    private static final String TABLE_CHAP = "chap_table";
    private static final String TABLE_FLASH = "flash_table";
    private static final String TABLE_SESSION = "SESSION_TABLE";
 
    // flash_table Table Columns names
    
    public static final String KEY_ChapID = "_id";
    public static final String KEY_TITLE = "Title";
    public static final String KEY_NBER_FLASHCARDS = "Nber_Flashcards";
    //Newly coulums added to chap_table - added sep2018
	public static final String KEY_NBER_MARKED = "Nber_Marked";
	public static final String KEY_NBER_FAVORITE = "Nber_Favorite";
	public static final String KEY_NBER_OWN = "Nber_Own";
	public static final String KEY_NBER_NOT_STUDIED = "Nber_Not_Studied";
	public static final String KEY_NBER_WRONG = "Nber_Wrong";

    
    public static final String KEY_ID = "Flash_ID";
    public static final String KEY_QUESTION = "Question";
    public static final String KEY_ANSWER = "Answer";
    public static final String KEY_CHAP_ID = "Chap_ID" ;
    public static final String KEY_COMMENT = "Comment";
    public static final String KEY_FAVORITE = "Favorite";
    public static final String KEY_MARKED = "Marked";
    public static final String KEY_CORRECT = "Correct";
    public static final String KEY_VIEWED = "Viewed";
    public static final String KEY_FLASH_RANK = "Flash_Chap_Rank";
    public static final String KEY_OWN = "own";

    public static final String KEY_Session_ID = "KEY_Session_ID";
	public static final String KEY_TestSession_ID = "KEY_TestSession_ID";
    public static final String KEY_TIME_START = "Time_Session_Start";
    public static final String KEY_DATE_START = "Date_Session_Start";
    public static final String KEY_TIME_FINISH = "Time_Session_Finish" ;
    public static final String KEY_DATE_FINISH = "Date_Session_Finish";
    public static final String KEY_MODE = "Session_Mode";
    public static final String KEY_CATEGORY = "Session_Flash_Category";
    public static final String KEY_SCHAP_ID = "Session_Chap_ID";
    public static final String KEY_SFLASH_RANK = "Session_Flash_Rank_ID";
    public static final String KEY_SVIEWED = "Session_Viewed";
    public static final String KEY_SRESPONDED = "Session_Responded";
    public static final String KEY_SRESULTS = "Session_Results";
    public static final String KEY_SWRONG = "Session_Wrong";
    


    private static final String TAG = "myApp";



 public void OldAddTitle(String title) {
	 this.database = openHelper.getWritableDatabase();
	 ContentValues values = new ContentValues();


	 values.put(KEY_TITLE, title); // Adding Title


	 // Inserting Row
	 database.insert(TABLE_CHAP, null, values);
	 database.close(); // Closing database connection

 }


	public void OldAddQuestionAnswer(int i,int j , String Question, String Answer) {
		this.database = openHelper.getWritableDatabase();


		ContentValues values1 = new ContentValues();

		values1.put(KEY_QUESTION, Question); // Adding Question
		values1.put(KEY_ANSWER, Answer); // Adding Answer
		values1.put(KEY_CHAP_ID, i);
		values1.put(KEY_FLASH_RANK, j);// Adding chap nber Title

		// Inserting Row
		database.insert(TABLE_FLASH, null, values1);
		database.close(); // Closing database connection

	}

   

    /**
     * Private constructor to avoid object creation from outside classes.
     *
     * @param context
     */
	public DatabaseAccess(Context context) {
		this.openHelper = new DatabaseOpenHelper(context);
	}



    //This onCreate function is to create tables if they did not exist
    public void onCreate(SQLiteDatabase db) {


		//this newly created in 22 september 2018
	//********	String CREATE_chap_table = "CREATE TABLE IF NOT EXISTS  chap_table  ( _id  INTEGER PRIMARY KEY AUTOINCREMENT,  Title  TEXT )";
	/********  database.execSQL(CREATE_chap_table);

		//this is new for chapter table
		Cursor c22 = database.rawQuery("SELECT * FROM chap_table", null);
		c22.moveToFirst();
		int index1 = c22.getColumnIndex("Title");

		c22.close();



//this newly created in 22 sepyember 2018
		//********String CREATE_flash_table = "CREATE TABLE IF NOT EXISTS  flash_table  ( Flash_ID  INTEGER PRIMARY KEY AUTOINCREMENT,  Question  TEXT,  Answer  TEXT )";
		//********database.execSQL(CREATE_flash_table);

		//this is new for flash table
		Cursor c23 = database.rawQuery("SELECT * FROM flash_table", null);
		c23.moveToFirst();
		int index2 = c23.getColumnIndex("Question");
		c23.close();
	 */

// this old
        String CREATE_SESSION_TABLE = "CREATE TABLE IF NOT EXISTS  SESSION_TABLE  ( KEY_Session_ID  INTEGER PRIMARY KEY AUTOINCREMENT, KEY_TestSession_ID  INTEGER, Date_Session_Start  TEXT,  Time_Session_Start  TEXT, Date_Session_Finish  TEXT,  Time_Session_Finish  TEXT,  Session_Viewed  INTEGER," +
        		"Session_Responded INTEGER, Session_Wrong  INTEGER, Session_Results INTEGER, Session_Flash_Rank_ID INTEGER, Session_Chap_ID INTEGER, Session_Mode INTEGER, Session_Flash_Category Integer )";
        database.execSQL(CREATE_SESSION_TABLE);
		//this is old
		Cursor c2 = database.rawQuery("SELECT * FROM SESSION_TABLE", null);
		c2.moveToFirst();
		int index = c2.getColumnIndex("Date_Session_Start");
		c2.close();


    }




    //this upgrade function to add new column  if doesn't exist to existing tables
    
    public void onUpgrade(SQLiteDatabase db) {
    	//open();



    	Cursor c1 = database.rawQuery("SELECT * FROM flash_table", null);
    	c1.moveToFirst();

/*
    	//newly added sep2018

		int index4 = c1.getColumnIndex("Chap_ID");
		if (index4 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Chap_ID INTEGER");
		}
//newly added sep2018

		int index12 = c1.getColumnIndex("Flash_Chap_Rank");
		if (index12 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Flash_Chap_Rank INTEGER" );
			 }
*/


		int index5 = c1.getColumnIndex("Comment");
    	if (index5 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Comment TEXT"); }
		int index6 = c1.getColumnIndex("Marked");
    	if (index6 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Marked INTEGER");
		}
		int index7 = c1.getColumnIndex("Favorite");
    	if (index7 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Favorite INTEGER");
		}
    	int index8 = c1.getColumnIndex("Viewed");
    	if (index8 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Viewed INTEGER");
		}
    	int index9 = c1.getColumnIndex("Correct");
    	if (index9 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Correct INTEGER");
		}
    	int index10 = c1.getColumnIndex("Own");
    	if (index10 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN Own INTEGER");
		}
	/*******/
		int index11 = c1.getColumnIndex("_id");
		if (index11 == -1) {
			database.execSQL("ALTER TABLE flash_table  ADD COLUMN _id INTEGER ");
		}
		Cursor c2 = database.rawQuery("SELECT * FROM chap_table", null);
    	c2.moveToFirst();

    	int ind2 = c2.getColumnIndex("Chapter_Resulats");
    	if (ind2 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Chapter_Resulats INTEGER");
		}

    	int ind3 = c2.getColumnIndex("Nber_Flashcards");
    	if (ind3 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Flashcards INTEGER");
		}
		int ind4 = c2.getColumnIndex("Chap_Views_Q");
    	if (ind4 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Chap_Views_Q INTEGER");
		}
		int ind5 = c2.getColumnIndex("Chap_Correct_Answer");
    	if (ind5 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Chap_Correct_Answer INTEGER");
		}
        	 //******Newly added column in chap_table - sep 2018
		int ind6 = c2.getColumnIndex("Nber_Marked");//Nber_Marked
		if (ind6 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Marked INTEGER");
		}

		int ind7 = c2.getColumnIndex("Nber_Favorite");
		if (ind7 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Favorite INTEGER");
		}
		int ind8 = c2.getColumnIndex("Nber_Own");
		if (ind8 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Own INTEGER");
		}
		int ind9 = c2.getColumnIndex("Nber_Not_Studied");
		if (ind9 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Not_Studied INTEGER");
		}
		int ind10 = c2.getColumnIndex("Nber_Wrong");
		if (ind10 == -1) {
			database.execSQL("ALTER TABLE chap_table  ADD COLUMN Nber_Wrong INTEGER");
		}

		//replace \n by sqlite carriage return
		String upQuery1= "update flash_table set  Question = replace(Question, '\\n', X'0A')";
		database.execSQL(upQuery1);

		String upQuery2= "update flash_table set  Answer = REPLACE(Answer, '\\n', char(10))";
		database.execSQL(upQuery2);

		c2.close();
    	c1.close();
    	}
    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    
    
    
    /**
     * Open the database connection.
     */
    public void open() {
    	
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();

        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new flashcard
    public void addNewFlashcard(Flash_Data_Class flash_Data_Class) {
    	this.database = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
       
        values.put(KEY_QUESTION, flash_Data_Class.getQuestion()); // Adding Question
        values.put(KEY_ANSWER, flash_Data_Class.getAnswer()); // Adding Answer

        // Inserting Row
        database.insert(TABLE_FLASH, null, values);
        database.close(); // Closing database connection
 
    }
    
    //adding Nber of Flashcards to chapters table
    public void addNberFlashcard(Chapter_Titles_Class chapter_Title_Class) {
    	this.database = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
       
        values.put(KEY_NBER_FLASHCARDS, chapter_Title_Class.getNberFlashcards()); // Adding nber of flashcards

        // Inserting Row
        database.insert(TABLE_CHAP, null, values);
        database.close(); // Closing database connection
 
    }

    // Getting single Flashcard
    public Flash_Data_Class getflash_Data_Class(int id) {
    	this.database = openHelper.getReadableDatabase();

        Cursor cursor = database.query(TABLE_FLASH, new String[] { KEY_ID,
                        KEY_QUESTION, KEY_ANSWER }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null )
            cursor.moveToFirst();

        Flash_Data_Class flash_Data_Class = new Flash_Data_Class(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
       
    	
        // return Flashcard content (id, question and answer)
        return flash_Data_Class;
    }
    
    public Flash_Data_Class cursorToStation(Cursor cursor) {
    	Flash_Data_Class flash_Data_Class = new Flash_Data_Class();
    	flash_Data_Class.getQuestion();
    	flash_Data_Class.getAnswer();
      
        return flash_Data_Class;
        }
    


    // Getting All flash_table
    public List<Flash_Data_Class> getAllflash_table() {
        List<Flash_Data_Class> flashcardList = new ArrayList<Flash_Data_Class>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FLASH;

        this.database = openHelper.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Flash_Data_Class flash_Data_Class = new Flash_Data_Class();
                flash_Data_Class.setID(Integer.parseInt(cursor.getString(0)));
                flash_Data_Class.setQuestion(cursor.getString(1));
                flash_Data_Class.setAnswer(cursor.getString(2));
                
                
                // Adding flashcards to the list
                flashcardList.add(flash_Data_Class);
            } while (cursor.moveToNext());
 
        }

        // return flashcard list
        return flashcardList;
        
    }
    
    
    

    // Updating single Flashcard
    public int updateFlash_Data_Class(Flash_Data_Class flash_Data_Class) {
    	this.database = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_QUESTION, flash_Data_Class.getQuestion());
        values.put(KEY_ANSWER, flash_Data_Class.getAnswer());

        // updating row (the Question is why returning an int?)
        return database.update(TABLE_FLASH, values, KEY_ID + " = ?",
                new String[] { String.valueOf(flash_Data_Class.getID()) });
    }

    // Deleting single Flashcard
    public void deleteFlash_Data_Class(Flash_Data_Class flash_Data_Class) {
    	this.database = openHelper.getWritableDatabase();
        database.delete(TABLE_FLASH, KEY_ID + " = ?",
                new String[] { String.valueOf(flash_Data_Class.getID()) });
        database.close();
       
    }


    // Getting flash_table Count
    public int getflash_tableCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FLASH +" WHERE "+KEY_ID+" IS NOT NULL";
        this.database = openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(countQuery, null);
       
        // return count
        return cursor.getCount();
        
    }
    
    // Getting Session_table Count
    public int getSession_tableCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SESSION +" WHERE "+KEY_Session_ID+" IS NOT NULL";
        this.database = openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(countQuery, null);
   
        // return count
        return cursor.getCount();
        
    }


	// Getting Session_table Count
	public int getTestSession_tableCount() {
		String countQuery = "SELECT  * FROM " + TABLE_SESSION +" WHERE "+KEY_TestSession_ID+" IS NOT NULL";
		this.database = openHelper.getReadableDatabase();
		Cursor cursor = database.rawQuery(countQuery, null);

		// return count
		return cursor.getCount();

	}
    
    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */


	//this is list handling & relationship with database - NOt understood
	public List<String> getQuotes() {
		List<String> list = new ArrayList<>();
		Cursor cursor = database.rawQuery("SELECT * FROM flash_table", null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			list.add(cursor.getString(0));
			list.add(cursor.getString(1));
			list.add(cursor.getString(2));
			cursor.moveToNext();
		}

		cursor.close();
		return list;
	}


	public Cursor fetchAllFlashsWithChap(int cid) {
		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid;
		//String cnQuery = "select * from flash_table ";
		Cursor cr = database.rawQuery(cnQuery, null);

		/*phoneNumber = cursorWrapper.getString(
				cursorWrapper.getColumnIndex(UserTable.Cols.PHONE_NUMBER));*/
		if (cr  != null && cr .moveToFirst()) {
			cr .moveToFirst();
		}

		return cr;
	}
    
  
    // Return Cursor that contains the full row*******************
    public Cursor fetchAllFlashsWithChap(int cid,int fid) {

   	 String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid; 
 	   //String cnQuery = "select * from flash_table ";
 	   Cursor cr = database.rawQuery(cnQuery, null);

 	  if (cr != null && cr.moveToFirst()) {
 	   cr.moveToFirst();
 	  }
 
 	  return cr;
 	 }
   
    
    
    public Cursor getSomethingFromChapsTable(int cid) {
    	
      	 String cnQuery= "SELECT *  FROM " + TABLE_CHAP + " WHERE " + KEY_ChapID + " = "+ cid; 
    	   //String cnQuery = "select * from flash_table ";
      
    	   Cursor cr = database.rawQuery(cnQuery, null);

    	  if (cr != null) {
    	   cr.moveToFirst();
    	  }
    
    	  return cr;
    	 }
 
    //////////////////////////////////////////for chapters*************************
    
    
    
    
    // Getting All flash_table
    public List<Chapter_Titles_Class> getAllchap_table() {
        List<Chapter_Titles_Class> ChaptersTitle = new ArrayList<Chapter_Titles_Class>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CHAP;

        this.database = openHelper.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
            	Chapter_Titles_Class chapter_data_class = new Chapter_Titles_Class();
            	chapter_data_class.setSectionNber(Integer.parseInt(cursor.getString(0)));
            	chapter_data_class.setTitle(cursor.getString(1));

            	//********  make sure getChap_tableCount() is correct and is not getFlash_tableCount()
            	chapter_data_class.setNberFlashcards(getChap_tableCount());
            	chapter_data_class.setIconID(0);

                // Adding chapters data to list
            	ChaptersTitle.add(chapter_data_class);
            } while (cursor.moveToNext());
 
        }

        // return chapter list
        return ChaptersTitle;
        
    }


    //retoune les donnees des chapitres et c'est different de fetch all flashcard ci-haut
    public Cursor fetchAllChapters() {

    	  Cursor mCursor = database.query(TABLE_CHAP, new String[] {KEY_ChapID,
    			  KEY_TITLE,KEY_NBER_FLASHCARDS,KEY_NBER_MARKED,KEY_NBER_FAVORITE,KEY_NBER_OWN, KEY_NBER_NOT_STUDIED,KEY_NBER_WRONG},
    	    null, null, null, null, null);

    	  if (mCursor != null) {
    	   mCursor.moveToFirst();
    	  }
    	  return mCursor;
    	 }


    //this is list handling & relationship with database
    public List<String> getChapters() {
        List<String> list = new ArrayList<String>();
        Cursor cursor = database.rawQuery("SELECT * FROM chap_table", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
        	//list.add(new Chapter_Titles_Class(cursor.getString(1),Integer.valueOf(cursor.getString(0)),0, 0));
        	list.add(new String(cursor.getString(1)));
            
            cursor.moveToNext();
        }
       
        cursor.close();
        return list;
    }
    
    
    
    
    // Getting chap_table Count
    public int getChap_tableCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CHAP;
        this.database = openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(countQuery, null);
        // return count
        return cursor.getCount();
      
        
    }
  
    
    
    //New method of counting flashcards from stackoverflow
    
 public  int getNumberOfFlashcards(int cid){
	   
	   //String countQuery="SELECT count(*) FROM "  .flash_table. " WHERE" flash_table.Chap_ID = chap_table._id";
	   String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid; 
	   //String cnQuery = "select * from flash_table ";
	   Cursor cr = database.rawQuery(cnQuery, null);

		 int total_count=cr.getCount();

	
		 
		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_FLASHCARDS+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		 this.database.execSQL(upQuery);
		 return total_count;
		 
   }


	// Calculating the nber of flashcards per chapter
	public void nberOfFlahcards(){
		int xy= getChap_tableCount();


		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){


			getNumberOfFlashcards(i);}

	}


	public  int getNumberOfMarkedFlashcards(int cid){

		//String countQuery="SELECT count(*) FROM "  .flash_table. " WHERE" flash_table.Chap_ID = chap_table._id";
		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND " + KEY_MARKED + " = " +1;
		//String cnQuery = "select * from flash_table ";
		Cursor cr = database.rawQuery(cnQuery, null);

		int total_count=cr.getCount();



		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_MARKED+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		this.database.execSQL(upQuery);
		return total_count; }


	public void nberOfMarkedFlahcards(){
		int xy= getChap_tableCount();


		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){

			getNumberOfMarkedFlashcards(i);} }


	public  int getNumberOfFavoriteFlashcards(int cid){


		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND " + KEY_FAVORITE + " = " +1;

		Cursor cr = database.rawQuery(cnQuery, null);

		int total_count=cr.getCount();



		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_FAVORITE+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		this.database.execSQL(upQuery);
		return total_count; }


	public void nberOfFavoriteFlahcards(){
		int xy= getChap_tableCount();

		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){

			getNumberOfFavoriteFlashcards(i);} }



	public  int getNumberOfWrongFlashcards(int cid){


		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND " + KEY_CORRECT + " = " +2;

		Cursor cr = database.rawQuery(cnQuery, null);

		int total_count=cr.getCount();



		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_WRONG+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		this.database.execSQL(upQuery);
		return total_count; }


	public void nberOfWrongFlahcards(){
		int xy= getChap_tableCount();

		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){

			getNumberOfWrongFlashcards(i);} }




	public  int getNumberOfOwnFlashcards(int cid){


		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND " + KEY_OWN + " = " +1;

		Cursor cr = database.rawQuery(cnQuery, null);

		int total_count=cr.getCount();


		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_OWN+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		this.database.execSQL(upQuery);
		return total_count; }


	public void nberOfOwnFlahcards(){
		int xy= getChap_tableCount();

		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){

			getNumberOfOwnFlashcards(i);} }


	public  int getNumberOfNotStudiedFlashcards(int cid){


		String cnQuery= "SELECT *  FROM " + TABLE_FLASH + " WHERE " + KEY_CHAP_ID + " = "+ cid+" AND " + KEY_VIEWED+" IS NULL";

		Cursor cr = database.rawQuery(cnQuery, null);

		int total_count=cr.getCount();



		String upQuery ="UPDATE "+ TABLE_CHAP+"  SET "+ KEY_NBER_NOT_STUDIED+ " = "+total_count+ " WHERE " + KEY_ChapID + " = "+ cid;
		this.database.execSQL(upQuery);
		return total_count; }


	public void nberOfNotStudiedFlahcards(){
		int xy= getChap_tableCount();

		this.database = openHelper.getReadableDatabase();
		database = openHelper.getWritableDatabase();
		for(int i=1; i<=xy; i++){

			getNumberOfNotStudiedFlashcards(i);} }




	///return_Comment methods
 public  String return_Comment(int cid,int fid){
	  
	  String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
	 Cursor c=database.rawQuery(upQuery, null);
	  String button="90"; 
	  
	  if(c!=null &&  c.moveToFirst()){
//if   (c.getString(5)==null) 
		 if(c.isNull(c.getColumnIndex("Comment"))||c.getString(c.getColumnIndex("Comment")).isEmpty()||c.getString(5)==" ") {button ="0";}else{button=c.getString(5);}}
		 
	  return button;}
 
 
 
//////Save Comment method

 
	public void saveComment(int cid,int fid,String comment, Context context) {
		
		
	    ContentValues values = new ContentValues();
	    if (!comment.isEmpty()||comment!=" "){
	  values.put(KEY_COMMENT, comment);
	  //databaseName.update(tableName, values, condition (row and column or any other condition,null);
	    database.update(TABLE_FLASH, values,  KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid, null);}
		else{
			String Xquery ="UPDATE "+TABLE_FLASH+" SET "+KEY_COMMENT+" = NULL  WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
			 this.database.execSQL(Xquery, null);
	
		
		}
	
	  
	}
//Save Edit question method

	public void saveEdit_question(int cid,int fid,String question, Context context) {
	    ContentValues values = new ContentValues();
	   
	  values.put(KEY_QUESTION, question);
	  //databaseName.update(tableName, values, condition (row and column or any other condition,null);
	    database.update(TABLE_FLASH, values,  KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid, null);}
	
//Save Edit answer method

		public void saveEdit_answer(int cid,int fid,String answer, Context context) {
		    ContentValues values = new ContentValues();
		   
		  values.put(KEY_ANSWER, answer);
		  //databaseName.update(tableName, values, condition (row and column or any other condition,null);
		    database.update(TABLE_FLASH, values,  KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid, null);}		
		
	
	  
//Date & Time  START saver Method
	public int	Time_and_Date_START_Saver_Method( String myTime_Start, String myDate_Start, int Mode, int cid, int fid ){
		

		 int Sessioncount= getSession_tableCount();

		 
	      ContentValues initialValues = new ContentValues();
	      initialValues.put(KEY_Session_ID, Sessioncount+1);

	      initialValues.put(KEY_TIME_START, myTime_Start);
	      initialValues.put(KEY_DATE_START, myDate_Start);
		initialValues.put(KEY_TIME_FINISH, myTime_Start);
		initialValues.put(KEY_DATE_FINISH, myDate_Start);
	    initialValues.put(KEY_MODE, Mode);

		initialValues.put(KEY_CATEGORY, Mode);
	    initialValues.put(KEY_SCHAP_ID, cid);
	    initialValues.put(KEY_SFLASH_RANK, fid);
		/*initialValues.put(KEY_SRESULTS, 0);
		initialValues.put(KEY_SRESPONDED, 0);
		initialValues.put(KEY_SWRONG, 0);
		initialValues.put(KEY_VIEWED, 1);*/
	      
	      database.insert(TABLE_SESSION, null, initialValues);

		 return Sessioncount+1;
	   
	}
	 
	//Date & Time  Finish saver Method
		public void	Time_and_Date_Finish_Saver_Method( String myTime_Finish, String myDate_Finish, int cid, int fid,int views, int session ){
			

			 ContentValues updateValues = new ContentValues();
		
			 updateValues.put(KEY_TIME_FINISH, myTime_Finish);
			 updateValues.put(KEY_DATE_FINISH, myDate_Finish);
			 updateValues.put(KEY_SVIEWED, views);
			 updateValues.put(KEY_SCHAP_ID, cid);
			 updateValues.put(KEY_SFLASH_RANK, fid);

			 database.update(TABLE_SESSION, updateValues,  KEY_Session_ID + " = "+ session, null);	
		
		}

    public void Time_and_Date_Finish_Saver_Method0( String myTime_Finish, String myDate_Finish, int cid, int fid,int views, int responded,int wrong, int session,int testSession ){


        int results=0;
        ContentValues updateValues = new ContentValues();
        if (responded!=0) { results=(responded-wrong)*100/responded;
        updateValues.put(KEY_TIME_FINISH, myTime_Finish);
        updateValues.put(KEY_DATE_FINISH, myDate_Finish);
        updateValues.put(KEY_SVIEWED, views);
        updateValues.put(KEY_SCHAP_ID, cid);
        updateValues.put(KEY_SFLASH_RANK, fid);
        updateValues.put(KEY_SRESULTS, results);
        updateValues.put(KEY_SRESPONDED, responded);
        updateValues.put(KEY_SWRONG, wrong);
		updateValues.put(KEY_TestSession_ID, testSession);


        database.update(TABLE_SESSION, updateValues,  KEY_Session_ID + " = "+ session, null);
        }

    }




	public int getResults(int testSessionNber){
		Cursor cursor = getSomethingFromSESSION_Table(testSessionNber);
        int A=0;
        if(cursor != null && cursor.moveToFirst()) {
	 A = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Results"));}return A;}

	public int getWrong(int sessionNber){

		Cursor cursor = getSomethingFromSESSION_Table(sessionNber);
        int A=0;
        if(cursor != null && cursor.moveToFirst()) {
		A = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Wrong"));}return A;}

	public String getStart(int sessionNber){

		Cursor cursor = getSomethingFromSESSION_Table(sessionNber);
		String A="";
		if(cursor != null && cursor.moveToFirst()) {
			A = cursor.getString(cursor.getColumnIndexOrThrow("Time_Session_Start"));}return A;}


	public String getFinish(int sessionNber){

		Cursor cursor = getSomethingFromSESSION_Table(sessionNber);
		String A="";
		if(cursor != null && cursor.moveToFirst()) {
			A = cursor.getString(cursor.getColumnIndexOrThrow("Time_Session_Finish"));}return A;}


	public int getResponded(int sessionNber){
		Cursor cursor = getSomethingFromSESSION_Table(sessionNber);

        int A=0;
        if(cursor != null && cursor.moveToFirst()) {
		A = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Responded"));}return A;}

	/*public int[] getResults(int sessionNber){
	    int A[]= {0,0,0};//delcaration+ instantiation +initialization
	/*	new int[3];
		A[0]=0;//initialization
		A[1]=0;
		A[2]=0;*/
		/*Cursor cursor = getSomethingFromSESSION_Table(sessionNber);
		 A[0]  = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Results"));
         A[1]  = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Responded"));
         A[2]  = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Wrong"));
	return A;
	}*/

	public Cursor getSomethingFromSESSION_Table(int sessionNber) {

		String cnQuery= "SELECT *  FROM " + TABLE_SESSION + " WHERE " + KEY_Session_ID+ " = "+(sessionNber);
		//String cnQuery = "select * from session_table ";

		Cursor cr = database.rawQuery(cnQuery, null);
		if (cr!=null &&  cr.moveToFirst()) {
			cr.moveToFirst();
		}

		return cr;
	}

	public Cursor getSomethingFromTestSESSION_Table(int TestSessionNber) {

		String cnQuery= "SELECT *  FROM " + TABLE_SESSION + " WHERE " + KEY_TestSession_ID+ " = "+(TestSessionNber);
		//String cnQuery = "select * from session_table ";

		Cursor cr = database.rawQuery(cnQuery, null);
		if (cr != null) {
			cr.moveToFirst();
		}

		return cr;
	}
 
 //////seen before
   
   public  int seen_method(int cid,int fid){
		  
		  String upQuery ="SELECT * FROM "+ TABLE_FLASH+"  WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		  Cursor c=database.rawQuery(upQuery, null);
		  int seen=90; 
		  if(c!=null &&  c.moveToFirst()){
			  seen=c.getInt(8)+1;
			  String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_VIEWED+ " = "+seen+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
				 this.database.execSQL(xQuery);
				 if (seen==999) {seen=1;}}
			
		  
		  return seen;}


	//newly added sep2018

	///My NonStudied flashcard  methods
	public  int unseen_method(int cid,int fid){

		String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		Cursor c=database.rawQuery(upQuery, null);
		int unseen=90;
		if(c!=null &&  c.moveToFirst()){
			if   (c.getInt(8)==0 ||c.getInt(8)==-1) {unseen =1;}else  {unseen=0;}}

		return unseen;}
   
 ///Correct or wrong  methods
   public  int correct_method(int cid,int fid){
 	  
 	  String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
 	  Cursor c=database.rawQuery(upQuery, null);
 	  int button=90; 
 	  if(c!=null &&  c.moveToFirst()){
 if   (c.getInt(9)==0) {button =0;}else if (c.getInt(9)==2){button=2;}else {button=1;}}
 	  
 	  return button;}


	public  void correct_click(int cid,int fid){

		int b1=correct_method(cid,fid);

		if   (b1==0 || b1==2) {


			String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_CORRECT+ " = "+1+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;

b1=90;

			this.database.execSQL(xQuery);}



		//return button;

	}

	public  void wrong_click(int cid,int fid){
		int b2=correct_method(cid,fid);

         if (b2==0 || b2==1) {String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_CORRECT+ " ="+2+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;

		this.database.execSQL(xQuery);
			 b2=90;
		}

		//return button;

	}



 ///My own flashcard  methods
   public  int ownFlash_method(int cid,int fid){
 	  
 	  String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
 	  Cursor c=database.rawQuery(upQuery, null);
 	  int button=90; 
 	  if(c!=null &&  c.moveToFirst()){
 if   (c.getInt(10)==1) {button =1;}else  {button=0;}}
 	  
 	  return button;}


	///My wrong flashcard  methods
	public  int wrongFlash_method(int cid,int fid){

		String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		Cursor c=database.rawQuery(upQuery, null);
		int button=90;
		if(c!=null &&  c.moveToFirst()){
			if   (c.getInt(9)==2) {button =1;}else  {button=0;}}

		return button;}
   
   //////Favorite method
   
   public  int favorite_method(int cid,int fid){
		  
		  String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		  
		  Cursor c=database.rawQuery(upQuery, null);
		  int button=90; 
		  if(c!=null &&  c.moveToFirst()){

			  
			if   (c.getInt(7)==0) {button =0;}else{button=1;}}
		  
		  return button;}
   
   //////Favorite methode_click
   
  public  int favorite_click(int cid,int fid){

	  int button=favorite_method(cid,fid);

	if   (button==0) {


		 String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_FAVORITE+ " = "+1+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;

		 button=1;

		 this.database.execSQL(xQuery);}
	else{ String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_FAVORITE+ " ="+null+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;

		 this.database.execSQL(xQuery);
		 button=0;
		}

		 return button;

   }

  //if(c.isNull(cr.getColumnIndex(Comment)))

  //Insert Your own flashcard

  public void insertLog_q(String q, String ans, int cid, int fid, int ownf,Context context) {

	  if(  !q.isEmpty()){
	  int flashcount= getflash_tableCount();
      ContentValues initialValues = new ContentValues();
      initialValues.put(KEY_ID, flashcount+1);
      initialValues.put(KEY_CHAP_ID, cid);
      initialValues.put(KEY_FLASH_RANK, fid);
      initialValues.put(KEY_ANSWER, ans);
      initialValues.put(KEY_QUESTION, q);
      initialValues.put(KEY_OWN, ownf);

      database.insert(TABLE_FLASH, null, initialValues);} else {Toast.makeText(context,
				"Please, add your Question/Term before saving!", Toast.LENGTH_SHORT)
				.show();}

  }
  public void insertLog_ans(String q, String ans, int cid, int fid, int ownf,Context context) {
	  
	 // int flashcount= getflash_tableCount();
//if  (Exists(flashcount+1+"")){
	  
	//  if( (!ans.isEmpty())){
	  ContentValues values = new ContentValues();
	   
	  values.put(KEY_ANSWER, ans);
	  //databaseName.update(tableName, values, condition (row and column or any other condition,null);
	    database.update(TABLE_FLASH, values,  KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid, null);	



  }


  
    ///Marked for review methods
  public  int marked_method(int cid,int fid){
	  
	  String upQuery ="SELECT * FROM "+ TABLE_FLASH+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		
	  Cursor c=database.rawQuery(upQuery, null);
	  int button=90; 
	  if(c!=null &&  c.moveToFirst()){
if   (c.getInt(6)==0) {button =0;}else{button=1;}}
	  
	  return button;}

//////Marked methode_click

public  int marked_click(int cid,int fid){
  
  int button=marked_method(cid,fid);
  
if   (button==0) {
	 
	
	 String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_MARKED+ " = "+1+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;
		
	 button=1;
	
	 this.database.execSQL(xQuery);} 
else{ String xQuery ="UPDATE "+ TABLE_FLASH+"  SET "+ KEY_MARKED+ " ="+0+" WHERE " + KEY_CHAP_ID + " = "+ cid+" AND "+ KEY_FLASH_RANK + " = "+ fid;

	 this.database.execSQL(xQuery);
	 button=0;
	 }

	 return button;
	 
}

	public int Marked_Nber() {

		// String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
		String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_MARKED+" IS NOT NULL ";
		this.database = openHelper.getWritableDatabase();
		Cursor c = database.rawQuery(selectQuery1, null);


		int button=c.getCount();



		return button;}


	public int OwnFlash_Nber() {

		// String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
		String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_OWN+" IS NOT NULL ";
		this.database = openHelper.getWritableDatabase();
		Cursor c = database.rawQuery(selectQuery1, null);


		int button=c.getCount();



		return button;}








	public int Favorite_Nber() {
    	        
    	       // String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
    	        String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_FAVORITE+" IS NOT NULL ";
    	        this.database = openHelper.getWritableDatabase();
    	        Cursor c = database.rawQuery(selectQuery1, null);

    	 
    	  	  int button=c.getCount();
    	  	  
    	
    	  	  
    	  	  return button;}


	public int Wrong_Nber() {

		// String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
		String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_CORRECT + " = " +2;
		this.database = openHelper.getWritableDatabase();
		Cursor c = database.rawQuery(selectQuery1, null);


		int button=c.getCount();



		return button;}


	public int Viewed_Nber() {
    	        
     	       // String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
     	        String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_VIEWED+" IS NOT NULL";
     	        this.database = openHelper.getWritableDatabase();
     	        Cursor c = database.rawQuery(selectQuery1, null);

     int button=c.getCount();

     return button;}

	public int Not_Studied_Nber() {

		// String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
		String selectQuery1= "SELECT * FROM "+ TABLE_FLASH+" WHERE "+KEY_VIEWED+" IS NULL";
		this.database = openHelper.getWritableDatabase();
		Cursor c = database.rawQuery(selectQuery1, null);

		int button=c.getCount();

		return button;}
    	    
	public int Chap_Nber() {
    	        
      	       // String selectQuery = "SELECT  * FROM " + TABLE_FLASH;
      	        String selectQuery1= "SELECT * FROM "+ TABLE_CHAP+" WHERE "+KEY_ChapID+" IS NOT NULL";
      	        this.database = openHelper.getWritableDatabase();
      	        Cursor c = database.rawQuery(selectQuery1, null);

      	 
      	  	  int button=c.getCount();
      	  	  
      	
      	  	  
      	  	  return button;}



	public Cursor search(String searchString) {

		String[] columns = new String[]{ KEY_QUESTION,KEY_ANSWER, KEY_COMMENT};
		String where =  KEY_QUESTION + " LIKE ? OR "+ KEY_ANSWER  + " LIKE ? ";
		searchString = "%" + searchString + "%";
		// here we should add 2 argument as the ccsearch will be in 2 columns.
		String[] whereArgs = new String[]{searchString, searchString };

		Cursor cursor = null;
		try {if (database == null) {
            database = openHelper.getReadableDatabase();
        }

			cursor = database.query(TABLE_FLASH, columns, where, whereArgs, null, null, null);
		} catch (Exception e) {}
		return cursor;
	}



 
  
   
}