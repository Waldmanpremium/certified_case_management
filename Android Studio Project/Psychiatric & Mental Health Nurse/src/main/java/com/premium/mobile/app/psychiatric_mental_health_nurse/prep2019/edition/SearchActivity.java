package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

//import android.support.v7.app.AppCompatActivity;

/**
 * Activity that searches the database for a string entered by the user.
 * - Displays any strings matching the ccsearch term in a text view.
 * - If the ccsearch term is empty, prints all the words in the database.
 * - If the word is not found, displays "No result."
 */
public class SearchActivity extends Activity{

   // private static final String TAG = EditWordActivity.class.getSimpleName();

    public DatabaseAccess dbHelper;
    //public SimpleCursorAdapter dataAdapter;
    public SQLiteDatabase database;

   // private WordListOpenHelper mDB;

    private EditText mEditWordView;
    private TextView mTextView;
    String avv="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.activity_search);
        DatabaseAccess instance;
        dbHelper = new DatabaseAccess(this);

        mEditWordView = ((EditText) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.search_word));
        mTextView = ((TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.search_result));
    }

    // Click handler for Search button.
    public void showResult(View view) {
        avv="";
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditWordView.getWindowToken(), 0);
        String word = mEditWordView.getText().toString();
        mTextView.setText("Search Results for \"" + word + "\":\n\n");

        // Search for the word in the database.
        Cursor cursor = dbHelper.search(word);
        // You must move the cursor to the first item.
        cursor.moveToFirst();
        // Only process a non-null cursor with rows.
        if (cursor != null & cursor.getCount() > 0) {
            int index1, index2,index3;
            String result1,result2,result3;
            // Iterate over the cursor, while there are entries.
            do {
                index1 = cursor.getColumnIndex(DatabaseAccess.KEY_QUESTION);
                index2 = cursor.getColumnIndex(DatabaseAccess.KEY_ANSWER);
                index3 = cursor.getColumnIndex(DatabaseAccess.KEY_COMMENT);

                result1 = cursor.getString(index1);
                result2 = cursor.getString(index2);
                result3 = cursor.getString(index3);
                // Add result to what's already in the text view.

                if (result3==null){

                    avv= avv+"<br>-----------------------------</br><br></br><br><FONT COLOR=#002080><B>Question: </B></FONT></br>"+result1+"<br></br><br><FONT COLOR=#002080><B>Answer: </B></FONT></br>"+result2+"<br></br>";
               }  else{avv=avv+ "<br>-----------------------------</br><br></br><br><FONT COLOR=#002080><B>Question: </B></FONT></br>"+result1+"<br></br><br><FONT COLOR=#002080><B>Answer: </B></FONT></br>"+result2+"<br></br><br></br>"+"<br></br><br><FONT COLOR=#002080><B>My Comment: </B></FONT></br>"+result3+"<br></br>";


                }
                mTextView.setText(Html.fromHtml(avv));



            } while (cursor.moveToNext());
            cursor.close();
        } else {
            mTextView.append("No Result");
        }
    }






    @Override
    public void onBackPressed(){


        Intent intent = new Intent(this,Tabs.class);
        startActivity(intent);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
        //finish();

    }

    @Override
    public void onDestroy(){

        super.onDestroy();
    }


    @Override
    protected   void onPause() {
        // TODO Auto-generated method stub
    super.onPause();
        finish();
    }
}

