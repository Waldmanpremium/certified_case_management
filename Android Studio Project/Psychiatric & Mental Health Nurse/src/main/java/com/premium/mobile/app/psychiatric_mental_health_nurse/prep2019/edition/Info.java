package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Info extends Activity {

	TextView t1;
	Typeface tf;
	ImageButton i1, i2, i3, i4, i5, i6;
	String application, market,market_name, facebook,email;
	Button i7, i8;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.info);
		i1 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i1);
		i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2);
		i3 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i3);
		i4 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i4);
		i5 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i5);
		i6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i6);
		i7 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.im3);
		i8 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.im2);
		t1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t1);
		
	TextView imm1 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.im1);
		
		application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
		market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
		market_name=getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market_name);
		email = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.email);
		facebook = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.facebook);
		t1.setTypeface(tf = Typeface.createFromAsset(getAssets(),
				"fonts/myriad.otf"));
	
		
		/*Animation animm1 = AnimationUtils.loadAnimation(this, R.anim.pushinhorizdown);
		imm1.startAnimation(animm1);
		
		LinearLayout l1= (LinearLayout) findViewById (R.id.l1);
		Animation anl1 = AnimationUtils.loadAnimation(this, R.anim.push_left_in);
		l1.startAnimation(anl1);
		LinearLayout l2= (LinearLayout) findViewById (R.id.l2);
		Animation anl2 = AnimationUtils.loadAnimation(this, R.anim.push_right_in);
		l2.startAnimation(anl2);
		
		Animation ant1 = AnimationUtils.loadAnimation(this, R.anim.pushinright);
		t1.startAnimation(ant1);*/
		
		Animation ani1 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i1.startAnimation(ani1);
		Animation ani2 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i2.startAnimation(ani2);
		Animation ani3 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i3.startAnimation(ani3);
		Animation ani4 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_down_in);
		i4.startAnimation(ani4);
		Animation ani5 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i5.startAnimation(ani5);
		Animation ani6 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i6.startAnimation(ani6);
		Animation ani7 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i7.startAnimation(ani7);
		Animation ani8 = AnimationUtils.loadAnimation(this, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_up_in);
		i8.startAnimation(ani8);

		String text1 = "<br><font color=#000080><b>World Class Learning App</b></font></br>"
				+ "<br>Did you know that this app have taken over 30 months of hard and forced work. The objective was to develop the best self-learning app in the world. We have added all the options and facilities that could be needed during the learning process. Today We feel proud of our achievement and we are sure that you are enjoying and taking benefit from our 2 years and half journey to build this outstanding app. </br>"
				+ "<br></br>"
				+"<br><font color=#000080><b>Who we are?</b></font></br>"
				+ "<br>Our Startup is  specialized in the development of self-learning applications using our innovative technology that doesn't need any internet connection, this means that you can use all our apps offline, anywhere and anytime.</br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>Our services</b></font></br>"
				+ "<br>We deliver high added value learning and entertainment apps. We are keen to share knowledge and trainings thru offline mobile applications for your personal development and continious improvement.</br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>Our Applications: </b></font></br>"
				+ "<br>All our Paid applications are advert free, however, freemium version may contain some ads in order to finance our activities, if your company intend to sponsor one of our application we may add its logo inside the application. </br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>Be Our Partner: </b></font></br>"
				+ "<br>We are working for the seek of knowledge, be our partner by suggesting new topics that may help students & professionals all over the world especially where the access to learning & knowledge is limited.</br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>Contact us: </b></font> </br>"
				+ "<br>If you have any comment or want to develop the content of this application, or to advertise please don't hesitate to contact us. </br>"
				+ "<br>Always don't forget that new ideas are our strongest capital, so please share your ideas with us.</br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>How can you help us: </b></font> </br>"
				+ "<br>If you are found of innovation and want more dedicated apps: Please rate us 5 stars, write down a nice comment from your side or just share the link of this app in your blog or your social media page, However if you are not satisfied please send us an email with your recommendations and we will review it carefully. </br>"
				+ "<br>We also recommend that you like our facebook page to get updated with our latest innovative and Hi-Tech products</br>"
				+ "<br></br>"
				+ "<br><font color=#000080><b>Copyrights: </b></font></br>"
				+ "<br>All information contained on this application are protected by authors' copyrights and may not be used without  written consent. However you are allowed  an encouraged to share the content of this app on social media  with the condition that it should be followed by the link of this app on Google Play Store.</br>"
				+ "<br></br>"
				
				+ "<br></br>"
				+"<br><font color=#000080><b>How to use this app? </b></font></br>"
						+ "<br>This application is designed to fit your exam preparation needs.\n The colors, the shapes, the writing style and the preparation modes, are meticulously chosen to fit your needs.\n\n</br>"
						+ "<br></br>"+ "<br>There are five (5) preparation modes in this application:</br>"
						+ "<br></br>"+ "<br>• The Test mode askes you questions in the order of the chapters. You tap on  \"Answer button\" to display the correct answer or tap on  \"next button\" to see the following question. After you read the answer, you should tap I\'m Right button if you feel that you got the answer or I\'m wrong button if you missed the right answer. After a number of questions you can ask for the meterwhite, however you should know that the scores are calculated live and displayed at the upper left corner above the questions field\n\n</br>"
						+ "<br></br>"+ "<br>• The Random Mode is the same as the test mode the main difference is that the questions are randomly shown, and not displayed in the normal order</br>"
						+ "<br></br>"+ "<br>• The Learning Mode, this mode shows you the question and the answer in the same time.</br>"
						+ "<br></br>"+ "<br>• The Handout Mode, you will read the questions and the answers like you are reading a normal book or a handout.</br>"
						+ "<br></br>"+ "<br>• The Slideshow Mode, displayes the questions and the answers at the same time for a minute or less and after that moves to the next fashcard automatically, it does not need any inervention from your side.</br>"
						+ "<br></br>"+ "<br>Please send us which mode you liked the most and if you don\'t mind drop us an email with your creative suggestions at:  </br>"
						+ "<br></br>"
		+email
				;
		t1.setText(Html.fromHtml(text1));

		application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
		i1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Apps.class);
				startActivity(i);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinright,
						com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushoutright);
			}
		});

		i2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Info.this, Tabs.class);
				startActivity(i);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinright,
						com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushoutright);

			}
		});

		i3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String[] to = { email };

				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(Intent.EXTRA_EMAIL, to);

				i.putExtra(Intent.EXTRA_SUBJECT, "Regarding your application: " + application + " from "
						+ market_name);
				i.putExtra(Intent.EXTRA_TEXT,
						"Dear Top Of Learning Team, \n\n I have a quick comment regarding you application :  \""
								+ application + "  from " + market_name + "\n\n");
				i.setType("message/rfc822");
				startActivity(i);

			}
		});
		i4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Info.this, Apps.class);
				startActivity(i);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinright,
						com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushoutright);

			}
		});

		i5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Info.this, Tabs.class);
				startActivity(i);
				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushinright,
						com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushoutright);

			}
		});
		i6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(facebook));
				startActivity(i);
			}
		});

		i7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent emailIntent = new Intent(
						Intent.ACTION_SEND);
				int imageURI = com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.icon;
				emailIntent.setType("text/plain");
				emailIntent.putExtra(Intent.EXTRA_SUBJECT,
						"An excellent application, I strongly recommend it for you");
				emailIntent.putExtra(Intent.EXTRA_TEXT, market);
				emailIntent.putExtra(Intent.EXTRA_TEXT,
						"Hi, \n\n I found an excellent android application from Google Play, I strongly recommend it for you:  \""
								+ application + "  from " + market_name + "\n\n"+ " here\'s the link: "+"\n\n"+ "https://play.google.com/store/apps/details?id="+ getPackageName()+"\n\n"+"All The Best.");
				emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				emailIntent.setType("image/*");
				emailIntent.putExtra(
						Intent.EXTRA_STREAM,
						Uri.parse("android.resource://" + getPackageName()
								+ "/" + imageURI));
				startActivity(emailIntent);
			}
		});

		i8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(market));
				startActivity(i);
			}
		});

	}
	//back button : this method is executed when the user click on his phone back button
	@Override
	public void onBackPressed(){


		Intent intent = new Intent(this,Tabs.class);
		startActivity(intent);
		overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
		//finish();

	}

	@Override
	public void onDestroy(){

		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
