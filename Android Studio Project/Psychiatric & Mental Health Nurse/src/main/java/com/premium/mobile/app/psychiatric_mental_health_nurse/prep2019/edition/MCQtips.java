package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

public class MCQtips extends AAText implements TextToSpeech.OnInitListener {
    TextView t2,title;
    ImageButton i3,i4,i9;
    int o=0,p=0,q=0;
    String abc,allText="",flashcard, quest1,text1,Comment1,colorStringBackground="#00000000",colorStringText="#000000";
    Typeface typeface1;


    //Newely added september 2018
    TextToSpeech tts;
    boolean zx=false;
    boolean boospeak=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.handout_new);

        iTextSize = 18;
        t2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t2);
        title = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.title);
        speaker = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.speaker);//speaker button
        i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2); //share button
        i3= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i3); // change the color of the background
        i4= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i4);  // change the font of the text
        i6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i6);  //zoomin button
        i7 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i7);   // zoomout button
        i8 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i8);
        i9= (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i9); //this is the home button
        i10 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i10);   // zoomout button


        tts = new TextToSpeech(getApplicationContext(), this);

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( 0, GridLayout.LayoutParams.FILL_PARENT, 0);
        super.i10.setLayoutParams(param);
        super.i8.setLayoutParams(param);
        Typeface tf2=Typeface
                .createFromAsset(getAssets(),"fonts/gothic.ttf");
        typeface1=tf2;
        title.setText("Exam Taking Tips & tricks");
        allText = titl1+tex1
        +titl2+tex2
        +titl3+tex3
          +titl4+tex4
                +titl5+tex5
                +titl6+tex6
                +titl7+tex7
                +titl8+tex8
                +titl9+tex9
                +titl10+tex10
                +titl11+tex11
                +titl12+tex12
                +titl13+tex13
                +titl14+tex14
                +titl15+tex15;


        displayTips();
        if(tts!=null){
            tts.stop(); }
        abc=t2.getText().toString();
        if (zx){speech(abc);} else {if(tts!=null){
            tts.stop();
            //tts.shutdown();
        }}

        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tts!=null){
                    tts.stop();
                }
                // 
                abc=t2.getText().toString();
                if (!zx){
                    speech(abc);
                    zx=true;
                    //  Log.v(TAG, "***********************!!!!!!!********************\n"+toSpeak);
                    speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
                } else {
                    tts.stop(); // this stops only
                    //tts.shutdown(); this method kills the process so you should initiate it back after using it
                    zx=false;
                    speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);}
            }});

        // This is the change of the color of the background
        i3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                o++;

                switch (o) {
                    case 1:
                        colorStringBackground="#000000";
                        colorStringText="#FFFFFF";t2.setBackgroundColor(Color.parseColor(colorStringBackground));

                        displayTips();

                        

                        Toast.makeText(getApplicationContext(),
                                "Night Mode Activated!", Toast.LENGTH_SHORT).show();

                        break;
                    case 2:
                        colorStringBackground="#F5A9E1";
                        colorStringText="#000000";      displayTips();
                        
                        Toast.makeText(getApplicationContext(),
                                "Night Mode Deactivated!", Toast.LENGTH_SHORT)
                                .show();

                        break;
                    case 3:

                        colorStringBackground="#30A9D0F5"; displayTips();
                        

                        break;
                    case 4:
                        colorStringBackground="#81F7D8"; displayTips();
                        
                        break;
                    case 5:
                        colorStringBackground="#A9F5A9"; displayTips();
                        

                        break;
                    case 6:
                        colorStringBackground="#D8F781"; displayTips();
                        
                        break;
                    case 7:

                        colorStringBackground="#F3F781"; displayTips();
                        

                        break;
                    case 8:

                        colorStringBackground="#F7BE81"; displayTips();
                        
                        break;
                    case 9:

                        colorStringBackground="#F5A9A9"; displayTips();
                        
                        Toast.makeText(getApplicationContext(),
                                "You look like playing with this app not studing ;)", Toast.LENGTH_SHORT)
                                .show();

                        break;
                    case 10:
                        colorStringBackground="#D0A9F5"; displayTips();
                        

                        break;
                    case 11:
                        colorStringBackground="#e6ccff"; displayTips();
                        

                        break;
                    case 12:
                        colorStringBackground="#8000ff";
                        colorStringText="#FFFFFF"; displayTips();
                        
                        ;

                        break;
                    case 13:
                        colorStringBackground="##ffcce6";
                        colorStringText="#000000"; displayTips();
                        


                    default:

                        colorStringBackground="#ff1a8c";
                        colorStringText="#FFFFFF"; displayTips();
                        

                        Toast.makeText(getApplicationContext(),
                                "Are you studing or playing with this app! :)",
                                Toast.LENGTH_SHORT).show(); displayTips();

                        o = 0;

                }}
        });


        // change the font of the text
        i4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {p++;

                switch (p) {
                    case 1:
                        Typeface tf3 = Typeface.createFromAsset(getAssets(),
                                "fonts/barbie.ttf");
                        typeface1=tf3; displayTips();
                        

                        break;

                    case 2:
                        Typeface tf4 = Typeface.createFromAsset(getAssets(),
                                "fonts/gothic.ttf");

                        typeface1=tf4; displayTips();
                        

                        break;
                    case 3:
                        Typeface tf5 = Typeface.createFromAsset(getAssets(),
                                "fonts/bold.ttf");
                        typeface1=tf5; displayTips();
                        

                        Toast.makeText(getApplicationContext(),
                                "Drop us your feedback regarding your best font! ;)",
                                Toast.LENGTH_SHORT).show(); displayTips();
                        break;

                    case 4:
                        Typeface tf11 = Typeface.createFromAsset(getAssets(),
                                "fonts/chalkdust.ttf");
                        typeface1=tf11;displayTips();


                        break;
                    case 5:
                        Typeface tf6 = Typeface.createFromAsset(getAssets(),
                                "fonts/frank.ttf");
                        typeface1=tf6;displayTips();

                        break;

                    case 6:
                        Typeface tf7 = Typeface.createFromAsset(getAssets(),
                                "fonts/pristina.ttf");
                        typeface1=tf7;displayTips();
                        break;

                    case 7:
                        Typeface tf8 = Typeface.createFromAsset(getAssets(),
                                "fonts/myriad.otf");
                        typeface1=tf8;displayTips();
                        

                        Toast.makeText(getApplicationContext(),
                                "Please try to  concentrate on your studies! :)",
                                Toast.LENGTH_SHORT).show();

                        p = 0; break;
                }


            }});



        //Zoomin  (text size) Button
        i6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                
                //dataAdapter.setViewBinder(binder);
                zoomIn();displayTips();



            }});

        // Zoomout  (text size) Button
        i7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                
                // dataAdapter.setViewBinder(binder);
                zoomOut();displayTips();


            }});

        // this the Home button
        i9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }});

// this the share button
        i2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        shareMethod();
            }});


    }
    private void speech(String charSequence) {

        int position=0 ;


        int sizeOfChar= charSequence.length();
        String testStri= charSequence.substring(position,sizeOfChar);


        int next = 1500;
        int pos =0;
        while(true) {
            String temp="";
            Log.e("in loop", "" + pos);

            try {

                temp = testStri.substring(pos, next);
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, temp);
                tts.speak(temp, TextToSpeech.QUEUE_ADD, params);

                pos = pos + 1500;
                next = next + 1500;

            } catch (Exception e) {
                temp = testStri.substring(pos, testStri.length());
                tts.speak(temp, TextToSpeech.QUEUE_ADD, null);
                break;

            }

        }

    }

    //zoomIn and zoomOut methods
    public void zoomIn(){
        if (iTextSize < 90)
        {iTextSize = iTextSize + 3;displayTips();}
         else   toastShort("Hey, This is the maximum font size allowed");//local method


    }

    public void zoomOut() {
        if (iTextSize > 6)
        {iTextSize = iTextSize - 3;displayTips();}
        else
            toastShort("Hey, This is the minimum font size allowed");

    }





    //speak function
    public void speakText() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(toSpeak);

            //toSpeak content is the comment_test() method
        } else {
            ttsUnder20(toSpeak);
        }
    }

    @SuppressWarnings("deprecation")
    public void ttsUnder20(String toSpeak) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(String toSpeak) {
        String utteranceId=this.hashCode() + "";
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }


public void displayTips(){

    t2.setTextSize(iTextSize);
    t2.setBackgroundColor(Color.parseColor(colorStringBackground));
    t2.setTextColor(Color.parseColor(colorStringText));
    t2.setTypeface(typeface1);
    t2.setText(Html.fromHtml(allText));
}

    @Override
    public void onInit(int status){

        if(status==TextToSpeech.SUCCESS){

            int result=tts.setLanguage(Locale.US);
        }

    }

    public void shareMethod() {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            String shareBody = "Hey,"
                    + "\n\n"
                    + "Just I would like to share with you these exam taking tips :"
                    + "\n\n"

                   +Html.fromHtml(titl1)
                    + "\n\n"
                    +Html.fromHtml(tex1)
                   + "\n\n\n"
                    + "If you wanna Get more exam taking tips & tricks and also plenty of outstanding study notes download this app  from Google Play  App Store:\n "
                    +"\"" + application +  "\"\n\n"+ "Here\'s the link: "+"\n\n"+ "https://play.google.com/store/apps/details?id="+ getPackageName()+"\n\n"+"All The Best.";;
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title.getText() + " from this App: " + application);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            sharingIntent.putExtra(Intent.EXTRA_BCC, email);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));





    }


    @Override
    public void onPause(){
        // Don't forget to shutdown tts!
        if(tts!=null){
            tts.stop();
            tts.shutdown();
            zx=false;speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
        }
        super.onPause();
       //  finish();
    }

    @Override
    public void onDestroy(){
        // Don't forget to shutdown tts!
        if(tts!=null){
            tts.stop();
            tts.shutdown();zx=false;
            speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
        }

        super.onDestroy();
    }

    //back button : this method is executed when the user click on his phone back button
    @Override
    public void onBackPressed()
    {if(tts!=null){
        tts.stop();
        tts.shutdown();
        zx=false; speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
    }
        Intent intent = new Intent(this,Tabs.class);
        startActivity(intent);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
        //finish();
        }

}