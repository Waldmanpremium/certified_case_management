package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

//import com.github.mikephil.charting.data.LineEntry;

public class Graph_Stuff extends Tabs {

    TextView t1,t2 ;
    Button i50, i51;
    BarChart chart;
    PieChart mChart;
    LineChart lineChart;
    int i=0;
    Date startDate;
    Date endDate;
    String myStartTime, myEndTime;
    int Learning_Mode, Handout_Mode, Test_Mode, Random_Mode, Slideshow_Mode;
    long different;
    private DatabaseAccess dbHelper;
    float average, means;

    ImageView topImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.graph_stuff);
        dbHelper = new DatabaseAccess(this);

        i50 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i50);
        i51 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i51);
        t1 =(TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t1);
        t2 =(TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.t2);
         chart = (BarChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chart);
        mChart = (PieChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.pieChart);
        lineChart = (LineChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lineChart);



        topImage= (ImageView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.topImage);
        dbHelper.open();
        testSessionNber= dbHelper.getTestSession_tableCount()   ;
        sessionNber=dbHelper.getSession_tableCount()   ;
        quizNber = dbHelper.getflash_tableCount();
        pieChartButton1();




        i50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             i++;
                switch (i) {
                    case 1:  barChartButton(); break;
                    case 2:  lineChartButton(); break;
                    case 3: barChartTimeButton();break;
                    case 4: pieChartButton6(); break;
                    case 5:  pieChartButton2(); break;
                    case 6:  pieChartButton3(); break;
                    case 7:  pieChartButton4(); break;
                    case 8:  pieChartButton5(); break;

                    case 9:  pieChartButton1();i=0; break;
                }

            }});

        i51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        Intent i4 = new Intent(getApplicationContext(),StatisticsCursorAdaptorActivity.class);

        startActivity(i4);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
            }});
    }




    public void barChartMethod(){
        // in this line a BarChart is initialized from xml


        ArrayList<BarEntry> barEntries = new ArrayList<>();

        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();

        XAxis xAxis = chart.getXAxis();

        for (int i=testSessionNber; i>=testSessionNber-8;i--){
            Cursor cursor = dbHelper.getSomethingFromTestSESSION_Table(i);
            if (i>=1){
        results= cursor.getInt(cursor.getColumnIndexOrThrow("Session_Results"));
        barEntries.add(new BarEntry(i,results));} else{break;}}

        BarDataSet barDataSet = new BarDataSet( barEntries, "last sessions");
        BarData data = new BarData(barDataSet);
        data.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
        //barDataSet.setColor(Color.parseColor("#104E78"));
        barDataSet.setValueTextColor(Color.parseColor("#FF8080"));
        //barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        Random r = new Random();
        int g =r.nextInt(12);
        switch (g){
            case 0: barDataSet.setColors(ColorTemplate.MATERIAL_COLORS); break;
            case 1: barDataSet.setColors(ColorTemplate.COLORFUL_COLORS); break;
            case 2: barDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS); break;
            case 3: barDataSet.setColors(ColorTemplate.LIBERTY_COLORS); break;
            case 4: barDataSet.setColors(ColorTemplate.PASTEL_COLORS); break;
            case 5: barDataSet.setColors(ColorTemplate.JOYFUL_COLORS); break;
            case 6: barDataSet.setColors(ColorTemplate.getHoloBlue()); break;
            case 7: barDataSet.setColor(Color.parseColor("#104E78"));
            case 8: barDataSet.setColor(Color.parseColor("#B4045F"));
            case 9: barDataSet.setColor(Color.parseColor("#0489B1"));
            case 10: barDataSet.setColor(Color.parseColor("#688A08"));
            case 11: barDataSet.setColor(Color.parseColor("#9A2EFE"));
            default: barDataSet.setColors(Color.parseColor("#DF7401")); break;
        }
        ArrayList<String> sessions = new ArrayList<>();
        sessions.add("Session"+testSessionNber+1);
        sessions.add("Session"+(testSessionNber));
        sessions.add("Session"+(testSessionNber-1));
        sessions.add("Session"+(testSessionNber-2));
        sessions.add("Session"+(testSessionNber-3));

        BarData  barData = new BarData(barDataSet);
        chart.setData(barData);
        barData.setValueTextSize(8);
        chart.animateY(1200);
        //chart.animateY(3000, Easing.EasingOption.EaseInBounce);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(sessions));
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(true);
        chart.getDescription().setText("Last Sessions meterwhite Barchart "+"\n"+"Empowered by "+Startup_Name);
        leftAxis.setAxisMinimum(0);
        leftAxis.setAxisMaximum(100);
        leftAxis.setTextSize(10f);
        rightAxis.setEnabled(false);
        xAxis.setEnabled(false);
        chart.invalidate();
    }

    public void pieChartMethod(float average,int raw, String A, String B, String description){
        //PieChart mChart = (PieChart) findViewById(R.id.pieChart);

        ArrayList<PieEntry> pieChartEntries = new ArrayList<>();

        pieChartEntries.add(new PieEntry(average, A));
        pieChartEntries.add(new PieEntry(100-average, B));
      PieDataSet set = new PieDataSet(pieChartEntries, "");

        PieData data = new PieData(set);
        data.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
        mChart.setData(data);
        Random r = new Random();
        int g =r.nextInt(5);
        switch (g){
        case 0: set.setColors(ColorTemplate.MATERIAL_COLORS); break;
        case 1: set.setColors(ColorTemplate.COLORFUL_COLORS); break;
        case 2: set.setColors(ColorTemplate.VORDIPLOM_COLORS); break;
        case 3: set.setColors(ColorTemplate.LIBERTY_COLORS); break;
        case 4: set.setColors(ColorTemplate.PASTEL_COLORS); break;
        case 5: set.setColors(ColorTemplate.JOYFUL_COLORS); break;
        case 6: set.setColors(ColorTemplate.getHoloBlue()); break;
        default: set.setColors(Color.parseColor("#FF8080")); break; }

        mChart.setUsePercentValues(true);
        mChart.setCenterText(raw+" "+A);
        mChart.setCenterTextSize(30);
        mChart.setCenterTextColor(Color.parseColor("#FF8080"));

        mChart.animateY(1000);
        mChart.setTouchEnabled(true);
        data.setValueTextSize(15);
        mChart.getDescription().setText(description);
        //change the color of label within the pie chart
        mChart.setEntryLabelColor(Color.parseColor("#3B0B17"));
       mChart.setBackgroundColor(Color.parseColor("#dbdbdb"));

        mChart.invalidate();


    }



    public void pieChartModeMethod(){
        //PieChart mChart = (PieChart) findViewById(R.id.pieChart);

        ArrayList<PieEntry> pieChartEntries = new ArrayList<>();
        categoryChart();
        pieChartEntries.add(new PieEntry(Learning_Mode,"Learning Mode"));
        pieChartEntries.add(new PieEntry(Handout_Mode, "Handout Mode"));
        pieChartEntries.add(new PieEntry(Test_Mode,"Test Mode"));
        pieChartEntries.add(new PieEntry(Random_Mode, "Random Mode"));
        pieChartEntries.add(new PieEntry(Slideshow_Mode, "Slideshow Mode"));
        PieDataSet set = new PieDataSet(pieChartEntries, "");

        PieData data = new PieData(set);
       data.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
        mChart.invalidate();
        mChart.setData(data);
        Random r = new Random();
        int g =r.nextInt(5);
        switch (g){
            case 0: set.setColors(ColorTemplate.MATERIAL_COLORS); break;
            case 1: set.setColors(ColorTemplate.COLORFUL_COLORS); break;
            case 2: set.setColors(ColorTemplate.VORDIPLOM_COLORS); break;
            case 3: set.setColors(ColorTemplate.LIBERTY_COLORS); break;
            case 4: set.setColors(ColorTemplate.PASTEL_COLORS); break;
            case 5: set.setColors(ColorTemplate.JOYFUL_COLORS); break;
            case 6: set.setColors(ColorTemplate.getHoloBlue()); break;
            default: set.setColors(Color.parseColor("#FF8080")); break; }

        mChart.setUsePercentValues(true);
        mChart.setCenterText(sessionNber+" sessions");
        mChart.setCenterTextSize(39);
        mChart.setCenterTextColor(Color.parseColor("#FF8080"));

        mChart.animateY(1000);
        mChart.setTouchEnabled(true);
        data.setValueTextSize(15);
        mChart.getDescription().setText("Sessions By Study Mode");
        //change the color of label within the pie chart
        mChart.setEntryLabelColor(Color.parseColor("#3B0B17"));
        mChart.setBackgroundColor(Color.parseColor("#dbdbdb"));

        mChart.invalidate();


    }


    public void lineChartMethod() {

        ArrayList<Entry> lineEntries = new ArrayList<Entry>();

         YAxis leftAxis = lineChart.getAxisLeft();
         YAxis rightAxis = lineChart.getAxisRight();

     XAxis xAxis = lineChart.getXAxis();
        int i;
        if(testSessionNber>20){i=(testSessionNber-20);}else i=1;
     // for (int i = testSessionNber; i > (testSessionNber-1); i--) {
        while (i<=testSessionNber) {
            try {
            Cursor cursor2 = dbHelper.getSomethingFromTestSESSION_Table(i);

                results = cursor2.getInt(cursor2.getColumnIndexOrThrow("Session_Results"));
                lineEntries.add(new Entry(i, results));i++;

            }catch (Exception e) {
                e.printStackTrace(); }}



        LineDataSet lineDataSet = new LineDataSet(lineEntries, "Scores Evolution");
        //LineData data = new LineData(lineDataSet);


     lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(3);
        lineDataSet.setColor(Color.parseColor("#60bff9"));
        lineDataSet.setCircleColor(Color.parseColor("#7af67c"));
        lineDataSet.setCircleRadius(4);
        lineDataSet.setCircleHoleRadius(2);
        lineDataSet.setDrawHighlightIndicators(true);
        lineDataSet.setHighLightColor(Color.RED);
        lineDataSet.setValueTextSize(8);
        lineDataSet.setValueTextColor(Color.parseColor("#636161"));

        LineData lineData = new LineData(lineDataSet);
        lineData.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
        lineChart.getDescription().setText("Scores of the last test sessions");
        lineChart.getDescription().setTextSize(8);
        lineChart.setDrawMarkers(true);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        lineChart.animateY(1000);
        lineChart.getXAxis().setGranularityEnabled(true);
        lineChart.getXAxis().setGranularity(1.0f);
        lineChart.getXAxis().setLabelCount(lineDataSet.getEntryCount());
        leftAxis.setAxisMinimum(0);
        leftAxis.setAxisMaximum(100);
        leftAxis.setTextSize(12f);
        rightAxis.setEnabled(false);
        lineChart.setData(lineData);
        lineChart.invalidate();

        if(testSessionNber>20){xAxis.setAxisMinimum(testSessionNber-20);}else xAxis.setAxisMinimum(0);

        lineChart.setBackgroundColor(Color.parseColor("#dbdbdb"));
    }

    public void barChartTimeMethod(){
        // in this line a BarChart is initialized from xml


        ArrayList<BarEntry> barEntries = new ArrayList<>();

        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();

        XAxis xAxis = chart.getXAxis();

        for (int i=sessionNber; i>=sessionNber-8;i--){
            Cursor cursor = dbHelper.getSomethingFromSESSION_Table(i);
            if (i>1){

            long xcv = getTimeDifference(i);
           barEntries.add(new BarEntry(i,xcv));}
           else{break;}}

        BarDataSet barDataSet = new BarDataSet( barEntries, "Sessions");
        BarData data = new BarData(barDataSet);

        barDataSet.setValueTextColor(Color.parseColor("#FF8080"));
        Random r = new Random();
        int g =r.nextInt(12);
        switch (g){
            case 0: barDataSet.setColors(ColorTemplate.MATERIAL_COLORS); break;
            case 1: barDataSet.setColors(ColorTemplate.COLORFUL_COLORS); break;
            case 2: barDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS); break;
            case 3: barDataSet.setColors(ColorTemplate.LIBERTY_COLORS); break;
            case 4: barDataSet.setColors(ColorTemplate.PASTEL_COLORS); break;
            case 5: barDataSet.setColors(ColorTemplate.JOYFUL_COLORS); break;
            case 6: barDataSet.setColors(ColorTemplate.getHoloBlue()); break;
            case 7: barDataSet.setColor(Color.parseColor("#104E78"));
            case 8: barDataSet.setColor(Color.parseColor("#B4045F"));
            case 9: barDataSet.setColor(Color.parseColor("#0489B1"));
            case 10: barDataSet.setColor(Color.parseColor("#688A08"));
            case 11: barDataSet.setColor(Color.parseColor("#9A2EFE"));
            default: barDataSet.setColors(Color.parseColor("#DF7401")); break;
        }

        ArrayList<String> sessions = new ArrayList<>();
        sessions.add("Session"+sessionNber);
        sessions.add("Session"+(sessionNber-1));
        sessions.add("Session"+(sessionNber-2));
        sessions.add("Session"+(sessionNber-3));
        sessions.add("Session"+(sessionNber-4));
        sessions.add("Session"+(sessionNber-5));
        chart.invalidate();
        BarData  barData = new BarData(barDataSet);
        chart.setData(barData);
        barData.setValueTextSize(10);
        chart.animateY(1000);
        //chart.animateY(3000, Easing.EasingOption.EaseInBounce);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(sessions));
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(true);
        chart.getDescription().setText("Time per session (in Minutes)");
        leftAxis.setAxisMinimum(0);
        leftAxis.setAxisMaximum(75);
        leftAxis.setTextSize(12f);
        rightAxis.setEnabled(false);
        xAxis.setEnabled(false);
        chart.invalidate();
    }


    public float means(int cards){

        means = cards*100/quizNber;
        return means;
    }




    public float average(){
        average =0;
        for (int i=1; i<=testSessionNber;i++) {
            Cursor cursor1 = dbHelper.getSomethingFromTestSESSION_Table(i);
            int xcc= cursor1.getInt(cursor1.getColumnIndexOrThrow("Session_Results"));
            average=(average+xcc);
        }
        average=average/(testSessionNber);

        return average;
    }


    public void barChartButton(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        mChart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        chart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
        chart.startAnimation(anb15);
        t1.setText("Scores of the last test sessions");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.chartwhite);
        t2.setText("** Each bar represents the result of a test session");

        barChartMethod();

    }


    public void pieChartButton2(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        mChart.startAnimation(anb15);
        t1.setText("Studied Flashcards Percentage - Pie Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.notseenwhite);
        t2.setText("** The percentage of studied flashcards is calculated by dividing the total number of studied cards: " +(quizNber-nonStudied1)+ " by the total number of flashcards: "+quizNber);
        pieChartMethod(means(quizNber-nonStudied1), quizNber-nonStudied1,"Studied Cards", "Not Studied", "Studied Flashcards Percentage");

    }

    public void pieChartButton3(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
        mChart.startAnimation(anb15);
        t1.setText("Flagged for review %age - Pie Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markclicked);
        t2.setText("** The percentage of Flagged for review cards is calculated by dividing the number of Flagged for review cards: " +marked1+ " by the total number of flashcards: "+quizNber);

        pieChartMethod(means(marked1), marked1,"Flagged For review cards", "Non-Flagged For Review", "Marked for review flashcards %age");



    }
    public void pieChartButton4(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        mChart.startAnimation(anb15);
        t1.setText("Favorite Study Notes %age - Pie Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoritewhite);
        t2.setText("** The percentage of Favorite study notes is calculated by dividing the total number of Favorite cards: " +favorite1+ " by the total number of flashcards: "+quizNber);
        pieChartMethod(means(favorite1), favorite1,"Favorite cards", "Non-favorite cards", "Favorite Study Notes %age");

    }
    public void pieChartButton5(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
        mChart.startAnimation(anb15);
        t1.setText("Your Own Flashcards %age - Pie Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcardswhiteicon);
        t2.setText("** The percentage of your own cards that you added to this app is calculated by dividing the total number of your own added cards: " +own1+ " by the total number of flashcards: "+quizNber);
        pieChartMethod(means(own1),own1, "My Own cards", "App Cards", "Overall Test Results");

    }
    public void pieChartButton1(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        mChart.startAnimation(anb15);
        t1.setText("Overall Results - Pie Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.chartwhite);
        t2.setText("** The percentage of correct answers is calculated by summing the meterwhite of the last 5 test sessions devided by the number of sessions");

        pieChartMethod(average(),(int)(average()), "Correct answers", "Wrong answers", "Overall Test Results");

    }

    public void pieChartButton6(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        mChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_left_in);
        mChart.startAnimation(anb15);
        t1.setText("Distribution of study session by study modes");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.chartwhite);
        t2.setText(Learning_Mode+"** The percentage of correct answers is calculated by summing the meterwhite of the last 5 test sessions devided by the number of sessions");

        pieChartModeMethod();

    }

    public void lineChartButton(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        chart.setLayoutParams(param);
        mChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        lineChart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        lineChart.startAnimation(anb15);
        t1.setText("Your Scores Evolution - Line Chart Analysis");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.line);
        t2.setText("** This line chart showcases the development of your meterwhite throughout  the last test sessions");
        lineChartMethod();

    }

    public void barChartTimeButton(){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 0);
        mChart.setLayoutParams(param);
        lineChart.setLayoutParams(param);
        LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams( GridLayout.LayoutParams.MATCH_PARENT, 0, 10);
        chart.setLayoutParams(param1);
        Animation anb15 = AnimationUtils.loadAnimation(this,
                com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.push_right_in);
        chart.startAnimation(anb15);
        t1.setText("Time Elapsed in the last sessions");
        topImage.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.meterwhite);
        t2.setText("** This Bar Chart showcases the time spent (in minutes) on each session. This is a measurement of you engagement towards this material");
        barChartTimeMethod();

    }


    public long getTimeDifference(int i){


        dbHelper.open();

        myStartTime =  dbHelper.getStart(i);
        myEndTime = dbHelper.getFinish(i);
       SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        if((myStartTime != null && !myStartTime .isEmpty())  && (myEndTime != null    && !myEndTime .isEmpty())) {
            try {
                startDate = sdf.parse(myStartTime);
                endDate = sdf.parse(myEndTime);
            } catch (ParseException e) {
                e.printStackTrace();Toast.makeText(getApplicationContext(),
                        "Sorry An error has occured. Write an email to us! "+myStartTime+"   "+myEndTime, Toast.LENGTH_LONG).show();
            }

        }


        //milliseconds
       try {  different = endDate.getTime() - startDate.getTime();} catch (Exception e) {

            e.printStackTrace(); }

    return (different/60000);

    }


    public void categoryChart(){
        Learning_Mode=0;
        Handout_Mode=0;
        Test_Mode=0;
        Random_Mode=0;
        Slideshow_Mode=0;

        for (int i=1; i<=sessionNber;i++)  {
            Cursor cursor1 = dbHelper.getSomethingFromSESSION_Table(i);
            int category = cursor1.getInt(cursor1.getColumnIndexOrThrow("Session_Mode"));
            switch (category){

                case 1: Learning_Mode++;  break;
                case 2: Handout_Mode++;  break;
                case 3: Test_Mode++;  break;
                case 4: Random_Mode++;  break;
                case 5: Slideshow_Mode++;  break;

            }


        }


    }



    @Override
    public void onBackPressed(){


        Intent intent = new Intent(this,Tabs.class);
        startActivity(intent);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
        //finish();

    }

    @Override
    public void onDestroy(){

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}


