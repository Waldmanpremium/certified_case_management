package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class ParentClass extends Activity {

    //Newely added september 2018
    TextToSpeech tts;
    boolean zx=false;

    //Variable declaration
   public DatabaseAccess dbHelper;

    boolean boospeak=false;
    //private TextToSpeech tts;
    ImageView i8, i10;
    TextView seen_times, title, gotoquiz;
    ImageButton addcards, addcomment, edit, correct, favorite, marked, speaker,
             i2, i7, i6;
    Button bquestion1, banswer1, bquestion2, banswer2,butt0, bcomment;
    LinearLayout lquestion, lanswer, lcomment, llistview, l1, ll1, toptop,lq1,lq2,lans1,lans2,
            ltitle,but3,but1, butt1,
            butt2, butt3, butt4;
    EditText flashNbers, q, ans, comment;
    ListView listTextMode;
    short c=0,f=0,o=0,v=0;

    int num, position, i = 1, del, chel, xxy = 1, xxx = 1, views=1, sessionNber,testSessionNber, responded,wrong,m;
    String xcomment, xedit, toSpeak,myDate,myTime,application,email, market,market_name, app_version, root , q1, ans1, comment1,title1;
    int iTextSize ;

    //back button : this method is executed when the user click on his phone back button



public void pageLoader() {


    setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.apasspartout);

    seen_times = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.seen_times);
    q = (EditText) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.q);
    ans = (EditText) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ans);
    title = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.title);
    gotoquiz = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.gotoquiz);

    addcards = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.addcards);
    addcomment = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.addcomment);
    edit = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.edit);
    correct = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.correct);
    marked = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.marked);
    speaker = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.speaker);
    favorite = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.favorite);
    speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);

    i2 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i2); //share button
    i6 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i6);  //zoomin button
    i7 = (ImageButton) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i7);   // zoomout button
    i8 = (ImageView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i8);   //previous chapter
    i10 = (ImageView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.i10);   // Next chapter

    bquestion1 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.bquestion1);
    banswer1 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.banswer1);
    bquestion2 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.bquestion2);
    banswer2 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.banswer2);
    bcomment = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.bcomment);
    but1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.but1);  //previous flashcard
    but3 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.but3);  //next flashcard
    butt0 = (Button) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.butt0); //  answer
    butt1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.butt1); // Correct answer
    butt2 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.butt2);  //Wrong answer
    butt3 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.butt3);   //Discard
    butt4 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.butt4);   //Results

    lquestion = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lquestion);
    lanswer = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lanswer);
    lcomment = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lcomment);
    llistview = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.llistview);
    l1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.l1);
    ll1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ll1);
    toptop = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.toptop);
    ltitle = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.ltitle);
    lq1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lq1);
    lq2 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lq2);
    lans1 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lans1);
    lans2 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lans2);
    // ScrollView sc1 =(ScrollView) findViewById (R.id.sc1);
    // ScrollView sc2 =(ScrollView) findViewById (R.id.sc2);
    comment = (EditText) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.comment);
    flashNbers = (EditText) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.flashNbers);

    listTextMode = (ListView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.listTextMode);
    final Context context = getApplicationContext();

    market_name=getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market_name);
    application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
    market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
    email = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.email);
    root = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.root_address)
            + getPackageName();
    app_version = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_version);
    iTextSize = 18;
    // final Drawable aImg = (Drawable)addcards.getBackground();
    // final Drawable bImg =
    // getResources().getDrawable(R.drawable.addcardswhiteicon);

    // layout parameters see the function down at the bottom of this page



     dbHelper = new DatabaseAccess(this);
    //dbHelper.getInstance(this);
    dbHelper.open();

    num=getIntent().getIntExtra("somekey2",1);
    sessionNber = getIntent().getIntExtra("somekey3",1);

    i = getIntent().getIntExtra("somekey4",1);
    responded = getIntent().getIntExtra("somekey5",0);
    wrong = getIntent().getIntExtra("somekey6",0);
    int marq = getIntent().getIntExtra("somekey7",6);
    del = dbHelper.getNumberOfFlashcards(num);
    chel = dbHelper.getChap_tableCount();
    //We update the session table dtat with this method the session data
    session_Tracker();
    m=dbHelper.marked_method(num, i);
    testSessionNber= dbHelper.getTestSession_tableCount();



    //********  Actions to be executed when the button is clicked by the inherited class***********
//*************************************************************************************************
// Zoomin  (text size) Button
    i6.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {zoomIn();}

    });

// Zoomout  (text size) Button
    i7.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            zoomOut();
        }

    });

// This is the share Button
    i2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //displayOneView(num, i);
            shareMethod();
            displayOneView(num, i);}

    });


// Activate and deactivate button
    speaker.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!zx){
                read_the_card();zx=true;
                speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakermuteblack);
            } else {tts.stop();tts.shutdown();
                zx=false;
                speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakermuteblack);}
        }});

//Previous Chapter imageButton
    i8.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            previousChapterButton();

        }
    });

//Next Chapter imageButton
    i10.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {nextChapterButton();
           }
    });

// next flashcard button
    but3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            del = dbHelper.getNumberOfFlashcards(num);
            if (i < del) {
                i = i + 1;

                displayCheckAllForButons(i,8);

            }

            else toastShort("Hey, this is the last flashcard");
        }
    });
// Previous flashcard button
    but1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (i > 1) {
                i = i - 1;
                views=views+1;
                displayCheckAllForButons(i,8);

            } else toastShort("Hey, this is the first flashcard");
        }
    });

    // Goto Quiz number	# Button
    gotoquiz.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            layoutParameters();
            String quizNber = flashNbers.getText().toString();
            if (quizNber == "") toastShort("Please type the quiz number you want to goto");

            int quizInt = Integer.parseInt(quizNber);
            if (quizInt == 0) toastShort("Please type the quiz number you want to goto, the number should be between 1 and "
                    + del);


            if (quizInt > del) toastShort("Please type the quiz number you want to goto, the number should be between 1 and "
                    + del);

            if (quizInt > 0 && quizInt <= del) {
                displayOneView(num, quizInt);
                i = quizInt;
                flashNbers.clearFocus();
                views=views+1;
                session_Tracker();
                favorite_test();
                seen_before_test();
                marked_test();
                correct_test();
                Comment_test();
                ownFlash_test();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(flashNbers.getWindowToken(), 0);

                ;
            }
            // setFocusable(false)
        }
    });


// Add Comment Button
    addcomment.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            xcomment = dbHelper.return_Comment(num, i);
            // String comment1=comment.getText().toString();
            comment.setEnabled(true);
            if (xcomment != "0") {
                toastShort("Already you have a comment!\n You Can edit your comment again.\nPlease SAVE it, once you are done.");
                commentWeight(5);
                pageWeight(3);
                bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcommentsave);
                comment.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(comment, InputMethodManager.SHOW_IMPLICIT);
            } else {
                if (xxx != 2) {

                    toastShort("Please Write down your comment!\nKindly SAVE it, once you are done.");
                    bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcommentsave);

                    commentWeight(5);

                    pageWeight(3);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(comment,
                            InputMethodManager.SHOW_IMPLICIT);
                    comment.setText("Write your comment here!");
                    comment.requestFocus();

                    xxx = 2;
                } else {
                    commentWeight(0);
                    pageWeight(8);
                    comment.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
                    bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcomment);
                    xxx = 1;
                }
            }

        }

    });
// This button save the comment added
    bcomment.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String comment1 = comment.getText().toString();
            bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcomment);

            // DONT USE ==Null or !Null in if condition or elsewhere in any
            // test condition, NEVER EVER
            if (!comment1.isEmpty()) {

                dbHelper.saveComment(num, i, comment1, getBaseContext().getApplicationContext());
                addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentclicked);
                toastShort("Your Comment is saved successfully");
                comment.clearFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);

                pageWeight(8);

            } else {
                dbHelper.saveComment(num, i, comment1, getBaseContext().getApplicationContext());
                addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentblack);
                toastShort("This Flahcard without any comment!");


                commentWeight(0);
                pageWeight(8);
                comment.clearFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);

            }
        }

    });
//bquestion 1 save the edited question
    bquestion1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String edit_q = q.getText().toString();
            bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);

            dbHelper.saveEdit_question(num, i, edit_q, getBaseContext().getApplicationContext());
            edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
            toastShort("Your Edited Question was saved successfully");
            q.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(q.getWindowToken(), 0);
            question_answer_editext_test(false, false);
            pageWeight(8);

        }
    });
//bquestion2  to save the new question
    bquestion2.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {

                                          // int j1=R.drawable.ccaddcards;
                                          // if(addcards.getId()==j1){

                                          // if(aImg==bImg){

                                          // if (!q.getText().toString().isEmpty()){
                                          if (!q.getText().toString().isEmpty()) {
                                              del = del + 1;
                                              dbHelper.insertLog_q(q.getText().toString(), ans.getText()
                                                      .toString(), num, del, 1, getBaseContext().getApplicationContext());
                                              toastShort("Your new Flashcard was saved successfully");
                                              q.clearFocus();
                                              ans.requestFocus();
                                              question_answer_editext_test(false, true);
                                              pageWeight(8);
                                              bquestion2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
                                              banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswersave);

                                          } else {
                                              toastShort("Please, add your Question/Term before saving!");
                                          }
                                      }

                                  }

    );
//banswer 1  to save the edited answer
    banswer1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String edit_ans = ans.getText().toString();
            banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

            dbHelper.saveEdit_answer(num, i, edit_ans, getBaseContext().getApplicationContext());
            edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
            toastShort("Your Edited Answer was saved successfully");
            ans.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(ans.getWindowToken(), 0);
            question_answer_editext_test(false, false);
            pageWeight(8);

        }
    });
//banswer2  to save the new answer
    banswer2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!ans.getText().toString().isEmpty()) {
                dbHelper.insertLog_ans(q.getText().toString(), ans
                        .getText().toString(), num, del, 1, getBaseContext().getApplicationContext());
                toastShort("Your new Flashcard was saved successfully");
                ans.clearFocus();

                question_answer_editext_test(false, false);
                pageWeight(8);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(q.getWindowToken(), 0);
                rightSide1_Weight(1);
                rightSide2_Weight(0);
                banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);
            } else {
                toastShort("Please, add your Answer/Explanation/Definition before saving!");
            }

        }

    });

// edit button To edit question and then answer
    edit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            rightSide1_Weight(1);
            rightSide2_Weight(0);
            edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
            if (xxx == 1) {

                toastShort("After editing the question.\nPlease SAVE it. to edit the answer click again on Edit button");
                question_answer_editext_test(true, false);
                commentWeight(0);
                q_Weight(10);
                ans_Weight(3);

                bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestionsave);
                banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

                q.setCursorVisible(true);
                q.setFocusableInTouchMode(true);
                ans.setFocusable(true);
                q.requestFocus(); // to trigger the soft input

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(q, InputMethodManager.SHOW_IMPLICIT);
                xxx = xxx + 1;
            } else {
                if (xxx == 2) {
                    {

                        toastShort("After editing the Answer.\nPlease SAVE it. to edit the question click again on Edit button" );
                        question_answer_editext_test(false, true);
                        banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswersave);
                        bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
                        commentWeight(0);

                        q_Weight(3);
                        ans_Weight(8);

                        ans.setCursorVisible(true);
                        ans.setFocusableInTouchMode(true);
                        ans.setFocusable(true);
                        ans.requestFocus();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(ans,
                                InputMethodManager.SHOW_IMPLICIT);



                        xxx = 1;

                    }

                }
            }
        }

    });

//addcard button is for adding a new flashcard
    addcards.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rightSide1_Weight(0);
            rightSide2_Weight(1);
            del = dbHelper.getNumberOfFlashcards(num);
            i = del + 1;
            displayQuizNber(i);

            edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
            addcards.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcardswhiteicon);
            // xedit = dbHelper.return_Edit(num, i);
            q.getText().clear();
            ans.getText().clear();
            // String edit_ans=ans.getText().toString();

            if (xxy == 1) {
                q.getText().clear();
                ans.getText().clear();
                Toast.makeText(
                        getApplicationContext(),
                        "Kindly write down your own question or term \nPlease don't save it until you write the answer",
                        Toast.LENGTH_SHORT).show();
                question_answer_editext_test(true, false);
                commentWeight(0);
                q_Weight(10);
                ans_Weight(3);

                bquestion2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestionsave);
                banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

                q.setCursorVisible(true);
                q.setFocusableInTouchMode(true);

                q.requestFocus(); // to trigger the soft input

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(q, InputMethodManager.SHOW_IMPLICIT);
                xxx = xxx + 1;
            } else {
                if (xxx == 2) {
                    {

                        ans.getText().clear();
                        toastShort("After writing the Answer.\nPlease SAVE it. to edit the question click again on Edit button");
                        question_answer_editext_test(false, true);
                        banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswersave);
                        bquestion2
                                .setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
                        commentWeight(0);

                        q_Weight(3);
                        ans_Weight(8);

                        ans.setCursorVisible(true);
                        ans.setFocusableInTouchMode(true);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(ans,
                                InputMethodManager.SHOW_IMPLICIT);
                        q.setFocusable(true);
                        ans.requestFocus();

                        xxx = 1;

                    }

                }
            }
        }

    });

    //marked imagebutton is to add the flashcard to marked flashcards
    marked.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int button = dbHelper.marked_click(num, i);

            if (button == 1) {

                marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markclicked);
               toastShort("Marked for review successfully: " + button);
            } else {

                marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markblack);
                toastShort("Removed from Marked for review successfully: "+ button);
            }
        }
    });
//correct imageButton is to give you information that the flashcard was tested successfully or not
    correct.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int button = dbHelper.correct_method(num, i);

            if (button == 1) {
                toastShort("This Quiz was answered correctly during the previous test");
            } else if (button == 2) {

                toastShort("This Quiz was answered wrongly during the previous test");
            } else {

                toastShort("This Quiz was not tested yet!");
            }
        }
    });


    favorite.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int button = dbHelper.favorite_click(num, i);

            if (button == 1) {

                favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteclicked);
                toastShort("Added to favorite successfully: " + button);
            } else {

                favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteblack);
                toastShort("Removed from favorite successfully: " + button);

            }
        }
    });



}




 //these methods are for button actions

    public void previousChapterButton(){
    if (num > 1) {
        num = num - 1;
        i = 1;
        views=views+1;
        displayCheckAllForButons(i,8);
        toastShort("You have just moved to the previous section/chapter");
    } else toastShort("Hey, this is the first section / chapter");}

    public void previousChapterButton(int i){
        if (num > 1) {
            num = num - 1;

            views=views+1;
            displayCheckAllForButons(i,8);
            toastShort("You have just moved to the previous section/chapter");
        } else toastShort("Hey, this is the first section / chapter");}

    public void nextChapterButton(){
    if (num < chel) {
        num = num + 1;
        i = 1;
        views=views+1;
        displayCheckAllForButons(i,8);
        toastShort("You have just moved to the next section/chapter");

    }

    else toastShort("Hey, this is the last section / chapter");}

    public void nextChapterButton(int i){
        if (num < chel) {
            num = num + 1;

            views=views+1;
            displayCheckAllForButons(i,8);
            toastShort("You have just moved to the next section/chapter");

        }

        else toastShort("Hey, this is the last section / chapter");}
    // this method checks the status of buttons and displays the content
// (question answers, comments) it is used when you open the app for the first time
public void displayCheckAll(int fid){
//15 methods
    layoutParameters();
    displayOneView(num, fid);
    getChapterTitle();
    favorite_test();
    seen_before_test();
    marked_test();
    correct_test();
    displayQuizNber(fid);
    Comment_test();
    ownFlash_test();
    rightButtons_test();
    Hide_softKey_test(ans);
    Hide_softKey_test(q);
    Hide_softKey_test(comment);
    question_answer_editext_test(false, false);
    session_Tracker();
}

// this method checks the status of buttons and displays the content
// (question answers, comments) it is used whin the button funtions like nextflash
public void displayCheckAllForButons(int fid, int pagewight){
   //18 methods
    getChapterTitle();
    displayOneView(num, fid);
    layoutParameters();
    favorite_test();
    seen_before_test();
    marked_test();
    correct_test();
    displayQuizNber(fid);
    Comment_test();
    ownFlash_test();
    rightButtons_test();
    Hide_softKey_test(ans);
    Hide_softKey_test(q);
    Hide_softKey_test(comment);
    question_answer_editext_test(false, false);
    speakerOnandOFF_Test();
    session_Tracker();
    pageWeight(pagewight);
}
    //method to be used in other classes


    public void displayOneView(int cid, int fid) {

        Cursor cr = dbHelper.fetchAllFlashsWithChap(cid, fid);

        q.setText(cr.getString(cr.getColumnIndex("Question")));
        ans.setText(cr.getString(cr.getColumnIndex("Answer")));
        comment.getText().clear();
        String xcr = cr.getString(cr.getColumnIndex("Comment"));
        if (xcr != null) {
            comment.setText(cr.getString(cr.getColumnIndex("Comment")));
        }

    }

    public void getChapterTitle() {
        Cursor cr = dbHelper.getSomethingFromChapsTable(num);
        if (cr != null) {
            if (cr.moveToFirst()) {
                String label = cr.getString(cr.getColumnIndex("Title"));
                title.setText(label);
            }
            cr.close();

        }
    }


    public void Hide_softKey_test(TextView i) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(i.getWindowToken(), 0);
    }




    //***************** LAYOUT Weights *******************************



    public void q_Weight(Integer i) {

        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) lquestion
                .getLayoutParams();
        lp2.weight = i;
        lquestion.setLayoutParams(lp2);

    }

    public void ans_Weight(Integer i) {

        LinearLayout.LayoutParams lp3 = (LinearLayout.LayoutParams) lanswer
                .getLayoutParams();
        lp3.weight = i;
        lanswer.setLayoutParams(lp3);
    }



    public void pageWeight(Integer i) {

        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) lquestion
                .getLayoutParams();
        lp2.weight = i;
        lquestion.setLayoutParams(lp2);

        LinearLayout.LayoutParams lp3 = (LinearLayout.LayoutParams) lanswer
                .getLayoutParams();
        lp3.weight = i;
        lanswer.setLayoutParams(lp3);
    }

    public void rightSide1_Weight(Integer i) {
        LinearLayout.LayoutParams lp8 = (LinearLayout.LayoutParams) lq1
                .getLayoutParams();
        lp8.weight = i;
        lq1.setLayoutParams(lp8);

        LinearLayout.LayoutParams lp9 = (LinearLayout.LayoutParams) lans1
                .getLayoutParams();
        lp9.weight = i;
        lans1.setLayoutParams(lp9);
    }

    public void rightSide2_Weight(Integer i) {
        LinearLayout.LayoutParams lp8 = (LinearLayout.LayoutParams) lq2
                .getLayoutParams();
        lp8.weight = i;
        lq2.setLayoutParams(lp8);

        LinearLayout.LayoutParams lp9 = (LinearLayout.LayoutParams) lans2
                .getLayoutParams();
        lp9.weight = i;
        lans2.setLayoutParams(lp9);
    }
    //***** Somethis may be missing in this method see last line
    public void commentWeight(Integer i) {
        LinearLayout.LayoutParams lp4 = (LinearLayout.LayoutParams) lcomment
                .getLayoutParams();
        lp4.weight = i;
        //***** is missing :     lcomment.setLayoutParams(lp9);
    }
    public void testingBar_Weignt(float i){
        LinearLayout.LayoutParams lp7 = (LinearLayout.LayoutParams) ll1
            .getLayoutParams();
        lp7.weight = i;
        ll1.setLayoutParams(lp7);}

    public void question_answer_editext_test(boolean x, boolean y) {
        q.setEnabled(x);
        ans.setEnabled(y);
    }
//***************** LAYOUT Parameters *******************************
    public void layoutParameters() {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) toptop
                .getLayoutParams();
        lp.weight = (float) 1.2;
        toptop.setLayoutParams(lp);

        LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) ltitle
                .getLayoutParams();
        lp1.weight = (float) 1.2;
        ltitle.setLayoutParams(lp1);

        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) lquestion
                .getLayoutParams();
        lp2.weight = 8;
        lquestion.setLayoutParams(lp2);

        LinearLayout.LayoutParams lp3 = (LinearLayout.LayoutParams) lanswer
                .getLayoutParams();
        lp3.weight = 8;
        lanswer.setLayoutParams(lp3);

        LinearLayout.LayoutParams lp4 = (LinearLayout.LayoutParams) lcomment
                .getLayoutParams();
        lp4.weight = 0;
        lcomment.setLayoutParams(lp4);

        LinearLayout.LayoutParams lp5 = (LinearLayout.LayoutParams) llistview
                .getLayoutParams();
        lp5.weight = 0;
        llistview.setLayoutParams(lp5);

        LinearLayout.LayoutParams lp6 = (LinearLayout.LayoutParams) l1
                .getLayoutParams();
        lp6.weight = (float) 1.4;
        l1.setLayoutParams(lp6);

        LinearLayout.LayoutParams lp7 = (LinearLayout.LayoutParams) ll1
                .getLayoutParams();
        lp7.weight = 0;
        ll1.setLayoutParams(lp7);

        LinearLayout.LayoutParams lp8 = (LinearLayout.LayoutParams) lq2
                .getLayoutParams();
        lp8.weight = 0;
        lq2.setLayoutParams(lp8);

        LinearLayout.LayoutParams lp9 = (LinearLayout.LayoutParams) lans2
                .getLayoutParams();
        lp9.weight = 0;
        lans2.setLayoutParams(lp9);
    }







    public void displayQuizNber(int i) {
        flashNbers.getText().clear();
        flashNbers.setText("" + i);

    }

    public void rightButtons_test() {
        bcomment.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bcomment);
        edit.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.editblack);
        bquestion1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
        banswer1.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);
        bquestion2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.bquestion);
        banswer2.setBackgroundResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.banswer);

    }

    public void Comment_test() {

        // displayOneView(num,i);
        // String button=comment.getText().toString();
        xcomment = dbHelper.return_Comment(num, i);

        if (xcomment.equals("0")) {

            addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentblack);
            commentWeight(0);
            toSpeak ="Question:\n"+q.getText().toString()+"\nThe Answer is:\n"+ans.getText().toString();

        } else {

            addcomment.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcommentclicked);

            commentWeight(5);
            comment.setEnabled(false);
            //if(!getApplicationContext().getClass().getPackageName().equals("Testing_Mode")){
           // toastShort( xcomment); was surpressed
            toSpeak ="Question:\n"+q.getText().toString()+"\n. The Answer is: \n"+ans.getText().toString()+"\n. Your Comment is:.\n "+comment.getText().toString();
        }
    }

    public void favorite_test() {
        int button = dbHelper.favorite_method(num, i);

        if (button == 1) {

            favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteclicked);
            f=1;
        } else {

            favorite.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.favoriteblack);
            f=0;
        }
    }

    public void marked_test() {
        int button = dbHelper.marked_method(num, i);

        if (button == 1) {

            marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markclicked);
            //m=1;
        } else {

            marked.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.markblack);
            //m=0;
        }
    }

    public void correct_test() {
        int button = dbHelper.correct_method(num, i);

        if (button == 1) {
            correct.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.correctblack);
            c=1;
        } else if (button == 2) {
            correct.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.wrongwhite);
            c=2;
        }else correct.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.correctblack);c=0;
    }

    public void ownFlash_test() {
        int button = dbHelper.ownFlash_method(num, i);

        if (button == 1) {
            addcards.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcardswhiteicon);
            o=1;
        } else  {
            addcards.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.addcardsblackicon);
            o=0;
        }
    }

    public void seen_before_test() {
        int viewed = dbHelper.seen_method(num, i);
        if (viewed != 0) {v=1;
            if (viewed == 1) {
                seen_times.setText("1st time!");
            } else {

                seen_times.setText(viewed + " times");
            }
        }
    }



    @SuppressWarnings("deprecation")
    public void ttsUnder20(String toSpeak) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(String toSpeak) {
        String utteranceId=this.hashCode() + "";
       // CharSequence text=flashcard + num1+"Question:. "+q.getText()+". The answer is:. "+ans.getText()+".";
        //CharSequence toSpeak="Question:\n"+q.getText().toString()+"\n. The Answer is: \n"+ans.getText().toString()+"\n. Your Comment is:.\n "+comment.getText().toString();
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }


    public void speakerOnandOFF_Test(){
        if(tts!=null){
        tts.stop();
    }
        if (zx){read_the_card();} else {if(tts!=null){
            tts.stop();
            //tts.shutdown();
        }}
        /*if (!boospeak){
            read_the_card();
        }*/
    }

    public void Time_Getter(){

        //Date & time parameter
        // 09/28/2015
        myDate = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());

        // 15:07:53
        myTime  = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) ;


    }


    public void session_Tracker(){

        Time_Getter();
        dbHelper.Time_and_Date_Finish_Saver_Method( myTime,myDate,num, i,views, sessionNber );


	/* Date currentDate = new Date(System.currentTimeMillis());

	 Time currentTime = new Time();
	 currentTime.setToNow();*/
    }


    public void speakerOnandOFF(){

        if (xxy==1){xxy=2;
            boospeak = false;
            read_the_card();
            speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakermuteblack);

        }
        else{xxy=1;
            speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
            onDestroy();
            boospeak = true;
        }}

    public void toastShort(String str){

        Toast.makeText(getApplicationContext(),
                str ,
                Toast.LENGTH_SHORT).show();

    }


    public void textSize (int i){
        q.setTextSize(i);
        ans.setTextSize(i);
        comment.setTextSize(i);
    }
    //zoomIn and zoomOut methods
    public void zoomIn(){
        if (iTextSize < 90)
            iTextSize = iTextSize + 3;
        else
            toastShort("Hey, This is the maximum font size allowed");//local method

        textSize(iTextSize);//local method

    }

    public void zoomOut() {
        if (iTextSize > 6)
            iTextSize = iTextSize - 3;
        else
            toastShort("Hey, This is the minimum font size allowed");

        textSize(iTextSize);
    }

    public void dataToShare(){
        q1= q.getText().toString();
        ans1= ans.getText().toString();
        title1= title.getText().toString();
        comment1 = comment.getText().toString();}


    public void shareMethod(){
        dataToShare();

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        String shareBody = "Hey,"
                + "\n\n"
                + "Just I would like to share this flashcard (# "
                + i
                + ")"
                + "\n\n"
                + "Question: "
                + q1
                + "\n\n"
                + "Answer: "
                + ans1
                + "\n\n"
                + comment1
                +"\n"
                + "Get more flashcards and outstanding study notes from this app (Google Play  App Store):\n "
                + root;
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title1 + " from this android App: " + application);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        //startActivity(Intent.createChooser(sharingIntent, "Share via"));
        startActivity(sharingIntent);}


    public void read_the_card() {
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                if (status == TextToSpeech.SUCCESS) {

                    int result = tts.setLanguage(Locale.US);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getApplicationContext(),
                                "Hey, This Language is not supported",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        //speakButton.setEnabled(true);
                        if (zx){speakText();} else {if(tts!=null){
                            tts.stop();
                            tts.shutdown();
                        }}
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Initilization Failed!",
                            Toast.LENGTH_SHORT).show();

                }
            }
        }


        );
        //speakText();}
    }
    @Override
    public void onBackPressed()
    { if(tts!=null){
        tts.stop();
        tts.shutdown();zx=false;
        speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
    }
        session_Tracker();
        Intent intent = new Intent(this,Tabs.class);
        startActivity(intent);
        overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
        finish();
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub


        super.onPause();
        //session_Tracker();
        //finish();
    }


    public void speakText(){

        CharSequence text="flashcard number" + i+"Question: \n"+q.getText().toString()+"\n The Answer is: \n"+ans.getText().toString()+"\n"+comment.getText().toString();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text,TextToSpeech.QUEUE_FLUSH,null,"id1");
        } else{HashMap<String, String> map = new HashMap<>();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
            tts.speak(""+text, TextToSpeech.QUEUE_FLUSH, map);}
    }

    @Override
    public void onDestroy(){
        // Don't forget to shutdown tts!
        if(tts!=null){
            tts.stop();
            tts.shutdown();zx=false;
            speaker.setImageResource(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.drawable.speakerblack);
        }
        //session_Tracker();
        super.onDestroy();
    }
}
