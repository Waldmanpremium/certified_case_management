package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

//import com.github.mikephil.charting.data.LineEntry;


public class Tabs  extends Activity  {
	 private DatabaseAccess dbHelper;
	 private SimpleCursorAdapter dataAdapter;
	 private SQLiteDatabase database;
	int resId1,resId2,resId3,num,numo,num1,k,z;
	String quest1,text1,title ,ftitle ,fquest,ftext;

	int sessionNber,testSessionNber, quizNber,viewed,marked1,nonStudied1,own1, favorite1,results,  marq, cid, fid,responded,wrong, ChapNber;
    String myTime,myDate;
	String application, market,market_name, facebook,email,Startup_Name;
	BarChart chart;
	PieChart mChart;
	LineChart lineChart;
	float average, means;
	TextView textRight2,textRight2a, textCenter2, textLeft2, textTop121, textLeft4;


	@Override
	protected void onCreate(Bundle savedInstanceState) {


		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.tabs);




		LinearLayout b01 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.toplastresults);
		LinearLayout top02 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top02);
		LinearLayout top03 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top03);
		LinearLayout b11 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top11);
		LinearLayout b12 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top12);
		LinearLayout b13 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top13);
		LinearLayout b14 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top14);
		LinearLayout b15 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top15);
		LinearLayout topRight2 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.topRight2);
		LinearLayout top36 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top36);

		LinearLayout b21 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top21);
		LinearLayout b22 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top22);
		LinearLayout b23 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top23);
		LinearLayout b24 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top24);
		LinearLayout b25 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top25);
		LinearLayout b31 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top31);

		LinearLayout b33 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top33);
		LinearLayout b34 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top34);
		LinearLayout b35 = (LinearLayout) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.top35);



		chart = (BarChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.chart);
		mChart = (PieChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.pieChart);
		lineChart = (LineChart) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.lineChart);



		application = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.app_name);
		market = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market);
		market_name=getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.market_name);
		email = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.email);
		facebook = getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.facebook);
		Startup_Name=getResources().getString(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.string.Startup_Name);



		dbHelper = new DatabaseAccess(this);
		dbHelper.open();
		dbHelper.onCreate(database);
		if (dbHelper.getResults(sessionNber) == -1 || sessionNber == 0 || sessionNber == -1) {

			dbHelper.onUpgrade(database);


		}

		ChapNber = dbHelper.Chap_Nber();
		sessionNber = dbHelper.getSession_tableCount();
		quizNber = dbHelper.getflash_tableCount();
		testSessionNber= dbHelper.getTestSession_tableCount();




		viewed = dbHelper.Viewed_Nber();
		favorite1 = dbHelper.Favorite_Nber();
		wrong = dbHelper.Wrong_Nber();
		marked1 = dbHelper.Marked_Nber();
		own1 = dbHelper.OwnFlash_Nber();
		nonStudied1 = dbHelper.Not_Studied_Nber();
		//dbHelper.addOldFlashcard();
		//dbHelper.open();


		//should be modified***************result1
		//results1=dbHelper.getflash_tableCount();
		//textTop151=(TextView) findViewById(R.id.textTop151);
		textRight2a = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textRight2a);
		textRight2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textRight2);
		textCenter2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textCenter2);
		textLeft2 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textLeft2);
		//textRight5 = (TextView) findViewById(R.id.textRight5);
		textLeft4 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textLeft4);
		textTop121 = (TextView) findViewById(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.id.textTop121);
		//textTop131 = (TextView) findViewById(R.id.textTop131);
		//textTop141 = (TextView) findViewById(R.id.textTop141);



		//newly added october 18
		Cursor cursor = dbHelper.getSomethingFromSESSION_Table(sessionNber);
		if (dbHelper.getTestSession_tableCount()==0){

		results= cursor.getInt(cursor.getColumnIndexOrThrow("Session_Results"));
		//if (meterwhite == -1) {}
			textLeft4.setText("-- %"); }
			else {
			Cursor cursor1 = dbHelper.getSomethingFromTestSESSION_Table(testSessionNber);
			results= cursor1.getInt(cursor.getColumnIndexOrThrow("Session_Results"));
			textLeft4.setText("" + results + "%");}
		textTop121.setText("" + dbHelper.Marked_Nber());
		//textTop131.setText("" + dbHelper.OwnFlash_Nber());
		//textTop141.setText("" + dbHelper.Not_Studied_Nber());


		textRight2.setText("" + viewed);
		textCenter2.setText("" + (quizNber ));
		textLeft2.setText("" + sessionNber);
		//textRight5.setText("" + favorite1);
		textRight2a.setText("" + ChapNber);
		//textTop151.setText("" + wrong);

		//new courbes oct2018
		lineChartMethod1();
		pieChartMethod1(means(quizNber-nonStudied1), quizNber-nonStudied1,"Studied", "Not Studied", "Studied Cards %age");


		b01.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				if (dbHelper.getTestSession_tableCount()==0){Toast.makeText(getApplicationContext(),
						"Sorry, You don't have any Scored exam review!  Please go to Test Mode and start testing." , Toast.LENGTH_LONG).show();
				} else {

					Intent i = new Intent(getApplicationContext(), Graph_Stuff.class);
					startActivity(i);


					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();
				}
			}
		});

		top36.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				if (dbHelper.getTestSession_tableCount()==0){Toast.makeText(getApplicationContext(),
						"Sorry, You don't have any Scored exam review!  Please go to Test Mode and start testing." , Toast.LENGTH_LONG).show();
				} else {

					Intent i = new Intent(getApplicationContext(), Graph_Stuff.class);
					startActivity(i);


					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();
				}
			}
		});



		b11.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (sessionNber!=0 && sessionNber!=-1) {
					// Get the cursor, positioned to the corresponding row in the result set
					Cursor cursor = dbHelper.getSomethingFromSESSION_Table(sessionNber);
					cid = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Chap_ID"));
					fid = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Flash_Rank_ID"));
					marq = cursor.getInt(cursor.getColumnIndexOrThrow("Session_Mode"));
					myTime= cursor.getString(cursor.getColumnIndexOrThrow("Time_Session_Finish"));
                    myDate= cursor.getString(cursor.getColumnIndexOrThrow("Date_Session_Finish"));
					responded= cursor.getInt(cursor.getColumnIndexOrThrow("Session_Responded"));
					wrong= cursor.getInt(cursor.getColumnIndexOrThrow("Session_Wrong"));
					results= cursor.getInt(cursor.getColumnIndexOrThrow("Session_Results"));
					//this part of the initial session tracker, it will create a new session key & save mainly starting date and time
					//time_Getter();
					//session=""+dbHelper.Time_and_Date_Finish_Saver_Method(myTime, myDate,marq,position+1,1);



				switch (marq) {
					case 1:
						Intent i1 = new Intent(getApplicationContext(),Learning_Mode.class);

						i1.putExtra("somekey2", cid);
						i1.putExtra("somekey3", sessionNber);
						i1.putExtra("somekey4", fid);

						startActivity(i1);
						overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
                        Toast.makeText(getApplicationContext(),
                                "This is the last flashcard you studied in your previous session in the Learning mode at "+myTime+" on "+myDate , Toast.LENGTH_LONG).show();
						finish();
                        break;
					case 2:
						Intent i2 = new Intent(getApplicationContext(),Handbook_New.class);
						i2.putExtra("somekey2", cid);
						i2.putExtra("somekey3", sessionNber);
						i2.putExtra("somekey4", fid);

						startActivity(i2);
						overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
                        Toast.makeText(getApplicationContext(),
                                "This is the last flashcard you studied in your previous session in the Textbook mode at "+myTime+" on "+myDate , Toast.LENGTH_LONG).show();
						finish();
                        break;
					case 3:
						Intent i3 = new Intent(getApplicationContext(),Testing_Mode.class);
						i3.putExtra("somekey2", cid);
						i3.putExtra("somekey3", sessionNber);
						i3.putExtra("somekey4", fid);
						i3.putExtra("somekey5", responded);
						i3.putExtra("somekey6", wrong);
						startActivity(i3);
						overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
                       Toast.makeText(getApplicationContext(),
                                "This is the last flashcard you studied in your previous session in the Test mode at "+myTime+" on "+myDate , Toast.LENGTH_LONG).show();
						finish();
                        break;
					case 4:

						Intent i4 = new Intent(getApplicationContext(),Random_Mode.class);
						i4.putExtra("somekey2", cid);
						i4.putExtra("somekey3", sessionNber);
						i4.putExtra("somekey4", fid);

						startActivity(i4);
						overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
                        Toast.makeText(getApplicationContext(),
                                "This is the last flashcard you studied in your previous session in the Random mode at "+myTime+" on "+myDate , Toast.LENGTH_LONG).show();
						finish();
                        break;
					case 5:
						Intent i5 = new Intent(getApplicationContext(),Slideshow_Mode.class);
						i5.putExtra("somekey2", cid);
						i5.putExtra("somekey3", sessionNber);
						i5.putExtra("somekey4", fid);

						startActivity(i5);
						overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
                        Toast.makeText(getApplicationContext(),
                                "This is the last flashcard you studied in your previous session in the Slideshow mode at "+myTime+" on "+myDate , Toast.LENGTH_LONG).show();
						finish();
						break;

				}



			}else Toast.makeText(getApplicationContext(),
					"Hey, This is your first session. Go down to learning Mode!" , Toast.LENGTH_SHORT).show();
			}
		});

		b12.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (marked1==-1||marked1==0){Toast.makeText(getApplicationContext(),
						"Sorry, You don't have any Marked for review flashcard!" , Toast.LENGTH_SHORT).show();
				} else {
					String clos = "6";
					Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
					i.putExtra("somekey0", clos);
					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();
				}
			}
		});

		b13.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (own1 == -1 || own1 == 0) {
					Toast.makeText(getApplicationContext(),
							"Sorry, You don't have your own flashcards yet!", Toast.LENGTH_SHORT).show();
				} else{
				String clos = "7";
				Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
				i.putExtra("somekey0", clos);
				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();
			}
			}
		});
		b14.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (nonStudied1 == -1 || nonStudied1 == 0) {
					Toast.makeText(getApplicationContext(),
							"Him, it seems that You have studied all the flashcards!", Toast.LENGTH_SHORT).show();
				} else {

					String clos = "8";
					Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
					i.putExtra("somekey0", clos);
					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();
				}
			}
		});

		topRight2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (favorite1 == -1 || favorite1 == 0) {
					Toast.makeText(getApplicationContext(),
							"Sorry, You don't have any favorite flashcard!", Toast.LENGTH_SHORT).show();
				} else{
					String clos ="9";
					Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
					i.putExtra("somekey0", clos);
					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

					finish();
				}}
		});

		top02.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (wrong == -1 || wrong == 0) {
					Toast.makeText(getApplicationContext(),
							"Sorry, You don't have any question that you responded wrongly! Go to test mode to start practicing.", Toast.LENGTH_SHORT).show();
				} else{
					String clos ="10";
					Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
					i.putExtra("somekey0", clos);
					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
					finish();

				}}
		});


		top03.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					Intent i = new Intent(getApplicationContext(), StatisticsCursorAdaptorActivity.class);

					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				finish();

				}
		});


		b15.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), SearchActivity.class);

				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				finish();

			}
		});
		
		b21.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String clos ="1";
					Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
					i.putExtra("somekey0", clos);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);

					overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				finish();


			}
		});

		b22.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String clos ="2";
				Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
				i.putExtra("somekey0", clos);
				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

				finish();

			}
		});
		b23.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String clos ="3";
				Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
				i.putExtra("somekey0", clos);
				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

				finish();

			}
		});
		b24.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String clos1 ="4";
				Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
				i.putExtra("somekey0", clos1);
				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

				finish();

			}
		});
		b25.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String clos ="5";
				Intent i = new Intent(getApplicationContext(), CursorAdaptorActivity.class);
				i.putExtra("somekey0", clos);
				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);

				finish();

			}
		});



		//Exam Tips

		b31.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), MCQtips.class);

				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);finish();
			}
		});




//Aps info page
		b33.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), AppInfos.class);

				startActivity(i);

				overridePendingTransition(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushin, com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.anim.pushout);
				finish();
			}
		});


//Send us your feed back
		b34.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String[] to = { email };

				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(Intent.EXTRA_EMAIL, to);

				i.putExtra(Intent.EXTRA_SUBJECT, "Regarding your application: " + application + " from "
						+ market_name);
				i.putExtra(Intent.EXTRA_TEXT,
						"Dear "+Startup_Name+" Team, \n\n I have a quick comment regarding you application :  \""
								+ application + "  from " + market_name + "\n\n");
				i.setType("message/rfc822");
				startActivity(i);

			}
		});

//Share this App
		b35.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				sharingIntent.setType("text/html");
				String shareBody = "Hi, \n\n I found an excellent self learning  app on "+ market_name + " Store, I strongly recommend it for you:\n"
				+"\"" + application +  "\"\n\n"+ "Here\'s the link: "+"\n\n"+ "https://play.google.com/store/apps/details?id="+ getPackageName()+"\n\n"+"All The Best.";
				sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "An excellent app, I strongly recommend it for you");
				sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
				//startActivity(Intent.createChooser(sharingIntent, "Share via"));
				startActivity(sharingIntent);

			}
		});









	}public void time_Getter(){

		String myDate = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());

		// 15:07:53
		String myTime  = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) ;

	}




	//method Newly added spetember 2018
	public void FromOldToNew(){
		ftitle = "tl" + num;
		fquest = "q" + num+"a";
		ftext = "tx" + num+"a";
		resId1 = getApplicationContext().getResources().getIdentifier(fquest + num1, "string", getPackageName());
		resId2 = getApplicationContext().getResources().getIdentifier(ftext + num1, "string", getPackageName());
		resId3 = getApplicationContext().getResources().getIdentifier("titl" + num, "string", getPackageName());

	}


	//method Newly added spetember 2018 to get total number of chapters
	public int OldReturnNumberOfChapterFromString(){

		k=0; numo=1;


		resId3 = getApplicationContext().getResources().getIdentifier("titl" + numo, "string", getPackageName());
		while (resId3 != 0) {resId3 = getApplicationContext().getResources().getIdentifier("titl" + numo, "string", getPackageName());
			k = k + 1;

			numo++;
			}
		k=k-1;
		return	k;
	}

	//method Newly added spetember 2018 to get total number of flashcards per chapter
	public int OldReturnNumberOfFalshcardsFromString(int num){

		z=0; num1=1;
		fquest = "q" + num+"a";
		resId1 =getApplicationContext().getResources().getIdentifier ( fquest + num1,  "string", getPackageName());
		while (resId1 != 0) {
			resId1 =getApplicationContext().getResources().getIdentifier ( fquest + num1,  "string", getPackageName());
			z = z + 1;
			num1++; }
		z = z - 1;
		num1=1;
		return	z;
	}

	//method Newly added spetember 2018 to return a specific question of a flashcards
	public  String oldGetQuestion(int resId1){
		quest1 = getResources().getString(resId1);
		return quest1;
	}

	//method Newly added spetember 2018 to return a specific answer of a flashcards
	public String oldGetAnswer(int resId2){
		text1 = getResources().getString(resId2);
		return text1;
	}


	//method Newly added spetember 2018 to return a specific title
	public String oldGetTitle(int resId3){
		title  = getResources().getString(resId3);
		return title ;
	}










	//*********************************
	public void lineChartMethod1() {

		ArrayList<Entry> lineEntries = new ArrayList<Entry>();

		YAxis leftAxis = lineChart.getAxisLeft();
		YAxis rightAxis = lineChart.getAxisRight();

		XAxis xAxis = lineChart.getXAxis();
		if (testSessionNber!=0 & testSessionNber!=0) {
			int i;
			if (testSessionNber > 12) {
				i = (testSessionNber - 12);
			} else i = 1;
			// for (int i = testSessionNber; i > (testSessionNber-1); i--) {
			while (i <= testSessionNber) {
				try {
					Cursor cursor2 = dbHelper.getSomethingFromTestSESSION_Table(i);

					results = cursor2.getInt(cursor2.getColumnIndexOrThrow("Session_Results"));
					lineEntries.add(new Entry(i, results));
					i++;

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {lineEntries.add(new Entry(0, 0));}

		LineDataSet lineDataSet = new LineDataSet(lineEntries, "Scores chart");
		//LineData data = new LineData(lineDataSet);


		lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
		lineDataSet.setHighlightEnabled(true);
		lineDataSet.setLineWidth(2);
		lineDataSet.setColor(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.color.butterflyblue);
		lineDataSet.setCircleColor(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.color.colorAccent);
		lineDataSet.setCircleRadius(2);
		lineDataSet.setCircleHoleRadius(1);
		lineDataSet.setDrawHighlightIndicators(true);
		lineDataSet.setHighLightColor(Color.RED);
		lineDataSet.setValueTextSize(6);
		lineDataSet.setValueTextColor(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.color.black);

		LineData lineData = new LineData(lineDataSet);
		lineData.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
		lineChart.getDescription().setText("Scores of test sessions");
		lineChart.getDescription().setTextSize(4);
		lineChart.setDrawMarkers(true);
		lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
		lineChart.animateY(1000);
		lineChart.getXAxis().setGranularityEnabled(true);
		lineChart.getXAxis().setGranularity(1.f);
		lineChart.getXAxis().setLabelCount(lineDataSet.getEntryCount());
		leftAxis.setAxisMinimum(0);
		leftAxis.setAxisMaximum(100);
		leftAxis.setTextSize(5f);
		rightAxis.setEnabled(false);
		lineChart.setData(lineData);
		lineChart.invalidate();
		if(testSessionNber>12){xAxis.setAxisMinimum(testSessionNber-12);}else xAxis.setAxisMinimum(0);

		xAxis.setTextSize(5f);
		lineChart.setBackgroundColor(Color.parseColor("#90ffffff"));
	}


	public void pieChartMethod1(float average,int raw, String A, String B, String description){
		//PieChart mChart = (PieChart) findViewById(R.id.pieChart);

		ArrayList<PieEntry> pieChartEntries = new ArrayList<>();

		pieChartEntries.add(new PieEntry(average, A));
		pieChartEntries.add(new PieEntry(100-average, B));


		PieDataSet set = new PieDataSet(pieChartEntries, "");

		PieData data = new PieData(set);

		data.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
		data.setValueTextSize(0x1.2p4f);
		data.setValueTextColor(Color.parseColor("#20084f"));
		mChart.setData(data);


		Random r = new Random();
		int g =r.nextInt(5);
		switch (g){
			case 0: set.setColors(ColorTemplate.MATERIAL_COLORS); break;
			case 1: set.setColors(ColorTemplate.COLORFUL_COLORS); break;
			case 2: set.setColors(ColorTemplate.VORDIPLOM_COLORS); break;
			case 3: set.setColors(ColorTemplate.LIBERTY_COLORS); break;
			case 4: set.setColors(ColorTemplate.PASTEL_COLORS); break;

			default: set.setColors(Color.parseColor("#FF8080")); break; }

		mChart.setUsePercentValues(true);
		mChart.setCenterText(raw+" "+A);
		mChart.setCenterTextColor(Color.parseColor("#808080"));
		mChart.setCenterTextSize(0x2.8p3f);
		mChart.animateY(1000);
		mChart.setTouchEnabled(true);

		mChart.getDescription().setText(description);
		mChart.getDescription().setTextSize(8f);


		mChart.getDescription().setTextAlign(Paint.Align.RIGHT);
		//change the color of label within the pie chart
		mChart.setEntryLabelColor(Color.parseColor("#3B0B17"));
		mChart.setBackgroundColor(Color.parseColor("#90ffffff"));

		mChart.invalidate();


	}

	public float average(){
		average =0;
		for (int i=1; i<=testSessionNber;i++) {
			Cursor cursor1 = dbHelper.getSomethingFromTestSESSION_Table(i);
			int xcc= cursor1.getInt(cursor1.getColumnIndexOrThrow("Session_Results"));
			average=(average+xcc);
		}
		average=average/(testSessionNber);

		return average;
	}

	public float means(int cards){
if (quizNber!=0){
		means = cards*100/quizNber;} else means=0;
		return means;
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//finish();
	}
	@Override
	public void onDestroy(){

		super.onDestroy();
	}

	@Override
	public void onBackPressed(){

		finish();

	}
}


