package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;
/**
 * Store information about a chapter.
 */
public class Chapter_Titles_Class {
	private String title;
	private int nberFlashcards;
	private int iconID;
	private int sectionNber;
	
	
	 // constructor
		public Chapter_Titles_Class() {
		}
	
	 // constructor
	public Chapter_Titles_Class(String title, int nberFlashcards, int iconID, int sectionNber) {
	
		this.title = title;
		this.nberFlashcards = nberFlashcards;
		this.iconID = iconID;
		this.sectionNber = sectionNber;
	}
	
	
	 // constructor
		public Chapter_Titles_Class(String title,  int iconID, int sectionNber) {
			super();
			this.title = title;
			this.iconID = iconID;
			this.sectionNber = sectionNber;
		}
		
		 // constructor
		public Chapter_Titles_Class(String title, int sectionNber) {
			super();
			this.title = title;
			this.sectionNber = sectionNber;
		}
		
		
   
	
	//Getting Title
	public String getTitle() {
		return this.title ;
	}
	 // setting Title
    public void setTitle(String title){
        this.title = title;
    }
	
	//Getting Nber of flashcards
	
	public int getNberFlashcards() {
		return this.nberFlashcards;
	}
	
	 // setting Nber of Flashcards
    public void setNberFlashcards(int nberFlashcards){
        this.nberFlashcards = nberFlashcards;
    }
	
	
	
	
	//Getting Icon ID
	public int getIconID() {
		return this.iconID;
	}


	
	// setting Icon ID
    public void setIconID(int iconID){
        this.iconID = iconID;
    }
	
    
    
    
	//Getting Section number
	public int getSectionNber() {
		return this.sectionNber;
	}
	
	// setting SectionNber
    public void setSectionNber(int iconID){
        this.sectionNber = sectionNber;
    }
	
	
	
}
