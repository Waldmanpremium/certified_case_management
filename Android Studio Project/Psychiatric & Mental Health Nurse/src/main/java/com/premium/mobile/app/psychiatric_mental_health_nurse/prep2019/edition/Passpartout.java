package com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Locale;


public class Passpartout  extends Activity {
	  

private TextToSpeech tts;
private Button speakButton;
private EditText inputText;


	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	    	requestWindowFeature(Window.FEATURE_NO_TITLE);
	        super.onCreate(savedInstanceState);
	        setContentView(com.premium.mobile.app.psychiatric_mental_health_nurse.prep2019.edition.R.layout.apasspartout);
	        
	    	//tts = new TextToSpeech(this, this);
			tts =new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener(){
				@Override
			    public void onInit(int status) {

			        if (status == TextToSpeech.SUCCESS) {

			            int result = tts.setLanguage(Locale.US);

			            if (result == TextToSpeech.LANG_MISSING_DATA
			                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
			                Log.e("TTS", "This Language is not supported");
			            } else {
			                //speakButton.setEnabled(true);
			                speakText();
			            }

			        } else {
			            Log.e("TTS", "Initilization Failed!");
			        }}}

			    
				);
			speakText();}
			
		

	       /*  *************** inputText = (EditText) findViewById(R.id.editText1);
	        

	       speakButton = (Button) findViewById(R.id.button1);
	       button on click event
	        speakButton.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View arg0) {
	                speakOut();
	            }

	        });
	    } ***************************   */

	   
			
			@Override
	    public void onDestroy() {
	        // Don't forget to shutdown tts!
	        if (tts != null) {
	            tts.stop();
	            tts.shutdown();
	        }
	        super.onDestroy();
	    }

	   
	    
	    
	    

	  
	    
	    private void speakText() {

	        //String toSpeak = inputText.getText().toString();
	    	String toSpeak ="Hi, This test is very successful indeed! good luck Mr. karim,   ";

	       // tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
	            ttsGreater21(toSpeak);
	        } else {
	            ttsUnder20(toSpeak);
	        }
	    }
	    
	    @SuppressWarnings("deprecation")
	    private void ttsUnder20(String toSpeak) {
	        HashMap<String, String> map = new HashMap<>();
	        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
	        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, map);
	    }

	    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
	    private void ttsGreater21(String toSpeak) {
	        String utteranceId=this.hashCode() + "";
	        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
	    }
}
